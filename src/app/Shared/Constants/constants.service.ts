import { Injectable } from "@angular/core";

@Injectable()
export class Constants {
    private MESSAGES: Object = {
        borrowerVal: 'Borrower referrence ID is not available',
        successMsg: 'Data saved successfully',
        errorMsg: 'There is some issue with the connectivity. Please try again later',
        emailVal: 'Please enter valid email id',
        pinCodeVal: 'Please enter valid pin code',
        mobileNoVal: 'Please enter valid mobile number',
        panVal: 'Please enter valid pan card number',
        requiredFieldVal: 'Please enter all required fields to save the data',
        unitDetailVal: 'Please fill and save unit details in Borrower Page',
        submitSuccess: 'Data submitted successfully',
        PFIDVal: 'PF ID is not valid. Please try again later',
        LOSDataError: 'Data is not available. Please try with other',
        DateVal: 'To Date should be greater than From Date',
        delPopUpMsg : 'Are you sure that you want to delete the liabilities?',
        liabilityDelSuccMsg: 'Liabilities removed successfully',
        liabilityDelFailMsg: 'Not able to clear liability data. Please try again later',
        PFIDValManageReview: 'Please enter PFID to proceed',
        addReviewerVal: 'You have already added 3 reviewers',
        inspectorVal: 'Please try with other PFID to proceed',
        removeReviewerVal: 'Do you want to remove reviewer?',
        reviewerIDVal: 'You have already added this reviewer',
        changeReviewerVal: 'Please select atleast one checkbox to proceed',
        reviewerAddVal: 'No permission to add this user.',
        submitInspectVal: 'Please fill the required field and submit',
        inspectionSubmitSuccess: 'Inspection review submitted successfully',
        LOSVal: 'Invalid LOS Number',
        rejectVal: 'You have crossed the limit to reinitiate the inspection. Please proceed with new request'
    };

    getMessage(message: string) : string {
        return this.MESSAGES[message];
    }
}