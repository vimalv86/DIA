
export class DIAConstants {
    public static states: any = [
        { label: 'Andaman And Nicobar', value: 'Andaman And Nicobar' },
        { label: 'Andhra Pradesh', value: 'Andhra Pradesh' },
        { label: 'Arunachal Pradesh', value: 'Arunachal Pradesh' },
        { label: 'Assam', value: 'Assam' },
        { label: 'Bihar', value: 'Bihar' },
        { label: 'Chandigarh', value: 'Chandigarh' },
        { label: 'Chhattisgarh', value: 'Chhattisgarh' },
        { label: 'Dadra And Nagar Haveli', value: 'Dadra And Nagar Haveli' },
        { label: 'Daman And Diu', value: 'Daman And Diu' },
        { label: 'Delhi', value: 'Delhi' },
        { label: 'Goa', value: 'Goa' },
        { label: 'Gujarat', value: 'Gujarat' },
        { label: 'Haryana', value: 'Haryana' },
        { label: 'Himachal Pradesh', value: 'Himachal Pradesh' },
        { label: 'Jammu and Kashmir', value: 'Jammu and Kashmir' },
        { label: 'Jharkhand', value: 'Jharkhand' },
        { label: 'Karnataka', value: 'Karnataka' },
        { label: 'Kerala', value: 'Kerala' },
        { label: 'Lakshadweep', value: 'Lakshadweep' },
        { label: 'Madhya Pradesh', value: 'Madhya Pradesh' },
        { label: 'Maharashtra', value: 'Maharashtra' },
        { label: 'Manipur', value: 'Manipur' },
        { label: 'Meghalaya', value: 'Meghalaya' },
        { label: 'Mizoram', value: 'Mizoram' },
        { label: 'Nagaland', value: 'Nagaland' },
        { label: 'Orissa', value: 'Orissa' },
        { label: 'Pondicherry', value: 'Pondicherry' },
        { label: 'Punjab', value: 'Punjab' },
        { label: 'Rajasthan', value: 'Rajasthan' },
        { label: 'Sikkim', value: 'Sikkim' },
        { label: 'Tamil Nadu', value: 'Tamil Nadu' },
        { label: 'Telangana', value: 'Telangana' },
        { label: 'Tripura', value: 'Tripura' },
        { label: 'Uttar Pradesh', value: 'Uttar Pradesh' },
        { label: 'Uttarakhand', value: 'Uttarakhand' },
        { label: 'West Bengal', value: 'West Bengal' }
    ];

    public static upToFive: any = [
        { label: 'One', value: '1' },
        { label: 'Two', value: '2' },
        { label: 'Three', value: '3' },
        { label: 'Four', value: '4' },
        { label: 'Five', value: '5' }
    ];

    public static constitution: any = [
        { label: 'Association of Persons', value: 'Association of Persons' },
        { label: 'Body of Persons', value: 'Body of Persons' },
        { label: 'Club', value: 'Club' },
        { label: 'HUF', value: 'HUF' },
        { label: 'Limited Liability Partnership', value: 'Limited Liability Partnership' },
        { label: 'Partnership Firm', value: 'Partnership Firm' },
        { label: 'Private Ltd Co.', value: 'Private Ltd Co.' },
        { label: 'Propriertorship', value: 'Propriertorship' },
        { label: 'Public Ltd - Listed', value: 'Public Ltd - Listed' },
        { label: 'Public Ltd - Unlisted', value: 'Public Ltd - Unlisted' },
        { label: 'Self Help Group', value: 'Self Help Group' },
        { label: 'Society', value: 'Society' },
        { label: 'Trust', value: 'Trust' },
        { label: 'Others', value: 'Others' },
        { label: 'Partnership Form', value: 'Partnership Form' }
    ];

    public static salutation: any = [
        { label: 'Shri', value: 'Shri' },
        { label: 'Mr.', value: 'Mr.' },
        { label: 'Mrs.', value: 'Mrs.' },
        { label: 'Ms.', value: 'Ms.' },
        { label: 'Dr.', value: 'Dr.' },
        { label: 'Dr. (Mrs.)', value: 'Dr. (Mrs.)' }
    ];

    public static ownership: any = [
        { label: 'Rented', value: 'Rented' },
        { label: 'Owed', value: 'Owed' },
        { label: 'Leased', value: 'Leased' }
    ];

    public static proposedActivity: any = [
        { label: 'Same as existing', value: 'Same as existing' },
        { label: 'New', value: 'New' }
    ];

    public static reviewStatus: any = [
        { label: 'All', value: '' },
        { label: 'PRE-COMPLETED', value: 'PRE-COMPLETED' },
        { label: 'REJECTED', value: 'REJECTED' },
        { label: 'ACCEPTED', value: 'ACCEPTED' }
    ];

    public static loanReqType: any = [
        { label: 'Cash Credit', value: 'Cash Credit' },
        { label: 'Term Loans', value: 'Term Loans' },
        { label: 'Others', value: 'Others' }
    ];

    public static facilityType: any = [
        { label: 'Cash Credit', value: 'Cash Credit' },
        { label: 'Term Loans', value: 'Term Loans' },
        { label: 'Others', value: 'Others' }
    ];

    public static purposeOfLoan: any = [
        { label: 'Working capital', value: 'Working capital' },
        { label: 'Procuring Machinery', value: 'Procuring Machinery' },
        { label: 'Others', value: 'Others' }
    ];

    public static applicationSource: any = [
        { label: 'Branch', value: 'Branch' },
        { label: 'MPST', value: 'MPST' },
        { label: 'Others', value: 'Others' }
    ];

    public static reviewer: any = [
        { name: '', code: '' },
        { name: '', code: '' },
        { name: '', code: '' }
    ];

}
