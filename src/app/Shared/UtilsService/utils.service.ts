import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class UtilsService {
  public validString: string = "";
  private subscription: Subscription;
  private timer: Observable<any>;
  private isNotEnabled: boolean = false;
  emailValidator(email: string): boolean {
    var EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!EMAIL_REGEXP.test(email)) {
      return false;
    }
    return true;
  }

  blockSpecialChar(event) {
    var k = event.keyCode;
    if (!this.isNotEnabled) {
      if (((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57))) {
        this.validString = "";
        this.isNotEnabled = false;
        return true;
      } else {
        this.isNotEnabled = true;
        this.timer = Observable.timer(2000);
        this.subscription = this.timer.subscribe(() => {
          this.isNotEnabled = false;
          this.validString = "";
        });
        this.validString = "Please enter alphabets & numbers";
        return false;
      }
    } else {
      if (((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57))) {
        this.validString = "";
        this.isNotEnabled = false;
        this.subscription.unsubscribe();
        return true;
      } else {
        this.validString = "Please enter alphabets & numbers";
        return false;
      }
    }
  }

  blockNumberAndSpecialChar(event) {
    var k = event.keyCode;
    if (!this.isNotEnabled) {
      if (((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k ==32 || k == 35 || k == 38 || (k > 43 && k < 48))) {
        this.validString = "";
        this.isNotEnabled = false;
        return true;
      } else {
        this.isNotEnabled = true;
        this.timer = Observable.timer(2000);
        this.subscription = this.timer.subscribe(() => {
          this.isNotEnabled = false;
          this.validString = "";
        });
        this.validString = "Please enter only alphabets";
        return false;
      }
    } else {
      if (((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k ==32 || k == 35 || k == 38 || (k > 43 && k < 48))) {
        this.validString = "";
        this.isNotEnabled = false;
        this.subscription.unsubscribe();
        return true;
      } else {
        this.validString = "Please enter only alphabets";
        return false;
      }
    }
  }

  blockNumber(event) {
    var k = event.keyCode;
    if (!this.isNotEnabled) {
      if (((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k > 34 && k < 48))) {
        this.validString = "";
        this.isNotEnabled = false;
        return true;
      } else {
        this.isNotEnabled = true;
        this.timer = Observable.timer(2000);
        this.subscription = this.timer.subscribe(() => {
          this.isNotEnabled = false;
          this.validString = "";
        });
        this.validString = "Numbers are not allowed";
        return false;
      }
    } else {
      if (((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k > 34 && k < 48))) {
        this.validString = "";
        this.isNotEnabled = false;
        this.subscription.unsubscribe();
        return true;
      } else {
        this.validString = "Numbers are not allowed";
        return false;
      }
    }
  }

  onlyNumber(event) {
    var k = event.keyCode;
    if (!this.isNotEnabled) {
      console.log("if")
      if (!(event.shiftKey || (k >= 59 && k <= 93) || (k >= 96 && k <= 123) || k == 8 || (k >= 34 && k < 48))) {
        this.validString = "";
        this.isNotEnabled = false;
        return true;
      } else {
        this.isNotEnabled = true;
        this.timer = Observable.timer(3000);
        this.subscription = this.timer.subscribe(() => {
          this.isNotEnabled = false;
          this.validString = "";
        });
        this.validString = "Only numbers are allowed";
        return false;
      }
    } else {
      console.log("else")
      if (!(event.shiftKey || (k >= 59 && k <= 93) || (k >= 96 && k <= 123) || k == 8 || (k >= 34 && k < 48))) {
        this.validString = "";
        this.isNotEnabled = false;
        this.subscription.unsubscribe();
        return true;
      } else {
        this.validString = "Only numbers are allowed";
        return false;
      }
    }
  }

  dataSourceCheckFromLOS(data) {
    var obj = data;
    Object.keys(obj).forEach(function (key) {
      if (obj[key]) {
        obj[key + "Status"] = "LOS";
      }
      else {
        obj[key + "Status"] = "null";
      }
      if(key == 'clientIdStatus' || key == 'clientId'){
         delete obj["clientIdStatus"];
      }
    });
    return obj;
  }

  dataSourceCheck(data) {
    var obj = data;
    obj = Object.assign(obj, obj.field_statusMap);
    obj["field_status"] = null;
    obj["field_statusMap"] = null;
    return obj;
  }

  dataSourceSave(data) {
    var obj = data;
    console.log(JSON.stringify(obj));
    Object.keys(obj).forEach(function (key) {
      if (key.substr(-6) == "Status") {
        if (obj[key] != "LOS" && obj[key] != "Mob") {
          var keyName = key.split('Status')[0];
          if (obj[keyName]) {
            obj[key] = "Web";
          }
          else {
            obj[key] = "null";
          }
        }
      }
    });
    obj["field_status"] = null;
    obj["field_statusMap"] = null;
    return obj;
  }

  dataSourceCheckFromLOSArray(data) {
    var arrData = data;
    for (let k = 0; k < arrData.length; k++) {
      var obj = arrData[k];
      Object.keys(obj).forEach(function (key) {
        if (obj[key]) {
          obj[key + "Status"] = "LOS";
        }
        else {
          obj[key + "Status"] = "null";
        }
      });
    }
    return arrData;
  }

  dataSourceCheckArray(data) {
    var arrData = data;
    for (let k = 0; k < arrData.length; k++) {
      var obj = arrData[k];
      obj = Object.assign(obj, obj.field_statusMap);
      obj["field_status"] = null;
      obj["field_statusMap"] = null;
    }
    return arrData;
  }
}