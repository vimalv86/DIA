import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule, Http, RequestOptions, XHRBackend } from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from "@angular/common";

//Routing
import { routes } from './routing/app.routing';

//Spinner
import { HttpService } from './Core/http.service';
import { HttpFactory } from './Core/http.factory';

//PrimeNg
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  RadioButtonModule,
  ButtonModule,
  CalendarModule,
  BreadcrumbModule,
  MenuItem,
  DataTableModule, 
  InputTextareaModule, 
  PanelModule, 
  DropdownModule,
  MessagesModule,
  MessageModule,
  CheckboxModule,
  AccordionModule,
  TooltipModule,
  ConfirmDialogModule,
  DialogModule,
  GalleriaModule 
} from 'primeng/primeng';

import 'hammerjs';
import { NgxGalleryModule } from 'ngx-gallery';

//Common Component
import { AppComponent } from './app.component';
import { HeaderComponent } from '../app/Component/Header/header.component';
import { HeaderNavigationComponent } from '../app/Component/HeaderNavigation/headerNavigation.component';
import { FooterComponent } from '../app/Component/Footer/footer.component';
import { FooterNavigationComponent } from '../app/Component/FooterNavigation/footerNavigation.component';
import { SectionComponent } from '../app/Component/Section/section.component';
import { SpinnerComponent } from '../app/Component/Spinner/spinner.component';
import { AsideNavigationComponent } from '../app/Component/AsideNavigation/asideNavigation.component';
import { PopupAsideMenuComponent } from '../app/component/popupAsideMenu/popupAsideMenu.component';

//Module Component
import { SmeComponent } from '../app/Module/SME/sme.component';
import { McgComponent } from '../app/Module/MCG/mcg.component';
import { AgricultureComponent } from '../app/Module/Agriculture/agriculture.component';
import { AutomobileComponent } from '../app/Module/Automobile/automobile.component';
import { CagComponent } from '../app/Module/CAG/cag.component';
import { EducationComponent } from '../app/Module/Education/education.component';
import { HomeComponent } from '../app/Module/Home/home.component';
import { PersonalComponent } from '../app/Module/Personal/personal.component';
import { DetailComponent } from '../app/Module/SME/Presanction/Detail/detail.component';
import { PresanctionComponent } from '../app/Module/SME/Presanction/presanction.component';
import { PresanctionNewComponent } from '../app/Module/SME/Presanction/PresanctionNew/presanctionNew.component';
import { PresanctionListComponent } from '../app/Module/SME/Presanction/PresanctionList/presanctionList.component';
import { PresanctionIncompleteComponent } from '../app/Module/SME/Presanction/PresanctionIncomplete/presanctionIncomplete.component';
import { PresanctionInspectionStatusComponent } from '../app/Module/SME/Presanction/PresanctionInspectionStatus/presanctionInspectionStatus.component';
import { PresanctionDetailComponent } from '../app/Module/SME/Presanction/PresanctionDetail/presanctionDetail.component';
import { BorrowerDetailComponent } from '../app/Module/SME/Presanction/BorrowerDetails/borrowerDetail.component';
import { UnitDetailComponent } from '../app/Module/SME/Presanction/BorrowerDetails/UnitDetails/unitDetail.component';
import { ContactPersonDetailComponent } from '../app/Module/SME/Presanction/BorrowerDetails/ContactPersonDetails/contactPersonDetail.component';
import { KeyPersonDetailComponent } from '../app/Module/SME/Presanction/BorrowerDetails/KeyPersonDetails/keyPersonDetail.component';
import { GuarantorDetailComponent } from '../app/Module/SME/Presanction/GuarantorDetails/guarantorDetail.component';
import { EstablishmentDetailComponent } from '../app/Module/SME/Presanction/EstablishmentDetails/establishmentDetail.component';
import { CollateralDetailComponent } from '../app/Module/SME/Presanction/CollateralDetails/collateralDetail.component';
import { LoanRequestedComponent } from '../app/Module/SME/Presanction/LoanRequested/loanRequested.component';
import { PresentLiabilitiesComponent } from '../app/Module/SME/Presanction/PresentLiabilities/presentLiabilities.component';
import { CommentsComponent } from '../app/Module/SME/Presanction/Comments/comments.component';
import { PopupUnitDetailsComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/UnitDetails/unitDetails.component';
import { ContactPersonDetailsComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/ContactPerson/contactPerson.component';
import { KeyPersonDetailsComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/KeyPerson/keyPerson.component';
import { PopupGuarantorDetailsComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/GuarantorDetails/guarantorDetails.component';
import { PopupEstablishmentDetailsComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/EstablishmentDetails/establishmentDetails.component';
import { PopupCollateralDetailsComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/CollateralDetails/collateralDetails.component';
import { PopupLoanRequestedComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/LoanRequested/loanRequested.component';
import { PopupPresentLiabilitiesComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/PresentLiabilities/presentLiabilities.component';
import { PopupCommentsComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/Comments/comments.component';
import { PopupInspectionComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/Inspection/inspection.component';

import { ReviewReportComponent } from '../app/Module/SME/ReviewReport/reviewReport.component';
import { ReviewReportPopupComponent } from '../app/Module/SME/ReviewReport/ReviewReportPopup/reviewReportPopup.component';
import { ManageInspectionComponent } from '../app/Module/SME/ManageInspection/manageInspection.component';
import { ManageReviewerComponent } from '../app/Module/SME/ManageInspection/ManageReviewer/manageReviewer.component';
import { ChangeReviewerComponent } from '../app/Module/SME/ManageInspection/ChangeReviewer/changeReviewer.component';
import { ManageAccountTransfer } from '../app/Module/SME/ManageInspection/ManageAccountTransfer/manageAccountTransfer.component';

import { SearchReportComponent } from '../app/Module/SME/SearchReport/searchReport.component';
import { SearchReportPopupComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/searchReportPopup.component';
import { SearchPopupUnitDetailsComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/UnitDetails/unitDetails.component';
import { SearchContactPersonDetailsComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/ContactPerson/contactPerson.component';
import { SearchKeyPersonDetailsComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/KeyPerson/keyPerson.component';
import { SearchPopupGuarantorDetailsComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/GuarantorDetails/guarantorDetails.component';
import { SearchPopupEstablishmentDetailsComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/EstablishmentDetails/establishmentDetails.component';
import { SearchPopupCollateralDetailsComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/CollateralDetails/collateralDetails.component';
import { SearchPopupLoanRequestedComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/LoanRequested/loanRequested.component';
import { SearchPopupPresentLiabilitiesComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/PresentLiabilities/presentLiabilities.component';
import { SearchPopupCommentsComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/Comments/comments.component';
import { SearchPopupInspectionComponent } from '../app/Module/SME/SearchReport/SearchReportPopup/Inspection/inspection.component';
//Service
import { DIAUserInfoService } from '../app/Service/User/userInfo.service';
import { SmePresanctionSearchLoanService } from '../app/Service/SME/Presanction/SearchLoan/searchLoan.service';
import { SmePresanctionGetCollateralListService } from '../app/Service/SME/Presanction/CollateralDetails/GetCollateralDetails/getCollateralDetails.service';
import { SmePresanctionGetEstablishmentListService } from '../app/Service/SME/Presanction/EstablishmentDetails/GetEstablishmentDetails/getEstablishmentDetails.service';
import { SmePresanctionGetUnitDetailsService } from '../app/Service/SME/Presanction/BorrowerDetails/UnitDetails/GetUnitDetails/getUnitDetails.service';
import { SmePresanctionGetContactPersonDetailsService } from '../app/Service/SME/Presanction/BorrowerDetails/ContactPersonDetails/GetContactPersonDetails/getContactPersonDetails.service';
import { SmePresanctionGetKeyPersonDetailsService } from '../app/Service/SME/Presanction/BorrowerDetails/KeyPersonDetails/GetKeyPersonDetails/getKeyPersonDetails.service';
import { SmePresanctionIncompleteService } from '../app/Service/SME/Presanction/Incomplete/incomplete.service';
import { SmePresanctionInspectionStatusService } from '../app/Service/SME/Presanction/InspectionStatus/inspectionStatus.service';
import { SmePresanctionGetLiabilityDetailsService } from '../app/Service/SME/Presanction/PresentLiabilities/GetLiabilityDetails/getLiabilityDetails.service'; 
import { SmePresanctionGetLoanRequestedDetailsService } from '../app/Service/SME/Presanction/LoanRequested/GetLoanRequestedDetails/getLoanRequestedDetails.service'; 
import { SmePresanctionGetCommentsService } from '../app/Service/SME/Presanction/Comments/GetComments/getComments.service';
import { SmePresanctionSectionNotificationService } from '../app/Service/SME/Presanction/SectionNotification/sectionNotification.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HeaderNavigationComponent,
    FooterComponent,
    FooterNavigationComponent,
    SectionComponent,
    SpinnerComponent,
    AsideNavigationComponent,
    SmeComponent,
    McgComponent,
    AgricultureComponent,
    AutomobileComponent,
    CagComponent,
    EducationComponent,
    HomeComponent,
    PersonalComponent,
    DetailComponent,
    PresanctionComponent,
    PresanctionNewComponent,
    PresanctionListComponent,
    PresanctionIncompleteComponent,
    PresanctionInspectionStatusComponent,
    PresanctionDetailComponent,
    BorrowerDetailComponent,
    UnitDetailComponent,
    ContactPersonDetailComponent,
    KeyPersonDetailComponent,
    GuarantorDetailComponent,
    EstablishmentDetailComponent,
    CollateralDetailComponent,
    LoanRequestedComponent,
    PresentLiabilitiesComponent,
    CommentsComponent,
    ReviewReportComponent,
    ReviewReportPopupComponent,
    ManageInspectionComponent,
    ManageReviewerComponent,
    ChangeReviewerComponent,
    ManageAccountTransfer,
    PopupAsideMenuComponent,
    PopupUnitDetailsComponent,
    ContactPersonDetailsComponent,
    KeyPersonDetailsComponent,
    PopupGuarantorDetailsComponent,
    PopupEstablishmentDetailsComponent,
    PopupCollateralDetailsComponent,
    PopupLoanRequestedComponent,
    PopupPresentLiabilitiesComponent,
    PopupCommentsComponent,
    PopupInspectionComponent,
    SearchReportComponent,
    SearchReportPopupComponent,
    SearchPopupUnitDetailsComponent,
    SearchContactPersonDetailsComponent,
    SearchKeyPersonDetailsComponent,
    SearchPopupGuarantorDetailsComponent,
    SearchPopupEstablishmentDetailsComponent,
    SearchPopupCollateralDetailsComponent,
    SearchPopupLoanRequestedComponent,
    SearchPopupPresentLiabilitiesComponent,
    SearchPopupCommentsComponent,
    SearchPopupInspectionComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpModule,
    BrowserAnimationsModule,
    RadioButtonModule,
    ButtonModule,
    CalendarModule,
    BreadcrumbModule,
    DataTableModule, 
    InputTextareaModule, 
    PanelModule, 
    DropdownModule,
    MessagesModule,
    MessageModule,
    CheckboxModule,
    AccordionModule,
    TooltipModule,
    //hammerjs,
    NgxGalleryModule,
    ConfirmDialogModule,
    DialogModule,
    GalleriaModule
  ],
  providers: [
    DIAUserInfoService,
    SmePresanctionSearchLoanService,
    SmePresanctionGetCollateralListService,
    SmePresanctionGetEstablishmentListService,
    SmePresanctionGetUnitDetailsService,
    SmePresanctionGetContactPersonDetailsService,
    SmePresanctionGetKeyPersonDetailsService,
    SmePresanctionIncompleteService,
    SmePresanctionInspectionStatusService,
    SmePresanctionGetLoanRequestedDetailsService,
    SmePresanctionGetCommentsService,
    SmePresanctionSectionNotificationService,
    {
      provide: Http, useFactory: HttpFactory,
      deps: [XHRBackend, RequestOptions]
    },
    {
      provide: LocationStrategy, 
      useClass: HashLocationStrategy}, 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
