import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'sme',
    templateUrl: './sme.component.html',
    styleUrls: ['./sme.component.scss', '../module.component.scss']
})

export class SmeComponent implements OnInit {
    private appUrl: string;

    constructor(@Inject(DOCUMENT) private document: Document) { 
        let baseAppUrl = (this.document.location.href).split('sme');
        this.appUrl = baseAppUrl[0];
        localStorage.setItem("appUrl", this.appUrl);
    }

    ngOnInit() {
        //let appUtl = 
    }
}