import { Component, OnInit } from '@angular/core';

import { SmeInspectionAddReviewersService } from '../../../../Service/SME/ManageInspection/ManageReviewer/AddReviewers/addReviewers.service';
import { SmeInspectionGetReviewersService } from '../../../../Service/SME/ManageInspection/ManageReviewer/GetReviewers/getReviewers.service';
import { SmeInspectionRemoveReviewersService } from '../../../../Service/SME/ManageInspection/ManageReviewer/RemoveReviewers/removeReviewers.service';
import { DIAUserInfoService } from '../../../../Service/User/userInfo.service';
import { ConfirmationService } from 'primeng/primeng';

import { Constants } from '../../../../Shared/Constants/constants.service';

@Component({
    selector: 'manageReviewer',
    templateUrl: './manageReviewer.component.html',
    styleUrls: ['./manageReviewer.component.scss', '../../../module.component.scss'],
    providers: [
        SmeInspectionAddReviewersService,
        SmeInspectionGetReviewersService,
        SmeInspectionRemoveReviewersService,
        DIAUserInfoService,
        ConfirmationService,
        Constants
    ]
})

export class ManageReviewerComponent implements OnInit {
    private display: boolean = false;
    private displayPFIDdata: boolean = false;
    private apiRequest: any;
    private reviewerPfId: any;
    private userInfo: any;
    private message: string;
    private pfId: any;
    private executivePFIndex: any;    
    private reviewerList: any;
    private password: any;

    constructor(private service: DIAUserInfoService, private constants: Constants, private addReviewerService: SmeInspectionAddReviewersService, private getReviewerService: SmeInspectionGetReviewersService, private removeReviewerService: SmeInspectionRemoveReviewersService, private confirmationService: ConfirmationService) { }

    ngOnInit() {
        window.scrollTo(0, 0);
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
        }
        this.getReviewerList();
    }

    showDialog() {
        this.reviewerPfId = "";
        this.displayPFIDdata = false;
        this.display = true;
    }

    searchPFID() {
        if (this.reviewerPfId) {
         //  this.password = "Hrms@123";
         //  this.apiRequest = { "pfId": +this.reviewerPfId, "password": this.password, "invokedFrom": "other" };
            this.apiRequest = { "pfId": this.reviewerPfId, "invokedFrom": "other" };
            this.service.getUserInfo(this.apiRequest).subscribe(response => {
                if (response.responseCode == 200) {
                    this.displayPFIDdata = true;
                    this.userInfo = response;
                    console.log(this.userInfo);
                } else {
                    console.log("Error");
                    this.message = response.responseMessage;
                    this.setTimeOut();
                }
                //this.userName = this.userInfo.user.firstName + this.userInfo.user.firstName
            })
        } else {
            this.message = this.constants.getMessage('PFIDValManageReview');
            this.setTimeOut();
        }
    }

    setTimeOut() {
        setTimeout(() => {
            this.message = "";
        }, 3000);
    }

    addReviewer() {
        if(this.reviewerList && this.reviewerList.length < 3){
            let validationComplete = true;
            for(let k = 0; k< this.reviewerList.length; k++){
                if(+this.reviewerList[k].reviewerPfId == +this.reviewerPfId){
                    validationComplete = false;
                }
            }
            if(validationComplete){
                if(+this.reviewerPfId == +this.pfId){
                    this.message = this.constants.getMessage('inspectorVal');
                    this.setTimeOut();
                }else{
                    if(this.userInfo.userDetails.role != 4){
                        this.message = this.constants.getMessage('reviewerAddVal');
                        this.setTimeOut();
                    }else{
                        var objectToPost: { inspectorPfId: any; reviewerPfId: any, firstName: string, lastName: string, branchCode: string, email: string  } = { inspectorPfId: this.pfId, reviewerPfId: this.reviewerPfId, firstName: this.userInfo.user.firstName, lastName: this.userInfo.user.lastName, branchCode: this.userInfo.user.brId, email: this.userInfo.user.email };
                        console.log(JSON.stringify(objectToPost));
                        this.addReviewerService.addReviewerList(objectToPost).subscribe(
                            data => {
                                if (data.responseCode == 200) {
                                    this.display = false;
                                    this.getReviewerList();
                                } else {
                                    this.message = data.responseMessage;
                                }
                                this.setTimeOut();
                            },
                            err => {
                                console.log(err);
                                this.message = this.constants.getMessage('errorMsg');
                                this.setTimeOut();
                            },
                            () => console.log('Request Completed')
                        );
                    }
                }
            }else{
                this.message = this.constants.getMessage('reviewerIDVal');
                this.setTimeOut();
            }
        }else{
            this.message = this.constants.getMessage('addReviewerVal');
            this.setTimeOut();
        }
    }

    getReviewerList(){
        var objectToPost: { inspectorPfId: any; } = { inspectorPfId: this.pfId};
        console.log(JSON.stringify(objectToPost));
        this.getReviewerService.getReviewerList(objectToPost).subscribe(
            data => {
                if (data.responseCode == 200) {
                    this.reviewerList = data.reviwerList;
                    console.log(this.reviewerList);
                } else {
                    this.message = data.responseMessage;
                }
                this.setTimeOut();
            },
            err => {
                console.log(err);
                this.message = this.constants.getMessage('errorMsg');
                this.setTimeOut();
            },
            () => console.log('Request Completed')
        );
    }

    removeReviewerList(reviewerDetail: any){
        this.confirmationService.confirm({
            //message: this.submissionStatusMessage,
            message: this.constants.getMessage('removeReviewerVal'),
            accept: () => {
                var objectToPost: { inspectorPfId: any; reviewerPfId: any, firstName: string, lastName: string, branchCode: string, email: string  } = { inspectorPfId: reviewerDetail.inspectorPfId, reviewerPfId: reviewerDetail.reviewerPfId, firstName: reviewerDetail.firstName, lastName: reviewerDetail.lastName, branchCode: reviewerDetail.branchCode, email: reviewerDetail.email };
                console.log(JSON.stringify(objectToPost));
                this.removeReviewerService.removeReviewerList(objectToPost).subscribe(
                    data => {
                        if (data.responseCode == 200) {
                            this.getReviewerList();
                        } else {
                            this.message = data.responseMessage;
                        }
                        this.setTimeOut();
                    },
                    err => {
                        console.log(err);
                        this.message = this.constants.getMessage('errorMsg');
                        this.setTimeOut();
                    },
                    () => console.log('Request Completed')
                );
            }
        });

    }
}