import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'manageInspection',
    templateUrl: './manageInspection.component.html',
    styleUrls: ['./manageInspection.component.scss', '../../module.component.scss']
})

export class ManageInspectionComponent implements OnInit {
    private appUrl:string;
    private items: any[];
    
    ngOnInit() {
        if(localStorage.getItem("appUrl") != null) {
            this.appUrl = localStorage.getItem("appUrl").toString();
        }
        
        this.items = [
            {label:'Home', url: this.appUrl + 'sme'},
            {label:'Manage Inspection'}
        ];
    }
}