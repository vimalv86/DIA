import { Component } from '@angular/core';

import { DIAUserInfoService } from '../../../../Service/User/userInfo.service';
import { SmeInspectionGetInspectionService } from '../../../../Service/SME/ManageInspection/changeReviewer/GetInspection/getInspection.service';
import { SmeInspectionChangeReviewerService } from '../../../../Service/SME/ManageInspection/changeReviewer/changeReviewerID/changeReviewer.service';
import { SmeInspectionGetReviewersService } from '../../../../Service/SME/ManageInspection/changeReviewer/GetReviewers/getReviewers.service';
import { Constants } from '../../../../Shared/Constants/constants.service';
import { DIAConstants } from '../../../../Shared/constants';

@Component({
    selector: 'changeReviewer',
    templateUrl: './changeReviewer.component.html',
    styleUrls: ['./changeReviewer.component.scss', '../../../module.component.scss'],
    providers: [
        SmeInspectionGetInspectionService,
        SmeInspectionChangeReviewerService,
        SmeInspectionGetReviewersService,
        DIAUserInfoService,
        Constants,
        DIAConstants
    ]
})

export class ChangeReviewerComponent {
    private pfId: any;
    private executivePFIndex: any;    
    private inspectionList: any;
    private message: string;
    private display: boolean = false;    
    private apiRequest: any;
    private reviewerPfId: any;
    private displayPFIDdata: boolean = false;
    private userInfo: any;
    private reviewerList: any = [];
    private checked: boolean = false;
    private reviewer: any = [];

    constructor(private service: DIAUserInfoService, private getInspectionService: SmeInspectionGetInspectionService, private constants: Constants, private changeReviewerService: SmeInspectionChangeReviewerService, private getReviewerService: SmeInspectionGetReviewersService) {
    }    

    ngOnInit() {
        window.scrollTo(0, 0);
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
        }
        this.getInspectionList();
    }

    getInspectionList(){
        var objectToPost: { appId: any; executivePFIndex: any} = { appId: "DIA", executivePFIndex: this.pfId };
        console.log(JSON.stringify(objectToPost));
        this.getInspectionService.getInspectionList(objectToPost).subscribe(
            data => {
                if (data.responseCode == 200) {
                    this.inspectionList = data.rejectedList;
                    this.inspectionList.forEach(element => {
                        element.checked = false;
                    });
                    console.log(this.inspectionList);
                } else {
                    this.message = data.responseMessage;
                }
                this.setTimeOut();
            },
            err => {
                console.log(err);
                this.message = this.constants.getMessage('errorMsg');
                this.setTimeOut();
            },
            () => console.log('Request Completed')
        );
    }

    setTimeOut() {
        setTimeout(() => {
            this.message = "";
        }, 3000);
    }

    showDialog() {
        if( this.reviewerList.length > 0){
            this.getReviewerList();
        }else{
            this.message = this.constants.getMessage('changeReviewerVal');
            this.setTimeOut();
        }
    }

    getReviewerList(){
        var objectToPost: { inspectorPfId: any; } = { inspectorPfId: this.pfId};
        console.log(JSON.stringify(objectToPost));
        this.getReviewerService.getReviewerList(objectToPost).subscribe(
            data => {
                if (data.responseCode == 200) {
                    this.reviewer = [];
                    let reviewerDetails = data.reviwerList;
                    reviewerDetails.forEach(element => {
                        let reviewerObj = {label: element.reviewerPfId, value: element.reviewerPfId}
                        this.reviewer.push(reviewerObj);
                    });
                    this.display = true;
                    this.reviewerPfId = "";
                    this.displayPFIDdata = false;
                    console.log(this.reviewer);
                } else {
                    this.message = data.responseMessage;
                }
                this.setTimeOut();
            },
            err => {
                console.log(err);
                this.message = this.constants.getMessage('errorMsg');
                this.setTimeOut();
            },
            () => console.log('Request Completed')
        );
    }

    searchPFID() {
        if (this.reviewerPfId) {
            this.apiRequest = { "pfId": this.reviewerPfId, "invokedFrom": "other" };
            this.service.getUserInfo(this.apiRequest).subscribe(response => {
                if (response.responseCode == 200) {
                    this.displayPFIDdata = true;
                    this.userInfo = response;
                    console.log(this.userInfo);
                } else {
                    console.log("Error");
                    this.message = response.responseMessage;
                    this.setTimeOut();
                }
            })
        } else {
            this.message = this.constants.getMessage('PFIDValManageReview');
            this.setTimeOut();
        }
    }

    selectAll(){
        if(this.checked){
            this.reviewerList = [];
            this.inspectionList.forEach(element => {
                element.checked = true;
                this.reviewerList.push(element.inspectionId);                
            });
        }else{
            this.reviewerList = [];
            this.inspectionList.forEach(element => {
                element.checked = false;
            });
        }
        
    }

    selectReviewer(reviewerDetail){
        if(reviewerDetail.checked){
            if(!(this.reviewerList.indexOf(reviewerDetail.inspectionId) > -1)){
                this.reviewerList.push(reviewerDetail.inspectionId);                
            }
        }else{
            this.checked = false;
            if (!(this.reviewerList.indexOf(reviewerDetail.inspectionId) == -1)) {
                this.reviewerList.splice(this.reviewerList.indexOf(reviewerDetail.inspectionId), 1);
              }
        }
    }

    changeReviewer(){
        var objectToPost: { appId: any; executivePFIndex: any; inspectIonList: any; reviwerPfId: any;} = { appId: "DIA", executivePFIndex: this.pfId, inspectIonList: this.reviewerList, reviwerPfId: this.reviewerPfId };
        console.log(JSON.stringify(objectToPost));
        this.changeReviewerService.changeReviewer(objectToPost).subscribe(
            data => {
                if (data.responseCode == 200) {
                    this.display = false;
                    this.reviewerList = [];
                   this.getInspectionList();
                    console.log(this.inspectionList);
                } else {
                    this.message = data.responseMessage;
                }
                this.setTimeOut();
            },
            err => {
                console.log(err);
                this.message = this.constants.getMessage('errorMsg');
                this.setTimeOut();
            },
            () => console.log('Request Completed')
        );
    }
}