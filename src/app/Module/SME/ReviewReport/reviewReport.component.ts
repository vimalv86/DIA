import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { ReviewReport } from '../../../Service/SME/ReviewReport/reviewReport';

import { SmeReviewGetReviewReportsService } from '../../../Service/SME/ReviewReport/GetReviewReports/getReviewReports.service';
import { Constants } from '../../../Shared/Constants/constants.service';

@Component({
    selector: 'reviewReport',
    templateUrl: './reviewReport.component.html',
    styleUrls: ['./reviewReport.component.scss', '../../module.component.scss'],
    providers: [
        ReviewReport,
        SmeReviewGetReviewReportsService,
        Constants
    ]
})

export class ReviewReportComponent implements OnInit {
    private appUrl:string;
    private items: any[];
    private reviewList: any[];
    private pfId: any;
    private executivePFIndex: any;  
    private message: string;

    constructor(private router: Router, private getReviewReportService: SmeReviewGetReviewReportsService, private constants: Constants) {}

    ngOnInit() {
        if(localStorage.getItem("appUrl") != null) {
            this.appUrl = localStorage.getItem("appUrl").toString();
        }
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
        }
        this.getReviewInspectionList();
        this.items = [
            {label:'Home', url: this.appUrl + 'sme'},
            {label:'Review Reports'}
        ];
    }

    viewReport(reviewDetails) {
        localStorage.setItem("borrowerRefID", null);
        localStorage.setItem("loanNo", null);
        localStorage.setItem("inspectionID", null);
        localStorage.setItem("borrowerRefID", JSON.stringify(reviewDetails.borrowerRefId));
        localStorage.setItem("loanNo", JSON.stringify(reviewDetails.loanNum));
        localStorage.setItem("inspectionID", JSON.stringify(reviewDetails.inspectionId));
        localStorage.setItem("inspectionType", JSON.stringify(reviewDetails.inspectionType));
        console.log("viewReport");
        this.router.navigate(['/sme/reviewReport/reviewReportPopup', { inspectionID: reviewDetails.inspectionId, inspectionDate: reviewDetails.inspectionDate }]);
    }

    getReviewInspectionList(){
        var objectToPost: { appId: any; executivePFIndex: any; } = { appId: "DIA", executivePFIndex: this.pfId };
        console.log(JSON.stringify(objectToPost));
        this.getReviewReportService.getReviewReportList(objectToPost).subscribe(
            data => {
                if (data.responseCode == 200) {
                    this.reviewList = data.rejectedList;
                    console.log(this.reviewList);
                } else {
                    this.message = data.responseMessage;
                }
                this.setTimeOut();
            },
            err => {
                console.log(err);
                this.message = this.constants.getMessage('errorMsg');
                this.setTimeOut();
            },
            () => console.log('Request Completed')
        );
    }

    setTimeOut() {
        setTimeout(() => {
            this.message = "";
        }, 3000);
    }
    

}