import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { SubmitInspectionService } from '../../../../../Service/SME/ReviewReport/Inspection/SubmitInspection/submitInspection.service';
import { Constants } from '../../../../../Shared/Constants/constants.service';

@Component({
  selector: 'popupInspection',
  templateUrl: './inspection.component.html',
  styleUrls: ['./inspection.component.scss', '../../../../module.component.scss'],
  providers: [
    SubmitInspectionService,
    Constants
  ]
})

export class PopupInspectionComponent {
  private reviewStatus: any;
  private comments: any;
  private inspectionID: string;
  private apiRequest: object;
  private pfId: string;
  private statusMsg: string;
  private message: string;
  private executivePFIndex: any;
  private inspectionType: any;
  private status: boolean;

  constructor(private router: Router, private service: SubmitInspectionService, private constants: Constants) { }

  submitReview() {
    if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
      this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
      this.pfId = this.executivePFIndex.user.pfId.toString();
    }
    if (this.reviewStatus && this.comments) {
      if (localStorage.getItem("inspectionID") != "null" && localStorage.getItem("inspectionID") != null) {
        this.inspectionID = JSON.parse(localStorage.getItem("inspectionID"));
        this.inspectionType = JSON.parse(localStorage.getItem("inspectionType"));
        if(+this.reviewStatus == 1){
          this.status = true;
        }else{
          this.status = false;
        }
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "inspectionId": this.inspectionID, "reviewComment": this.comments, "inspectionType": this.inspectionType, "accepted": this.status};
        this.service.postComment(this.apiRequest).subscribe(response => {
          if (response.responseCode == 200) {
            this.statusMsg = "success";
            this.message = this.constants.getMessage('inspectionSubmitSuccess');
            this.router.navigate(['/sme']);
             setTimeout(() => {
                this.router.navigate(['sme/reviewReport']);
            }, 10);
          } else {
            this.statusMsg = "error";
            this.message = this.constants.getMessage('errorMsg');
          }
          this.setTimeOut();
        })
      }
    } else {
      this.statusMsg = "error";
      this.message = this.constants.getMessage('submitInspectVal');
      this.setTimeOut();
    }
  }

   setTimeOut() {
        setTimeout(() => {
            this.message = "";
        }, 3000);
    }
}