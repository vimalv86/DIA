import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { SmePresanctionGetLiabilityDetailsService } from '../../../../../Service/SME/ReviewReport/PresentLiabilities/GetLiabilityDetails/getLiabilityDetails.service';

@Component({
    selector: 'popupPresentLiabilities',
    templateUrl: './presentLiabilities.component.html',
    styleUrls: ['./presentLiabilities.component.scss', '../../../../module.component.scss'],
     providers: [
      SmePresanctionGetLiabilityDetailsService,
    ]
})

export class PopupPresentLiabilitiesComponent {
  private borrowerRefId: string;
  private apiRequest: object;
  private pfId: string;
  private statusMsg: string;
  private executivePFIndex: any;  
  private liabilityList: any;
  private noData: any;

  constructor(private router: Router, private service: SmePresanctionGetLiabilityDetailsService) { }

 ngOnInit() {
    window.scrollTo(0, 0);
    if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
      this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
      this.pfId = this.executivePFIndex.user.pfId.toString();
    }
    if (localStorage.getItem("borrowerRefID") != null && localStorage.getItem("borrowerRefID") != "null") {
    this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
     this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };
        this.service.getLiabilityDetails(this.apiRequest).subscribe(response => {
            if (response.borrowerLiabilityList && response.borrowerLiabilityList.length > 0) {
                this.liabilityList = response.borrowerLiabilityList;
                
            }else{
                this.noData = true;
            }
        })
    }
  }

  back(): void {
    this.router.navigate(['sme/reviewReport/reviewReportPopup/loanRequested']);
  }

  next(): void {
    this.router.navigate(['sme/reviewReport/reviewReportPopup/comments']);
  }

}