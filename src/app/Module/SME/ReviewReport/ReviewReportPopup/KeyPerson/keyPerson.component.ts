import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { Constants } from '../../../../../Shared/Constants/constants.service';
import { SmePresanctionGetKeyPersonDetailsService } from '../../../../../Service/SME/ReviewReport/BorrowerDetails/KeyPersonDetails/GetKeyPersonDetails/getKeyPersonDetails.service';

@Component({
    selector: 'keyPersonDetails',
    templateUrl: './keyPerson.component.html',
    styleUrls: ['./keyPerson.component.scss', '../../../../module.component.scss'],
    providers: [
      SmePresanctionGetKeyPersonDetailsService,
      Constants
    ]
})

export class KeyPersonDetailsComponent {
  private borrowerRefId: string;
  private apiRequest: object;
  private pfId: string;
  private statusMsg: string;
  private executivePFIndex: any;  
  private keyPersonList: any;
  private keyPersonDetail: any;
  private keyPersonNo: any;
  private noData: any;

  constructor(private route: ActivatedRoute,private router: Router, private service: SmePresanctionGetKeyPersonDetailsService, private constants: Constants) { 
  }

  ngOnInit(){
    this.route
    .params
    .subscribe(params => {
        this.keyPersonNo = params['keyPersonNo'];
        if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
          this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
          this.pfId = this.executivePFIndex.user.pfId.toString();
        }
        if (localStorage.getItem("borrowerRefID") != null && localStorage.getItem("borrowerRefID") != "null") {
        this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };        
        this.service.getKeyPersonDetails(this.apiRequest).subscribe(response => {
            if (response.borrowerPersonDetail) {
                this.keyPersonList = response.borrowerPersonDetail;
                this.keyPersonDetail = this.keyPersonList[this.keyPersonNo];
                console.log(this.keyPersonList);
            }else{
              this.noData = true;
            }
        })
       }
    });    
  }
}