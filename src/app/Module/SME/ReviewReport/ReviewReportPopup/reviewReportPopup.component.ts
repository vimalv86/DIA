import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'reviewReportPopup',
  templateUrl: './reviewReportPopup.component.html',
  styleUrls: ['./reviewReportPopup.component.scss', '../../../module.component.scss']
})

export class ReviewReportPopupComponent implements OnInit {
  private inspectionID: string;
  private inspectionDate: any;
  constructor(private router: Router, private route: ActivatedRoute) {

  }

  ngAfterViewInit() {
    this.router.events.subscribe((event) => {
      const mainDiv = document.getElementById('review-report-popup');
      if (mainDiv) {
        mainDiv.scrollTop = 0;
      }
    });
  }

  ngOnInit() {
    this.route
      .params
      .subscribe(params => {
        this.inspectionID = params['inspectionID'];
        this.inspectionDate = params['inspectionDate'];
      });
  }

  closePopup() {
    this.router.navigate(['sme/reviewReport']);
  }
}