import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { ReviewReport } from '../../../Service/SME/ReviewReport/reviewReport';

import { SmeSearchGetSearchReportsService } from '../../../Service/SME/SearchReport/GetSearchReports/getSearchReports.service';
import { Constants } from '../../../Shared/Constants/constants.service';

@Component({
    selector: 'searchReport',
    templateUrl: './searchReport.component.html',
    styleUrls: ['./searchReport.component.scss', '../../module.component.scss'],
    providers: [
        ReviewReport,
        SmeSearchGetSearchReportsService,
        Constants
    ]
})

export class SearchReportComponent implements OnInit {
    private appUrl:string;
    private items: any[];
    private reviewList: any[];
    private pfId: any;
    private executivePFIndex: any;  
    private message: string;
    private inspectionTypeList: any;
    private rangeDates: any;
    private reviewListDupl: any;
    private selectedRange: any;

    constructor(private router: Router, private getSearchReportService: SmeSearchGetSearchReportsService, private constants: Constants) {}

    ngOnInit() {
        if(localStorage.getItem("appUrl") != null) {
            this.appUrl = localStorage.getItem("appUrl").toString();
        }
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
        }
        this.getInspectionList();
        this.items = [
            {label:'Home', url: this.appUrl + 'sme'},
            {label:'Search Reports'}
        ];
    }

    viewReport(details) {
        localStorage.setItem("borrowerRefID", null);
        localStorage.setItem("loanNo", null);
        localStorage.setItem("inspectionID", null);
        localStorage.setItem("borrowerRefID", JSON.stringify(details.borrowerRefId));
        localStorage.setItem("loanNo", JSON.stringify(details.losNum));
        localStorage.setItem("inspectionID", JSON.stringify(details.inspectionId));
        localStorage.setItem("inspectionType", JSON.stringify(details.inspectionType));
        console.log("viewReport");
        this.router.navigate(['/sme/searchReport/searchReportPopup', { inspectionID: details.inspectionId, inspectionDate: details.inspectionDate }]);
    }

    getInspectionList(){
        var objectToPost: { appId: any; executivePFIndex: any; } = { appId: "DIA", executivePFIndex: this.pfId };
        console.log(JSON.stringify(objectToPost));
        this.getSearchReportService.getSearchReportList(objectToPost).subscribe(
            data => {
                if (data.responseCode == 200) {
                    this.reviewList = data.rejectedList;
                    this.reviewListDupl = data.rejectedList;
                    console.log(this.reviewList);
                    this.inspectionTypeList = [];
                        this.inspectionTypeList.push({ label: "All", value: '' });
                        for (let k = 0; k < this.reviewList.length; k++) {
                            let reviewStatusObj = {
                                label: this.reviewList[k].inspectionType,
                                value: this.reviewList[k].inspectionType
                            }
                            this.inspectionTypeList.push(reviewStatusObj);
                        }
                        this.inspectionTypeList = this.inspectionTypeList.filter((object, index, duplicate) =>
                            index === duplicate.findIndex((t) => (
                                t.label === object.label
                            ))
                        )
                } else {
                    this.message = data.responseMessage;
                }
                this.setTimeOut();
            },
            err => {
                console.log(err);
                this.message = this.constants.getMessage('errorMsg');
                this.setTimeOut();
            },
            () => console.log('Request Completed')
        );
    }

    selectDateRange(){
        if(this.rangeDates[1]){
             let startDate = new Date(this.rangeDates[0]);
             let endDate = new Date(this.rangeDates[1].getTime() + 60 * 60 * 24 * 1000).getTime();
             this.reviewList = this.reviewListDupl.filter(
            data => data.inspectionDate >= startDate && data.inspectionDate <= endDate);
        }
    }

    clearFilter(){
        this.rangeDates = '';
        this.reviewList = this.reviewListDupl;
        this.selectedRange = '';
    }

    setTimeOut() {
        setTimeout(() => {
            this.message = "";
        }, 3000);
    }
    

}