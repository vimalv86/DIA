import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { SmeSearchReportsGetContactPersonDetailsService } from '../../../../../Service/SME/SearchReport/BorrowerDetails/ContactPersonDetails/GetContactPersonDetails/getContactPersonDetails.service';
import { Constants } from '../../../../../Shared/Constants/constants.service';

@Component({
  selector: 'contactPersonDetails',
  templateUrl: './contactPerson.component.html',
  styleUrls: ['./contactPerson.component.scss', '../../../../module.component.scss'],
  providers: [
    SmeSearchReportsGetContactPersonDetailsService,
    Constants
  ]
})

export class SearchContactPersonDetailsComponent {
  private borrowerRefId: string;
  private apiRequest: object;
  private pfId: string;
  private statusMsg: string;
  private executivePFIndex: any;  
  private contactPersonDetails: any;
  private noData: boolean = false;

  constructor(private router: Router, private service: SmeSearchReportsGetContactPersonDetailsService, private constants: Constants) { }


  ngOnInit() {
    if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
      this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
      this.pfId = this.executivePFIndex.user.pfId.toString();
    }
    if (localStorage.getItem("borrowerRefID") != null && localStorage.getItem("borrowerRefID") != "null") {
      this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
      this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };

      this.service.getContactPersonDetails(this.apiRequest).subscribe(response => {
        if (response.borrowerPersonDetail) {
          this.contactPersonDetails = response.borrowerPersonDetail;
        }else{
          this.noData = true;
        }
      })
    }
  }

  back(): void {
    this.router.navigate(['sme/reviewReport/reviewReportPopup/unitDetails']);
  }

  next(): void {
    this.router.navigate(['sme/reviewReport/reviewReportPopup/keyPersonDetails']);
  }
}