import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { SmeSearchReportsGetUnitDetailsService } from '../../../../../Service/SME/SearchReport/BorrowerDetails/UnitDetails/GetUnitDetails/getUnitDetails.service';
import { Constants } from '../../../../../Shared/Constants/constants.service';

@Component({
  selector: 'popupUnitDetails',
  templateUrl: './unitDetails.component.html',
  styleUrls: ['./unitDetails.component.scss', '../../../../module.component.scss'],
  providers: [
    SmeSearchReportsGetUnitDetailsService,
    Constants
  ]
})

export class SearchPopupUnitDetailsComponent {

  private borrowerRefId: string;
  private apiRequest: object;
  private unitDetails: any;
  private pfId: string;
  private statusMsg: string;
  private executivePFIndex: any;  
  private noData: boolean = false;

  constructor(private router: Router, private service: SmeSearchReportsGetUnitDetailsService, private constants: Constants) { }


  ngOnInit() {
    window.scrollTo(0, 0);
    if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
      this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
      this.pfId = this.executivePFIndex.user.pfId.toString();
    }
    if (localStorage.getItem("borrowerRefID") != null && localStorage.getItem("borrowerRefID") != "null") {
      this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
      this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };
      this.service.getUnitDetails(this.apiRequest).subscribe(response => {
        if (response.borrowerUnit) {
          this.unitDetails = response.borrowerUnit;
        }else{
          this.noData = true;
        }
      })
    }
    else {
      this.statusMsg = this.constants.getMessage('borrowerVal');
      console.log(this.statusMsg);
    }
  }

  next(): void {
    this.router.navigate(['sme/reviewReport/reviewReportPopup/contactPersonDetails']);
  }
}