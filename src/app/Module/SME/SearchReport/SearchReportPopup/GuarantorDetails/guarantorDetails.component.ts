import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { Constants } from '../../../../../Shared/Constants/constants.service';
import { SmeSearchReportsGetGuarantorDetailsService } from '../../../../../Service/SME/SearchReport/GuarantorDetails/GetGuarantorDetails/getGuarantorDetails.service';

@Component({
    selector: 'popupGuarantorDetails',
    templateUrl: './guarantorDetails.component.html',
    styleUrls: ['./guarantorDetails.component.scss', '../../../../module.component.scss'],
    providers: [
      SmeSearchReportsGetGuarantorDetailsService,
      Constants
    ]
})

export class SearchPopupGuarantorDetailsComponent {
  private borrowerRefId: string;
  private apiRequest: object;
  private pfId: string;
  private statusMsg: string;
  private executivePFIndex: any;  
  private guarantorList: any;
  private guarantorDetail: any;
  private guarantorNo: any;
  private noData: any;

  constructor(private route: ActivatedRoute,private router: Router, private service: SmeSearchReportsGetGuarantorDetailsService, private constants: Constants) { }

ngOnInit(){
   this.route
    .params
    .subscribe(params => {
        this.guarantorNo = params['guarantorNo'];
        if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
          this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
          this.pfId = this.executivePFIndex.user.pfId.toString();
        }
        if (localStorage.getItem("borrowerRefID") != null && localStorage.getItem("borrowerRefID") != "null") {
        this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };
        this.service.getGuarantorDetails(this.apiRequest).subscribe(response => {
            if (response.guarantorList && response.guarantorList.length != 0) {
                this.guarantorList = response.guarantorList;
                this.guarantorDetail = this.guarantorList[this.guarantorNo];
                console.log(this.guarantorList);
            }else{
              this.noData = true;
            }
        })
       }
    });
  }
}
