import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { Constants } from '../../../../../Shared/Constants/constants.service';
import { SmeSearchReportsGetEstablishmentListService } from '../../../../../Service/SME/SearchReport/EstablishmentDetails/GetEstablishmentDetails/getEstablishmentDetails.service';

@Component({
  selector: 'popupEstablishmentDetails',
  templateUrl: './establishmentDetails.component.html',
  styleUrls: ['./establishmentDetails.component.scss', '../../../../module.component.scss'],
  providers: [
    SmeSearchReportsGetEstablishmentListService,
    Constants
  ]
})

export class SearchPopupEstablishmentDetailsComponent {
  //private loanNumber: string;
  private inspectionID: string;
  private apiRequest: object;
  private pfId: string;
  private statusMsg: string;
  private executivePFIndex: any;
  private establishmentList: any;
  private establishmentDetail: any;
  private establishmentNo: any;
  private  images:  any[];
  private noData: any;

  constructor(private route: ActivatedRoute, private router: Router, private service: SmeSearchReportsGetEstablishmentListService, private constants: Constants) { }

  ngOnInit() {
    this.route
      .params
      .subscribe(params => {
        this.establishmentNo = params['establishmentNo'];
        if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
          this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
          this.pfId = this.executivePFIndex.user.pfId.toString();
        }
        if (localStorage.getItem("inspectionID") != null && localStorage.getItem("inspectionID") != "null") {
          // this.loanNumber = JSON.parse(localStorage.getItem('loanNo'));
          this.inspectionID = JSON.parse(localStorage.getItem("inspectionID"));
          this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.inspectionID };
          this.service.getEstablishmentList(this.apiRequest).subscribe(response => {
            if (response.establishmentWebVOList && response.establishmentWebVOList.length > 0) {
              this.images = [];
              this.establishmentList = response.establishmentWebVOList;
              this.establishmentDetail = this.establishmentList[this.establishmentNo];
              for (let k = 0; k < this.establishmentDetail.imageList.length; k++) {
                let latitudeAndLongitude = "Latitude: " + this.establishmentDetail.imageList[k].imageLatitude + ", " + "Longitude: " + this.establishmentDetail.imageList[k].imageLongitude;
                this.images.push({ source: this.establishmentDetail.imageList[k].imagePath, alt: latitudeAndLongitude });
              }
              console.log(this.establishmentList);
            } else {
              this.noData = true;
            }
          })
        }
      });
  }
}
