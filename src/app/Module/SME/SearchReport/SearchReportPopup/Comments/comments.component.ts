import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { SmeSearchReportsGetCommentsService } from '../../../../../Service/SME/SearchReport/Comments/GetComments/getComments.service';


@Component({
    selector: 'popupComments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.scss', '../../../../module.component.scss'],
     providers: [
      SmeSearchReportsGetCommentsService,
    ]
})

export class SearchPopupCommentsComponent {
  private inspectionID: string;
  private apiRequest: object;
  private pfId: string;
  private statusMsg: string;
  private executivePFIndex: any;  
  private comments: any;
  private noData: boolean = false;

  constructor(private router: Router, private service: SmeSearchReportsGetCommentsService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
      this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
      this.pfId = this.executivePFIndex.user.pfId.toString();
    }
     if (localStorage.getItem("inspectionID") != "null" && localStorage.getItem("inspectionID") != null) {
    this.inspectionID = JSON.parse(localStorage.getItem("inspectionID"));
    this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.inspectionID };
    this.service.getComments(this.apiRequest).subscribe(response => {
            if (response.officialCommentsVO) {
                this.comments = response.officialCommentsVO;
                console.log(this.comments);
            }else{
              this.noData = true;
            }
        })
    }
  }

  back(): void {
    this.router.navigate(['sme/reviewReport/reviewReportPopup/presentLiabilities']);
  }

  next(): void {
    this.router.navigate(['sme/reviewReport/reviewReportPopup/inspection']);
  }

}