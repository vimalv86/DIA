import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { getInspectionDetailsService } from '../../../../../Service/SME/SearchReport/Inspection/getInspection/getInspectionDetails.service';
import { Constants } from '../../../../../Shared/Constants/constants.service';

@Component({
  selector: 'popupInspection',
  templateUrl: './inspection.component.html',
  styleUrls: ['./inspection.component.scss', '../../../../module.component.scss'],
  providers: [
    getInspectionDetailsService,
    Constants
  ]
})

export class SearchPopupInspectionComponent {
  private executivePFIndex: any;
  private pfId: any;
  private apiRequest: any;
  private noData: any;
  private inspaectionDetail: any;
  private message: any;
  private loanStatus: any;
  private comments: any;

  constructor(private router: Router, private service: getInspectionDetailsService, private constants: Constants) { }

  ngOnInit() {
    if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
      this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
      this.pfId = this.executivePFIndex.user.pfId.toString();
    }
     if (localStorage.getItem("inspectionID") != null && localStorage.getItem("inspectionID") != "null") {
      this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "serviceKey": JSON.parse(localStorage.getItem("inspectionID")) };
      this.service.postComment(this.apiRequest).subscribe(response => {
        if (response.responseCode == 200) {
          this.loanStatus = response.loandetails.loanStatus;
          
          if(this.loanStatus == 'WebIncomplete') {
            this.loanStatus = 'Web Incomplete';
          } else if(this.loanStatus == 'MobIncomplete') {
            this.loanStatus = 'Mob Incomplete';
          } else if(this.loanStatus == 'PendingForReview') {
            this.loanStatus = 'Pending For Review';
          }
          
          this.comments = response.loandetails.reviewComment;
        } else {
          this.noData = true;
        }
      })
    }
  }

  setTimeOut() {
    setTimeout(() => {
      this.message = "";
    }, 3000);
  }
}