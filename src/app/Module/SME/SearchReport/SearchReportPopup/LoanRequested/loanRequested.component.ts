import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { SmeSearchReportsGetLoanRequestedDetailsService } from '../../../../../Service/SME/SearchReport/LoanRequested/GetLoanRequestedDetails/getLoanRequestedDetails.service';

@Component({
    selector: 'popupLoanRequested',
    templateUrl: './loanRequested.component.html',
    styleUrls: ['./loanRequested.component.scss', '../../../../module.component.scss'],
     providers: [
      SmeSearchReportsGetLoanRequestedDetailsService,
    ]
})

export class SearchPopupLoanRequestedComponent {
  private borrowerRefId: string;
  private apiRequest: object;
  private pfId: string;
  private statusMsg: string;
  private executivePFIndex: any;  
  private loanRequestedList: any;
  private noData: any;

  constructor(private router: Router, private service: SmeSearchReportsGetLoanRequestedDetailsService) { }

 ngOnInit() {
    window.scrollTo(0, 0);
    if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
      this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
      this.pfId = this.executivePFIndex.user.pfId.toString();
    }
    if (localStorage.getItem("borrowerRefID") != null && localStorage.getItem("borrowerRefID") != "null") {
    this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
     this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };
        this.service.getLoanRequestedDetails(this.apiRequest).subscribe(response => {
            if (response.loanRequestedDetails && response.loanRequestedDetails.length > 0) {
                this.loanRequestedList = response.loanRequestedDetails               
            }else{
                this.noData = true;
            }
        })
    }
  }

  back(): void {
    this.router.navigate(['sme/reviewReport/reviewReportPopup/loanRequested']);
  }

  next(): void {
    this.router.navigate(['sme/reviewReport/reviewReportPopup/comments']);
  }
}