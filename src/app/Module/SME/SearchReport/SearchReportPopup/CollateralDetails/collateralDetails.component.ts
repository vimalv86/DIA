import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { Constants } from '../../../../../Shared/Constants/constants.service';
import { SmeSearchReportsGetCollateralListService } from '../../../../../Service/SME/SearchReport/CollateralDetails/GetCollateralDetails/getCollateralDetails.service';


@Component({
    selector: 'popupCollateralDetails',
    templateUrl: './collateralDetails.component.html',
    styleUrls: ['./collateralDetails.component.scss', '../../../../module.component.scss'],
    providers: [
      SmeSearchReportsGetCollateralListService,
      Constants
    ]
})

export class SearchPopupCollateralDetailsComponent {
//  private loanNumber: string;
  private inspectionID: string;
  private apiRequest: object;
  private pfId: string;
  private statusMsg: string;
  private executivePFIndex: any;  
  private collateralList: any;
  private collateralDetail: any;
  private collateralNo: any;
  private images: any[]; 
  private noData: any = false;

  constructor(private route: ActivatedRoute,private router: Router, private service: SmeSearchReportsGetCollateralListService, private constants: Constants) { }

    ngOnInit(){
     this.route
    .params
    .subscribe(params => {
        this.collateralNo = params['collateralNo'];
        if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
          this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
          this.pfId = this.executivePFIndex.user.pfId.toString();
        }
        if (localStorage.getItem("inspectionID") != null && localStorage.getItem("inspectionID") != "null") {
       // this.loanNumber = JSON.parse(localStorage.getItem('loanNo'));
        this.inspectionID = JSON.parse(localStorage.getItem("inspectionID"));
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.inspectionID };
        this.service.getCollateralList(this.apiRequest).subscribe(response => {
                if (response.collateralWebList) {
                  this.images = [];
                    this.collateralList = response.collateralWebList;
                    this.collateralDetail = this.collateralList[this.collateralNo];
                    if(this.collateralDetail && this.collateralDetail.imageList.length>0){
                      for(let k = 0; k < this.collateralDetail.imageList.length; k++){
                        let latitudeAndLongitude = "Latitude: " + this.collateralDetail.imageList[k].imageLatitude + ", " + "Longitude: " + this.collateralDetail.imageList[k].imageLongitude;
                        this.images.push({source: this.collateralDetail.imageList[k].imagePath, alt: latitudeAndLongitude});
                      }
                    }
                }else{
                  this.noData = true;
                }
            })
       }
    });
  }

}
