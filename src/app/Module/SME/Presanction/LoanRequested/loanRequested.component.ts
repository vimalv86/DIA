import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DIAConstants } from '../../../../Shared/constants';
import { Constants } from '../../../../Shared/Constants/constants.service';
import { UtilsService } from '../../../../Shared/UtilsService/utils.service';

import { LoanRequestedDetails } from '../../../../Service/SME/Presanction/LoanRequested/loanRequestedDetails';
import { AddLoanRequestedDetailsService } from '../../../../Service/SME/Presanction/LoanRequested/AddLoanRequestedDetails/addLoanRequestedDetails.service';
import { SmePresanctionGetLoanRequestedDetailsService } from '../../../../Service/SME/Presanction/LoanRequested/GetLoanRequestedDetails/getLoanRequestedDetails.service';

@Component({
    selector: 'loanRequested',
    templateUrl: './loanRequested.component.html',
    styleUrls: ['./loanRequested.component.scss'],
    providers: [
        LoanRequestedDetails,
        AddLoanRequestedDetailsService,
        SmePresanctionGetLoanRequestedDetailsService,
        UtilsService,
        Constants
    ]
})

export class LoanRequestedComponent implements OnInit {
    private apiRequest: any;
    private responseStatus: Object = [];
    private loanRequestedList: any = [];
    private executivePFIndex: any;
    private borrowerRefId: string;
    private status: boolean;
    private message: string;
    private statusMsg: string;
    private noData: boolean;
    private loanReqType: any = [];
    private purposeOfLoan: any = [];
    private pfId: string;
    private isOtherLoanPurposeEnabled: boolean;
    private isOtherLoanTypeEnabled: boolean;
    private isValidationComplete: boolean;
    private isLoanEditable: boolean;
    private loanDetailsWithOutInspection: any;
    private otherVal: boolean = false;
    private purposeVal: boolean = false;
    private limitVal: boolean = false;
    private otherValBlur: boolean = false;
    private purposeValBlur: boolean = false;
    private limitValBlur: boolean = false;
    private onlyNumber: string;
    private onlyAlphabet: string;
    private avoidSpecialChar: string;

    constructor(private _addLoanRequestedDetailsService: AddLoanRequestedDetailsService, private service: SmePresanctionGetLoanRequestedDetailsService, private route: ActivatedRoute, private utilsService: UtilsService, private constants: Constants) {
        this.loanReqType = DIAConstants.loanReqType;
        this.purposeOfLoan = DIAConstants.purposeOfLoan;
    }

    @Input() loanRequestedDetails: LoanRequestedDetails;

    addLoanRequestedDetail() {
        if (this.borrowerRefId) {
            this.isValidationComplete = true;
            if (this.loanRequestedDetails.loanReqType && this.loanRequestedDetails.purposeOfLoan && this.loanRequestedDetails.limitRequested) {
                if (this.loanRequestedDetails.loanReqType == "Others") {
                    if (!this.loanRequestedDetails.otherLoan) {
                        this.isValidationComplete = false;
                    }
                }
                if (this.loanRequestedDetails.purposeOfLoan == "Others") {
                    if (!this.loanRequestedDetails.otherPurposeOfLoan) {
                        this.isValidationComplete = false;
                    }
                }
                if (this.isValidationComplete) {
                    //console.log(this.borrowerRefId);
                    this.loanRequestedDetails["borrowerRefId"] = this.borrowerRefId;

                    var objectToPost: { loanRequestedDetails: object; appId: string; executivePFIndex: string; } = { loanRequestedDetails: this.loanRequestedDetails, appId: "WEB-DIA", executivePFIndex: this.pfId };

                    console.log(JSON.stringify(objectToPost));
                    this._addLoanRequestedDetailsService.postComment(objectToPost).subscribe(
                        data => {
                            console.log(this.responseStatus = data);
                            if (data.responseMessage == "Successful") {
                                this.message = this.constants.getMessage('successMsg');
                                this.statusMsg = "success";
                                this.getLoanList();
                            } else {
                                this.message = data.responseMessage;
                                this.statusMsg = "error";
                            }
                        },
                        err => {
                            console.log(err);
                            this.message = this.constants.getMessage('errorMsg');
                            this.statusMsg = "error";
                        },
                        () => console.log('Request Completed')
                    );
                    this.status = true;
                    this.setTimeOut();
                } else {
                    this.message = this.constants.getMessage('requiredFieldVal');
                    this.statusMsg = "error";
                    this.setTimeOut();
                }
            } else {
                this.message = this.constants.getMessage('requiredFieldVal');
                this.statusMsg = "error";
                this.setTimeOut();
            }
        } else {
            this.message = this.constants.getMessage('unitDetailVal');
            this.statusMsg = "error";
            this.setTimeOut();
        }
    }

    setTimeOut() {
        setTimeout(() => {
            this.message = "";
            this.onlyNumber = "";
            this.onlyAlphabet = "";
            this.avoidSpecialChar = "";
        }, 3000);
    }


    ngOnInit() {
        window.scrollTo(0, 0);
        if (localStorage.getItem("isLoanEditable") != null && localStorage.getItem("isLoanEditable") != "null") {
            if (localStorage.getItem("isLoanEditable") == "true") {
                this.isLoanEditable = true;
            } else {
                this.isLoanEditable = false;
            }
        }
        this.loanRequestedDetails = new LoanRequestedDetails();
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null && localStorage.getItem("borrowerRefID") != "null" && localStorage.getItem("borrowerRefID") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
            this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
            this.getLoanList();
        } else {
            this.getDataFromLocalStorage();
        }
    }

    getLoanList() {
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };
        this.service.getLoanRequestedDetails(this.apiRequest).subscribe(response => {
            if (response.loanRequestedDetails) {
                this.loanRequestedList = response.loanRequestedDetails

                this.loanRequestedDetails.loanReqType = "";
                this.loanRequestedDetails.otherLoan = "";
                this.loanRequestedDetails.purposeOfLoan = "";
                this.loanRequestedDetails.otherPurposeOfLoan = "";
                this.loanRequestedDetails.limitRequested = "";
            } else {
                this.getDataFromLocalStorage();
            }
        })
    }

    getDataFromLocalStorage() {
        this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
        if (this.loanDetailsWithOutInspection) {
            if (this.loanDetailsWithOutInspection.loanInspectionList && this.loanDetailsWithOutInspection.loanInspectionList.length > 0) {
                this.loanRequestedList = this.loanDetailsWithOutInspection.loanInspectionList;
                this.loanRequestedDetails.loanReqType = "";
                this.loanRequestedDetails.otherLoan = "";
                this.loanRequestedDetails.purposeOfLoan = "";
                this.loanRequestedDetails.otherPurposeOfLoan = "";
                this.loanRequestedDetails.limitRequested = "";
            } else {
                this.noData = true;
            }
        } else {
            this.noData = true;
        }
    }



    loanPurposeSelection(data) {
        if (data == "Others") {
            this.isOtherLoanPurposeEnabled = true;
        } else {
            this.isOtherLoanPurposeEnabled = false;
            this.loanRequestedDetails.otherPurposeOfLoan = null;
        }
    }

    loanTypeSelection(data) {
        if (data == "Others") {
            this.isOtherLoanTypeEnabled = true;
        } else {
            this.isOtherLoanTypeEnabled = false;
            this.loanRequestedDetails.otherLoan = null;
        }
    }

    keypressCheck(data, field) {
        this.otherVal = false;
        this.purposeVal = false;
        this.limitVal = false;
        switch (field) {
            case "Other":
                if (this.utilsService.validString) {
                    this.otherVal = true;
                } else {
                    this.otherVal = false;
                }
                break;

            case "Purpose":
                if (this.utilsService.validString) {
                    this.purposeVal = true;
                } else {
                    this.purposeVal = false;
                }
                break;

            case "Limit":
                if (this.utilsService.validString) {
                    this.limitVal = true;
                } else {
                    this.limitVal = false;
                }
                break;
        }
        return data;
    }

    onBlurValidation(value, field) {
        if (value) {
            this.otherValBlur = false;
            this.purposeValBlur = false;
            this.limitValBlur = false;
            this.onlyNumber = "Only numbers are allowed";
            this.onlyAlphabet = "Please enter only alphabets";
            this.avoidSpecialChar = "Please enter alphabets & numbers";
            switch (field) {
                case "Other":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.loanRequestedDetails.otherLoan = '';
                        this.otherValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Purpose":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.loanRequestedDetails.otherPurposeOfLoan = '';
                        this.purposeValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Limit":
                    if (!(value.match(/^\d+$/))) {
                        this.loanRequestedDetails.limitRequested = '';
                        this.limitValBlur = true;
                        this.setTimeOut();
                    }
                    break;
            }
        }
    }
}