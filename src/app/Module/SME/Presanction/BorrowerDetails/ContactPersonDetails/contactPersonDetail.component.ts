import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DIAConstants } from '../../../../../Shared/constants';
import { Constants } from '../../../../../Shared/Constants/constants.service';

import { ContactPersonDetails } from '../../../../../Service/SME/Presanction/BorrowerDetails/ContactPersonDetails/contactPersonDetails';
import { AddContactPersonDetailsService } from '../../../../../Service/SME/Presanction/BorrowerDetails/ContactPersonDetails/AddContactPersonDetails/addContactPersonDetails.service';
import { SmePresanctionGetContactPersonDetailsService } from '../../../../../Service/SME/Presanction/BorrowerDetails/ContactPersonDetails/GetContactPersonDetails/getContactPersonDetails.service';
import { UtilsService } from '../../../../../Shared/UtilsService/utils.service';

@Component({
    selector: 'contactPersonDetail',
    templateUrl: './contactPersonDetail.component.html',
    styleUrls: ['./contactPersonDetail.component.scss', '../../../../module.component.scss'],
    providers: [
        ContactPersonDetails,
        AddContactPersonDetailsService,
        SmePresanctionGetContactPersonDetailsService,
        UtilsService,
        Constants
    ]
})

export class ContactPersonDetailComponent implements OnInit {
    public apiRequest: any;
    private states: any[];
    private salutation: any[];
    private upToFive: any[];
    private responseStatus: Object = [];
    private status: boolean;
    private message: string;
    private statusMsg: string;
    private checked: boolean;
    private executivePFIndex: any;
    private registredAddressUnit: any = {};
    private unRegistredAddressUnit: any = {};
    private borrowerRefId: string;
    private pfId: string;
    private isEmailValid: boolean = true;
    private isLoanEditable: boolean;
    private isPinCodeValid: boolean = true;
    private isMobileNumberValid: boolean = true;
    private firstNameVal: boolean = false;
    private middleNameVal: boolean = false;
    private lastNameVal: boolean = false;
    private relationshipVal: boolean = false;
    private cityVal: boolean = false;
    private districtVal: boolean = false;
    private blockVal: boolean = false;
    private villageVal: boolean = false;
    private pincodeVal: boolean = false;
    private mobileVal: boolean = false;
    private landlineVal: boolean = false;
    private contactPersonDetailsDupl = null;
    private firstNameValBlur: boolean = false;
    private middleNameValBlur: boolean = false;
    private lastNameValBlur: boolean = false;
    private relationshipValBlur: boolean = false;
    private cityValBlur: boolean = false;
    private districtValBlur: boolean = false;
    private blockValBlur: boolean = false;
    private villageValBlur: boolean = false;
    private pincodeValBlur: boolean = false;
    private mobileValBlur: boolean = false;
    private landlineValBlur: boolean = false;
    private onlyNumber: string;
    private onlyAlphabet: string;
    private avoidSpecialChar: string;

    constructor(private _addContactPersonDetailsService: AddContactPersonDetailsService, private service: SmePresanctionGetContactPersonDetailsService, private route: ActivatedRoute, private utilsService: UtilsService, private constants: Constants) {
        this.states = DIAConstants.states;
        this.salutation = DIAConstants.salutation;
        this.upToFive = DIAConstants.upToFive;
    }

    /**
    This function is used for adding the contact person details
    @param No Parameters
    @return Nothing
    */
    @Input() contactPersonDetails: ContactPersonDetails;
    addContactPersonDetail() {
        if (this.borrowerRefId) {
            //   if (this.contactPersonDetails.salutation && this.contactPersonDetails.firstName && this.contactPersonDetails.lastName && this.contactPersonDetails.relationshipWithFirm && this.contactPersonDetails.plotBuildingFlatNum && this.contactPersonDetails.addressLine1 && this.contactPersonDetails.addressLine2 && this.contactPersonDetails.city && this.contactPersonDetails.pinCode && this.contactPersonDetails.mobileNum && this.contactPersonDetails.state) {
            if (this.contactPersonDetails.emailId) {
                this.isEmailValid = this.utilsService.emailValidator(this.contactPersonDetails.emailId);
            } else {
                this.isEmailValid = true;
            }
            if (this.contactPersonDetails.pinCode) {
                if (this.contactPersonDetails.pinCode.length < 6) {
                    this.isPinCodeValid = false;
                } else {
                    this.isPinCodeValid = true;
                }
            } else {
                this.isPinCodeValid = true;
            }
            if (this.contactPersonDetails.mobileNum) {
                if (this.contactPersonDetails.mobileNum.length < 10) {
                    this.isMobileNumberValid = false;
                } else {
                    this.isMobileNumberValid = true;
                }
            } else {
                this.isMobileNumberValid = true;
            }

            if (this.isMobileNumberValid && this.isPinCodeValid && this.isEmailValid) {
                this.contactPersonDetails["borrowerRefId"] = this.borrowerRefId;
                this.contactPersonDetails = this.utilsService.dataSourceSave(this.contactPersonDetails);
                localStorage.setItem("salutationAddress", JSON.stringify(this.contactPersonDetails));
                var objectToPost: { borrowerPersonDetail: object; appId: string; executivePFIndex: string, } = { borrowerPersonDetail: this.contactPersonDetails, appId: "WEB-DIA", executivePFIndex: this.pfId };
                console.log(JSON.stringify(objectToPost));
                this._addContactPersonDetailsService.postComment(objectToPost).subscribe(
                    data => {
                        console.log(this.responseStatus = data);
                        if (data.responseMessage == "Successful") {
                            this.getContactPersonDetails();
                            this.message = this.constants.getMessage('successMsg');
                            this.statusMsg = "success";
                            //this.disabledFeature();
                        } else {
                            this.message = data.responseMessage;
                            this.statusMsg = "error";
                        }
                    },
                    err => {
                        console.log(err);
                        this.message = this.constants.getMessage('errorMsg');
                        this.statusMsg = "error";
                    },
                    () => console.log('Request Completed')
                );
                this.status = true;
                this.setTimeOut();
            } else {
                if (!this.isEmailValid) {
                    this.message = this.constants.getMessage('emailVal');
                    this.statusMsg = "error";
                } else if (!this.isPinCodeValid) {
                    this.message = this.constants.getMessage('pinCodeVal');
                    this.statusMsg = "error";
                } else if (!this.isMobileNumberValid) {
                    this.message = this.constants.getMessage('mobileNoVal');
                    this.statusMsg = "error";
                }
                this.setTimeOut();
            }
            // } else {
            //     this.message = this.constants.getMessage('requiredFieldVal');
            //     this.statusMsg = "error";
            //     this.setTimeOut();
            // }
        } else {
            this.message = this.constants.getMessage('unitDetailVal');
            this.statusMsg = "error";
            this.setTimeOut();
        }
    }

    /**
    This function is used for timeout call
    @param No Parameters
    @return Nothing
    */
    setTimeOut() {
        setTimeout(() => {
            this.message = "";
            this.onlyNumber = "";
            this.onlyAlphabet = "";
            this.avoidSpecialChar = "";
        }, 3000);
    }

    /**
    This function is used when user click on Same As Address field
    @param No Parameters
    @return Nothing
    */
    sameAddress() {
        console.log(this.contactPersonDetails);
        console.log(this.unRegistredAddressUnit);
        if (this.checked) {
            console.log(this.registredAddressUnit);
            this.unRegistredAddressUnit = JSON.stringify(this.contactPersonDetails);
            if (!(this.contactPersonDetailsDupl.plotBuildingFlatNumStatus == 'LOS' || this.contactPersonDetailsDupl.plotBuildingFlatNumStatus == 'Mob')) {
                this.contactPersonDetails["plotBuildingFlatNum"] = this.registredAddressUnit.plotBuildingFlatNum;
            }
            if (!(this.contactPersonDetailsDupl.addressLine1Status == 'LOS' || this.contactPersonDetailsDupl.addressLine1Status == 'Mob')) {
                this.contactPersonDetails["addressLine1"] = this.registredAddressUnit.addressLine1;
            }
            if (!(this.contactPersonDetailsDupl.addressLine2Status == 'LOS' || this.contactPersonDetailsDupl.addressLine2Status == 'Mob')) {
                this.contactPersonDetails["addressLine2"] = this.registredAddressUnit.addressLine2;
            }
            if (!(this.contactPersonDetailsDupl.stateStatus == 'LOS' || this.contactPersonDetailsDupl.stateStatus == 'Mob')) {
                this.contactPersonDetails["state"] = this.registredAddressUnit.state;
            }
            if (!(this.contactPersonDetailsDupl.cityStatus == 'LOS' || this.contactPersonDetailsDupl.cityStatus == 'Mob')) {
                this.contactPersonDetails["city"] = this.registredAddressUnit.city;
            }
            if (!(this.contactPersonDetailsDupl.districtStatus == 'LOS' || this.contactPersonDetailsDupl.districtStatus == 'Mob')) {
                this.contactPersonDetails["district"] = this.registredAddressUnit.district;
            }
            if (!(this.contactPersonDetailsDupl.blockTehsilSubdistrictStatus == 'LOS' || this.contactPersonDetailsDupl.blockTehsilSubdistrictStatus == 'Mob')) {
                this.contactPersonDetails["blockTehsilSubdistrict"] = this.registredAddressUnit.blockTehsilSubdistrict;
            }
            if (!(this.contactPersonDetailsDupl.villageStatus == 'LOS' || this.contactPersonDetailsDupl.villageStatus == 'Mob')) {
                this.contactPersonDetails["village"] = this.registredAddressUnit.village;
            }
            if (!(this.contactPersonDetailsDupl.pinCodeStatus == 'LOS' || this.contactPersonDetailsDupl.pinCodeStatus == 'Mob')) {
                this.contactPersonDetails["pinCode"] = this.registredAddressUnit.pinCode;
            }
            if (!(this.contactPersonDetailsDupl.mobileNumStatus == 'LOS' || this.contactPersonDetailsDupl.mobileNumStatus == 'Mob')) {
                this.contactPersonDetails["mobileNum"] = this.registredAddressUnit.mobileNum;
            }
            if (!(this.contactPersonDetailsDupl.landlineNumStatus == 'LOS' || this.contactPersonDetailsDupl.landlineNumStatus == 'Mob')) {
                this.contactPersonDetails["landlineNum"] = this.registredAddressUnit.landlineNum;
            }
            if (!(this.contactPersonDetailsDupl.emailIdStatus == 'LOS' || this.contactPersonDetailsDupl.emailIdStatus == 'Mob')) {
                this.contactPersonDetails["emailId"] = this.registredAddressUnit.emailId;
            }
        } else {
            console.log("Unchecked");
            this.unRegistredAddressUnit = JSON.parse(this.unRegistredAddressUnit);
            this.contactPersonDetails["plotBuildingFlatNum"] = this.unRegistredAddressUnit.plotBuildingFlatNum;
            this.contactPersonDetails["addressLine1"] = this.unRegistredAddressUnit.addressLine1;
            this.contactPersonDetails["addressLine2"] = this.unRegistredAddressUnit.addressLine2;
            this.contactPersonDetails["state"] = this.unRegistredAddressUnit.state;
            this.contactPersonDetails["city"] = this.unRegistredAddressUnit.city;
            this.contactPersonDetails["district"] = this.unRegistredAddressUnit.district;
            this.contactPersonDetails["blockTehsilSubdistrict"] = this.unRegistredAddressUnit.blockTehsilSubdistrict;
            this.contactPersonDetails["village"] = this.unRegistredAddressUnit.village;
            this.contactPersonDetails["pinCode"] = this.unRegistredAddressUnit.pinCode;
            this.contactPersonDetails["mobileNum"] = this.unRegistredAddressUnit.mobileNum;
            this.contactPersonDetails["landlineNum"] = this.unRegistredAddressUnit.landlineNum;
            this.contactPersonDetails["emailId"] = this.unRegistredAddressUnit.emailId;
        }
    }

    /**
    This function is called in the initial component loading
    @param No Parameters
    @return Nothing
    */
    ngOnInit() {
        this.contactPersonDetails = new ContactPersonDetails();
        if (localStorage.getItem("isLoanEditable") != null && localStorage.getItem("isLoanEditable") != "null") {
            if (localStorage.getItem("isLoanEditable") == "true") {
                this.isLoanEditable = true;
            } else {
                this.isLoanEditable = false;
            }
        }
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null && localStorage.getItem("borrowerRefID") != "null" && localStorage.getItem("borrowerRefID") != null) {
            this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.registredAddressUnit = JSON.parse(localStorage.getItem("registredAddressUnit"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
            this.getContactPersonDetails();
        } else {
            this.contactPersonDetails = this.utilsService.dataSourceCheckFromLOS(this.contactPersonDetails);
        }
    }

    /**
    This function is used for getting the contact person details
    @param No Parameters
    @return Nothing
    */
    getContactPersonDetails() {
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };

        this.service.getContactPersonDetails(this.apiRequest).subscribe(response => {
            if (response.borrowerPersonDetail) {
                this.contactPersonDetails = response.borrowerPersonDetail;
                this.contactPersonDetails = this.utilsService.dataSourceCheck(this.contactPersonDetails);
            } else {
                this.contactPersonDetails = this.utilsService.dataSourceCheckFromLOS(this.contactPersonDetails);
            }
            this.contactPersonDetailsDupl = this.contactPersonDetails;
        })
    }

    /**
    This function is called when user clicked on the input fields
    @param data - input validation case
    @param field - field name
    @return data - input validation case
    */
    keypressCheck(data, field) {
        this.firstNameVal = false;
        this.middleNameVal = false;
        this.lastNameVal = false;
        this.relationshipVal = false;
        this.cityVal = false;
        this.districtVal = false;
        this.blockVal = false;
        this.villageVal = false;
        this.pincodeVal = false;
        this.mobileVal = false;
        this.landlineVal = false;
        switch (field) {
            case "First Name":
                if (this.utilsService.validString) {
                    this.firstNameVal = true;
                } else {
                    this.firstNameVal = false;
                }
                break;

            case "Middle Name":
                if (this.utilsService.validString) {
                    this.middleNameVal = true;
                } else {
                    this.middleNameVal = false;
                }
                break;

            case "Last Name":
                if (this.utilsService.validString) {
                    this.lastNameVal = true;
                } else {
                    this.lastNameVal = false;
                }
                break;

            case "Relationship":
                if (this.utilsService.validString) {
                    this.relationshipVal = true;
                } else {
                    this.relationshipVal = false;
                }
                break;

            case "City":
                if (this.utilsService.validString) {
                    this.cityVal = true;
                } else {
                    this.cityVal = false;
                }
                break;

            case "District":
                if (this.utilsService.validString) {
                    this.districtVal = true;
                } else {
                    this.districtVal = false;
                }
                break;

            case "Block":
                if (this.utilsService.validString) {
                    this.blockVal = true;
                } else {
                    this.blockVal = false;
                }
                break;

            case "Village":
                if (this.utilsService.validString) {
                    this.villageVal = true;
                } else {
                    this.villageVal = false;
                }
                break;

            case "Pincode":
                if (this.utilsService.validString) {
                    this.pincodeVal = true;
                } else {
                    this.pincodeVal = false;
                }
                break;

            case "Mobile":
                if (this.utilsService.validString) {
                    this.mobileVal = true;
                } else {
                    this.mobileVal = false;
                }
                break;

            case "Landline":
                if (this.utilsService.validString) {
                    this.landlineVal = true;
                } else {
                    this.landlineVal = false;
                }
                break;

        }
        return data;
    }

    onBlurValidation(value, field) {
        if (value) {
            this.firstNameValBlur = false;
            this.middleNameValBlur = false;
            this.lastNameValBlur = false;
            this.relationshipValBlur = false;
            this.cityValBlur = false;
            this.districtValBlur = false;
            this.blockValBlur = false;
            this.villageValBlur = false;
            this.pincodeValBlur = false;
            this.mobileValBlur = false;
            this.landlineValBlur = false;
            this.onlyNumber = "Only numbers are allowed";
            this.onlyAlphabet = "Please enter only alphabets";
            this.avoidSpecialChar = "Please enter alphabets & numbers";
            switch (field) {
                case "First Name":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.contactPersonDetails.firstName = '';
                        this.firstNameValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Middle Name":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.contactPersonDetails.middleName = '';
                        this.middleNameValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Last Name":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.contactPersonDetails.lastName = '';
                        this.lastNameValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Relationship":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.contactPersonDetails.relationshipWithFirm = '';
                        this.relationshipValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "City":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.contactPersonDetails.city = '';
                        this.cityValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "District":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.contactPersonDetails.district = '';
                        this.districtValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Block":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.contactPersonDetails.blockTehsilSubdistrict = '';
                        this.blockValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Village":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.contactPersonDetails.village = '';
                        this.villageValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Pincode":
                    if (!(value.match(/^\d+$/))) {
                        this.contactPersonDetails.pinCode = '';
                        this.pincodeValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Mobile":
                    if (!(value.match(/^\d+$/))) {
                        this.contactPersonDetails.mobileNum = '';
                        this.mobileValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Landline":
                    if (!(value.match(/^\d+$/))) {
                        this.contactPersonDetails.landlineNum = '';
                        this.landlineValBlur = true;
                        this.setTimeOut();
                    }
                    break;
            }
        }
    }
}
