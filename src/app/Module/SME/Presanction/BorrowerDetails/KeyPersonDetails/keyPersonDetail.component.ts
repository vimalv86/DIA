import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DIAConstants } from '../../../../../Shared/constants';
import { Constants } from '../../../../../Shared/Constants/constants.service';

import { KeyPersonDetails } from '../../../../../Service/SME/Presanction/BorrowerDetails/KeyPersonDetails/keyPersonDetails';
import { AddKeyPersonDetailsService } from '../../../../../Service/SME/Presanction/BorrowerDetails/KeyPersonDetails/AddKeyPersonDetails/addKeyPersonDetails.service';
import { SmePresanctionGetKeyPersonDetailsService } from '../../../../../Service/SME/Presanction/BorrowerDetails/KeyPersonDetails/GetKeyPersonDetails/getKeyPersonDetails.service';
import { UtilsService } from '../../../../../Shared/UtilsService/utils.service';

@Component({
    selector: 'keyPersonDetail',
    templateUrl: './keyPersonDetail.component.html',
    styleUrls: ['./keyPersonDetail.component.scss', '../../../../module.component.scss'],
    providers: [
        KeyPersonDetails,
        AddKeyPersonDetailsService,
        SmePresanctionGetKeyPersonDetailsService,
        UtilsService,
        Constants
    ]
})

export class KeyPersonDetailComponent implements OnInit {
    private apiRequest: any;
    private states: any[];
    private salutation: any[];
    private responseStatus: Object = [];
    private message: string;
    private statusMsg: string;
    private status: boolean;
    private checked: boolean;
    private salutationChecked: boolean;
    private executivePFIndex: any;
    private borrowerRefId: string;
    private registredAddressUnit: any = {};
    private unRegistredAddressUnit: any = {};
    private registeredSalutationAddress: any = {};
    private unRegisteredSalutationAddress: any = {};
    private pfId: string;
    private isEmailValid: boolean = true;
    private noData: boolean;
    private keyPersonList: any = [];
    private indexVal: any;
    private isLoanEditable: boolean;
    private isPinCodeValid: boolean = true;
    private isMobileNumberValid: boolean = true;
    private loanDetailsWithOutInspection: any;
    private firstNameVal: boolean = false;
    private middleNameVal: boolean = false;
    private lastNameVal: boolean = false;
    private relationshipVal: boolean = false;
    private cityVal: boolean = false;
    private districtVal: boolean = false;
    private blockVal: boolean = false;
    private villageVal: boolean = false;
    private pincodeVal: boolean = false;
    private mobileVal: boolean = false;
    private landlineVal: boolean = false;
    private keyPersonListDupl = null;
    private firstNameValBlur: boolean = false;
    private middleNameValBlur: boolean = false;
    private lastNameValBlur: boolean = false;
    private relationshipValBlur: boolean = false;
    private cityValBlur: boolean = false;
    private districtValBlur: boolean = false;
    private blockValBlur: boolean = false;
    private villageValBlur: boolean = false;
    private pincodeValBlur: boolean = false;
    private mobileValBlur: boolean = false;
    private landlineValBlur: boolean = false;
    private onlyNumber: string;
    private onlyAlphabet: string;
    private avoidSpecialChar: string;

    constructor(private _addKeyPersonDetailsService: AddKeyPersonDetailsService, private service: SmePresanctionGetKeyPersonDetailsService, private route: ActivatedRoute, private utilsService: UtilsService, private constants: Constants) {
        this.states = DIAConstants.states;
        this.salutation = DIAConstants.salutation;
    }

    @Input() keyPersonDetails: KeyPersonDetails;

    addKeyPersonDetail() {
        if (this.borrowerRefId && !this.keyPersonList[this.indexVal].borrowerRefId) {
            this.keyPersonList[this.indexVal].borrowerRefId = this.borrowerRefId;
        }
        if (this.keyPersonList[this.indexVal].borrowerRefId) {
            //  if(this.keyPersonList[this.indexVal].salutation && this.keyPersonList[this.indexVal].firstName && this.keyPersonList[this.indexVal].lastName && this.keyPersonList[this.indexVal].relationshipWithFirm && this.keyPersonList[this.indexVal].plotBuildingFlatNum && this.keyPersonList[this.indexVal].addressLine1 && this.keyPersonList[this.indexVal].addressLine2 && this.keyPersonList[this.indexVal].city && this.keyPersonList[this.indexVal].mobileNum && this.keyPersonList[this.indexVal].pinCode){            
            if (this.keyPersonList[this.indexVal].emailId) {
                this.isEmailValid = this.utilsService.emailValidator(this.keyPersonList[this.indexVal].emailId);
            } else {
                this.isEmailValid = true;
            }
            if (this.keyPersonList[this.indexVal].pinCode) {
                if (this.keyPersonList[this.indexVal].pinCode.length < 6) {
                    this.isPinCodeValid = false;
                } else {
                    this.isPinCodeValid = true;
                }
            } else {
                this.isPinCodeValid = true;
            }
            if (this.keyPersonList[this.indexVal].mobileNum) {
                if (this.keyPersonList[this.indexVal].mobileNum.length < 10) {
                    this.isMobileNumberValid = false;
                } else {
                    this.isMobileNumberValid = true;
                }
            } else {
                this.isMobileNumberValid = true;
            }

            if (this.isMobileNumberValid && this.isPinCodeValid && this.isEmailValid) {
                this.checked = this.keyPersonList[this.indexVal].checked;
                this.salutationChecked = this.keyPersonList[this.indexVal].salutationChecked;
                delete this.keyPersonList[this.indexVal].checked;
                delete this.keyPersonList[this.indexVal].salutationChecked;
                delete this.keyPersonList[this.indexVal].message;
                this.keyPersonList[this.indexVal] = this.utilsService.dataSourceSave(this.keyPersonList[this.indexVal]);
                var objectToPost: { borrowerPersonDetail: object; appId: string; executivePFIndex: string, } = { borrowerPersonDetail: this.keyPersonList[this.indexVal], appId: "WEB-DIA", executivePFIndex: this.pfId };
                console.log(JSON.stringify(objectToPost));
                this._addKeyPersonDetailsService.postComment(objectToPost).subscribe(
                    data => {
                        console.log(this.responseStatus = data);
                        if (data.responseMessage == "Successful") {
                            //    this.disableCurrentIndex();
                            this.keyPersonList[this.indexVal].borrowerPersonRefId = data.refId;
                            this.keyPersonList[this.indexVal].message = this.constants.getMessage('successMsg');
                            this.statusMsg = "success";
                        } else {
                            this.keyPersonList[this.indexVal].message = data.responseMessage;
                            this.statusMsg = "error";
                        }
                        this.setTimeOut();
                    },
                    err => {
                        console.log(err);
                        this.keyPersonList[this.indexVal].message = this.constants.getMessage('errorMsg');
                        this.statusMsg = "error";
                        this.setTimeOut();
                    },
                    () => console.log('Request Completed')
                );
                this.keyPersonList[this.indexVal].checked = this.checked;
                this.keyPersonList[this.indexVal].salutationChecked = this.salutationChecked;
                this.keyPersonList[this.indexVal].message = '';
                this.status = true;
            } else {
                if (!this.isEmailValid) {
                    this.keyPersonList[this.indexVal].message = this.constants.getMessage('emailVal');
                    this.statusMsg = "error";
                } else if (!this.isPinCodeValid) {
                    this.keyPersonList[this.indexVal].message = this.constants.getMessage('pinCodeVal');
                    this.statusMsg = "error";
                } else if (!this.isMobileNumberValid) {
                    this.keyPersonList[this.indexVal].message = this.constants.getMessage('mobileNoVal');
                    this.statusMsg = "error";
                }
                this.setTimeOut();
            }
            // }else{
            //     this.keyPersonList[this.indexVal].message = this.constants.getMessage('requiredFieldVal');
            //     this.statusMsg = "error";
            //     this.setTimeOut();
            // }
        } else {
            this.keyPersonList[this.indexVal].message = this.constants.getMessage('unitDetailVal');
            this.statusMsg = "error";
            this.setTimeOut();
        }
    }

    setTimeOut() {
        setTimeout(() => {
            this.keyPersonList[this.indexVal].message = "";
            this.onlyNumber = "";
            this.onlyAlphabet = "";
            this.avoidSpecialChar = "";
        }, 3000);
    }

    sameSalutationDetails() {
        if (this.keyPersonList[this.indexVal].salutationChecked) {
            this.unRegisteredSalutationAddress = JSON.stringify(this.keyPersonList[this.indexVal]);
            if (!(this.keyPersonListDupl[this.indexVal].salutationStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].salutationStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].salutation = this.registeredSalutationAddress.salutation;
            }
            if (!(this.keyPersonListDupl[this.indexVal].firstNameStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].firstNameStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].firstName = this.registeredSalutationAddress.firstName;
            }
            if (!(this.keyPersonListDupl[this.indexVal].middleNameStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].middleNameStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].middleName = this.registeredSalutationAddress.middleName;
            }
            if (!(this.keyPersonListDupl[this.indexVal].lastNameStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].lastNameStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].lastName = this.registeredSalutationAddress.lastName;
            }
            if (!(this.keyPersonListDupl[this.indexVal].relationshipWithFirmStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].relationshipWithFirmStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].relationshipWithFirm = this.registeredSalutationAddress.relationshipWithFirm;
            }
        } else {
            this.unRegisteredSalutationAddress = JSON.parse(this.unRegisteredSalutationAddress);
            this.keyPersonList[this.indexVal].salutation = this.unRegisteredSalutationAddress.salutation;
            this.keyPersonList[this.indexVal].firstName = this.unRegisteredSalutationAddress.firstName;
            this.keyPersonList[this.indexVal].middleName = this.unRegisteredSalutationAddress.middleName;
            this.keyPersonList[this.indexVal].lastName = this.unRegisteredSalutationAddress.lastName;
            this.keyPersonList[this.indexVal].relationshipWithFirm = this.unRegisteredSalutationAddress.relationshipWithFirm;
        }
    }

    sameAddress(i) {
        if (this.keyPersonList[this.indexVal].checked) {
            console.log(this.registredAddressUnit);
            this.unRegistredAddressUnit = JSON.stringify(this.keyPersonList[this.indexVal]);
            if (!(this.keyPersonListDupl[this.indexVal].plotBuildingFlatNumStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].plotBuildingFlatNumStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].plotBuildingFlatNum = this.registredAddressUnit.plotBuildingFlatNum;
            }
            if (!(this.keyPersonListDupl[this.indexVal].addressLine1Status == 'LOS' || this.keyPersonListDupl[this.indexVal].addressLine1Status == 'Mob')) {
                this.keyPersonList[this.indexVal].addressLine1 = this.registredAddressUnit.addressLine1;
            }
            if (!(this.keyPersonListDupl[this.indexVal].addressLine2Status == 'LOS' || this.keyPersonListDupl[this.indexVal].addressLine2Status == 'Mob')) {
                this.keyPersonList[this.indexVal].addressLine2 = this.registredAddressUnit.addressLine2;
            }
            if (!(this.keyPersonListDupl[this.indexVal].stateStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].stateStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].state = this.registredAddressUnit.state;
            }
            if (!(this.keyPersonListDupl[this.indexVal].cityStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].cityStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].city = this.registredAddressUnit.city;
            }
            if (!(this.keyPersonListDupl[this.indexVal].districtStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].districtStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].district = this.registredAddressUnit.district;
            }
            if (!(this.keyPersonListDupl[this.indexVal].blockTehsilSubdistrictStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].blockTehsilSubdistrictStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].blockTehsilSubdistrict = this.registredAddressUnit.blockTehsilSubdistrict;
            }
            if (!(this.keyPersonListDupl[this.indexVal].villageStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].villageStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].village = this.registredAddressUnit.village;
            }
            if (!(this.keyPersonListDupl[this.indexVal].pinCodeStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].pinCodeStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].pinCode = this.registredAddressUnit.pinCode;
            }
            if (!(this.keyPersonListDupl[this.indexVal].mobileNumStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].mobileNumStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].mobileNum = this.registredAddressUnit.mobileNum;
            }
            if (!(this.keyPersonListDupl[this.indexVal].landlineNumStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].landlineNumStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].landlineNum = this.registredAddressUnit.landlineNum;
            }
            if (!(this.keyPersonListDupl[this.indexVal].emailIdStatus == 'LOS' || this.keyPersonListDupl[this.indexVal].emailIdStatus == 'Mob')) {
                this.keyPersonList[this.indexVal].emailId = this.registredAddressUnit.emailId;
            }
        } else {
            console.log("Unchecked");
            this.unRegistredAddressUnit = JSON.parse(this.unRegistredAddressUnit);
            this.keyPersonList[this.indexVal].plotBuildingFlatNum = this.unRegistredAddressUnit.plotBuildingFlatNum;
            this.keyPersonList[this.indexVal].addressLine1 = this.unRegistredAddressUnit.addressLine1;
            this.keyPersonList[this.indexVal].addressLine2 = this.unRegistredAddressUnit.addressLine2;
            this.keyPersonList[this.indexVal].state = this.unRegistredAddressUnit.state;
            this.keyPersonList[this.indexVal].city = this.unRegistredAddressUnit.city;
            this.keyPersonList[this.indexVal].district = this.unRegistredAddressUnit.district;
            this.keyPersonList[this.indexVal].blockTehsilSubdistrict = this.unRegistredAddressUnit.blockTehsilSubdistrict;
            this.keyPersonList[this.indexVal].village = this.unRegistredAddressUnit.village;
            this.keyPersonList[this.indexVal].pinCode = this.unRegistredAddressUnit.pinCode;
            this.keyPersonList[this.indexVal].mobileNum = this.unRegistredAddressUnit.mobileNum;
            this.keyPersonList[this.indexVal].landlineNum = this.unRegistredAddressUnit.landlineNum;
            this.keyPersonList[this.indexVal].emailId = this.unRegistredAddressUnit.emailId;
        }
    }

    ngOnInit() {
        this.keyPersonDetails = new KeyPersonDetails();
        if (localStorage.getItem("isLoanEditable") != null && localStorage.getItem("isLoanEditable") != "null") {
            if (localStorage.getItem("isLoanEditable") == "true") {
                this.isLoanEditable = true;
            } else {
                this.isLoanEditable = false;
            }
        }

        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null && localStorage.getItem("borrowerRefID") != "null" && localStorage.getItem("borrowerRefID") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
            this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
            console.log(this.borrowerRefId);
            this.registredAddressUnit = JSON.parse(localStorage.getItem("registredAddressUnit"));
            this.registeredSalutationAddress = JSON.parse(localStorage.getItem("salutationAddress"));
            this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
            this.getKeyPersonDetails();
        } else {
            this.getDataFromLocalStorage();
        }
    }

    getKeyPersonDetails() {
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };
        this.service.getKeyPersonDetails(this.apiRequest).subscribe(response => {
            if (response.borrowerPersonDetail) {
                this.keyPersonList = response.borrowerPersonDetail;
                if (this.keyPersonList.length > 0) {
                    for (let k = 0; k < this.keyPersonList.length; k++) {
                        if (this.keyPersonList[k].borrowerPersonRefId == null) {
                            this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
                            if (this.loanDetailsWithOutInspection) {
                                if (this.loanDetailsWithOutInspection.borrowerPersonalList && this.loanDetailsWithOutInspection.borrowerPersonalList.length > 0) {
                                    if (this.loanDetailsWithOutInspection.borrowerPersonalList.length > k) {
                                        this.keyPersonList[k] = this.loanDetailsWithOutInspection.borrowerPersonalList[k];
                                        this.keyPersonList[k].borrowerPersonRefId = null;
                                        this.keyPersonList[k] = this.utilsService.dataSourceCheckFromLOS(this.keyPersonList[k]);
                                    } else {
                                        this.keyPersonList[k] = this.utilsService.dataSourceCheck(this.keyPersonList[k]);
                                    }
                                }
                            } else {
                                this.keyPersonList[k] = this.utilsService.dataSourceCheck(this.keyPersonList[k]);
                            }
                        } else {
                            this.keyPersonList[k] = this.utilsService.dataSourceCheck(this.keyPersonList[k]);
                        }
                    }
                    this.unRegistredAddressUnit = JSON.stringify(this.keyPersonList[0]);
                    this.unRegisteredSalutationAddress = JSON.stringify(this.keyPersonList[0]);
                    for (let k = 0; k < this.keyPersonList.length; k++) {
                        this.keyPersonList[k].checked = false;
                        this.keyPersonList[k].salutationChecked = false;
                        this.keyPersonList[k].message = '';
                    }
                } else {
                    console.log("No data");
                    this.getDataFromLocalStorage();
                }
                console.log(this.keyPersonList);
                this.keyPersonListDupl = this.keyPersonList;
            } else {
                this.getDataFromLocalStorage();
            }
        })
    }

    getDataFromLocalStorage() {
        this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
        if (this.loanDetailsWithOutInspection) {
            if (this.loanDetailsWithOutInspection.borrowerPersonalList && this.loanDetailsWithOutInspection.borrowerPersonalList.length > 0) {
                this.keyPersonList = this.loanDetailsWithOutInspection.borrowerPersonalList;
                this.keyPersonList = this.utilsService.dataSourceCheckFromLOSArray(this.keyPersonList);
                this.keyPersonListDupl = this.keyPersonList;
                if (this.keyPersonList.length > 0) {
                    this.unRegistredAddressUnit = JSON.stringify(this.keyPersonList[0]);
                    this.unRegisteredSalutationAddress = JSON.stringify(this.keyPersonList[0]);
                    for (let k = 0; k < this.keyPersonList.length; k++) {
                        this.keyPersonList[k].checked = false;
                        this.keyPersonList[k].message = '';
                        this.keyPersonList[k].borrowerPersonRefId = null;
                    }
                } else {
                    console.log("No data");
                    this.noData = true;
                    this.message = this.constants.getMessage('unitDetailVal');
                }
            } else {
                this.noData = true;
            }
        } else {
            this.noData = true;
            this.message = this.constants.getMessage('unitDetailVal');
        }
    }

    keypressCheck(data, field) {
        this.firstNameVal = false;
        this.middleNameVal = false;
        this.lastNameVal = false;
        this.relationshipVal = false;
        this.cityVal = false;
        this.districtVal = false;
        this.blockVal = false;
        this.villageVal = false;
        this.pincodeVal = false;
        this.mobileVal = false;
        this.landlineVal = false;
        switch (field) {
            case "First Name":
                if (this.utilsService.validString) {
                    this.firstNameVal = true;
                } else {
                    this.firstNameVal = false;
                }
                break;

            case "Middle Name":
                if (this.utilsService.validString) {
                    this.middleNameVal = true;
                } else {
                    this.middleNameVal = false;
                }
                break;

            case "Last Name":
                if (this.utilsService.validString) {
                    this.lastNameVal = true;
                } else {
                    this.lastNameVal = false;
                }
                break;

            case "Relationship":
                if (this.utilsService.validString) {
                    this.relationshipVal = true;
                } else {
                    this.relationshipVal = false;
                }
                break;

            case "City":
                if (this.utilsService.validString) {
                    this.cityVal = true;
                } else {
                    this.cityVal = false;
                }
                break;

            case "District":
                if (this.utilsService.validString) {
                    this.districtVal = true;
                } else {
                    this.districtVal = false;
                }
                break;

            case "Block":
                if (this.utilsService.validString) {
                    this.blockVal = true;
                } else {
                    this.blockVal = false;
                }
                break;

            case "Village":
                if (this.utilsService.validString) {
                    this.villageVal = true;
                } else {
                    this.villageVal = false;
                }
                break;

            case "Pincode":
                if (this.utilsService.validString) {
                    this.pincodeVal = true;
                } else {
                    this.pincodeVal = false;
                }
                break;

            case "Mobile":
                if (this.utilsService.validString) {
                    this.mobileVal = true;
                } else {
                    this.mobileVal = false;
                }
                break;

            case "Landline":
                if (this.utilsService.validString) {
                    this.landlineVal = true;
                } else {
                    this.landlineVal = false;
                }
                break;

        }
        return data;
    }


    tabHeaderClick(i) {
        this.indexVal = i;
        for (var k = 0; k < this.keyPersonList.length; k++) {
            this.keyPersonList[k].message = '';
        }
    }

    onBlurValidation(value, field) {
        if (value) {
            this.firstNameValBlur = false;
            this.middleNameValBlur = false;
            this.lastNameValBlur = false;
            this.relationshipValBlur = false;
            this.cityValBlur = false;
            this.districtValBlur = false;
            this.blockValBlur = false;
            this.villageValBlur = false;
            this.pincodeValBlur = false;
            this.mobileValBlur = false;
            this.landlineValBlur = false;
            this.onlyNumber = "Only numbers are allowed";
            this.onlyAlphabet = "Please enter only alphabets";
            this.avoidSpecialChar = "Please enter alphabets & numbers";
            switch (field) {
                case "First Name":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.keyPersonList[this.indexVal].firstName = '';
                        this.firstNameValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Middle Name":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.keyPersonList[this.indexVal].middleName = '';
                        this.middleNameValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Last Name":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.keyPersonList[this.indexVal].lastName = '';
                        this.lastNameValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Relationship":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.keyPersonList[this.indexVal].relationshipWithFirm = '';
                        this.relationshipValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "City":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.keyPersonList[this.indexVal].city = '';
                        this.cityValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "District":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.keyPersonList[this.indexVal].district = '';
                        this.districtValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Block":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.keyPersonList[this.indexVal].blockTehsilSubdistrict = '';
                        this.blockValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Village":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.keyPersonList[this.indexVal].village = '';
                        this.villageValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Pincode":
                    if (!(value.match(/^\d+$/))) {
                        this.keyPersonList[this.indexVal].pinCode = '';
                        this.pincodeValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Mobile":
                    if (!(value.match(/^\d+$/))) {
                        this.keyPersonList[this.indexVal].mobileNum = '';
                        this.mobileValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Landline":
                    if (!(value.match(/^\d+$/))) {
                        this.keyPersonList[this.indexVal].landlineNum = '';
                        this.landlineValBlur = true;
                        this.setTimeOut();
                    }
                    break;
            }
        }
    }
}