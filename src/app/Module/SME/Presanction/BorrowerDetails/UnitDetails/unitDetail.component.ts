import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DIAConstants } from '../../../../../Shared/constants';
import { Constants } from '../../../../../Shared/Constants/constants.service';

import { UnitDetails } from '../../../../../Service/SME/Presanction/BorrowerDetails/UnitDetails/unitDetails';
import { AddUpdateInspection } from '../../../../../Service/SME/Presanction/BorrowerDetails/UnitDetails/addUpdateInspection';
import { AddUnitDetailsService } from '../../../../../Service/SME/Presanction/BorrowerDetails/UnitDetails/AddUnitDetails/addUnitDetails.service';
import { AddUpdateInspectionService } from '../../../../../Service/SME/Presanction/BorrowerDetails/UnitDetails/AddUpdateInspection/addUpdateInspection.service';
import { SmePresanctionGetUnitDetailsService } from '../../../../../Service/SME/Presanction/BorrowerDetails/UnitDetails/GetUnitDetails/getUnitDetails.service';
import { UtilsService } from '../../../../../Shared/UtilsService/utils.service';

@Component({
  selector: 'unitDetail',
  templateUrl: './unitDetail.component.html',
  styleUrls: ['./unitDetail.component.scss', '../../../../module.component.scss'],
  providers: [
    AddUnitDetailsService,
    AddUpdateInspectionService,
    SmePresanctionGetUnitDetailsService,
    UtilsService,
    Constants
  ]
})

export class UnitDetailComponent implements OnInit {
  private apiRequest: object;
  private states: any[];
  private upToFive: any[];
  private constitution: any;
  private responseStatus: Object = [];
  private status: boolean;
  private dateOfEstablish: Date;
  private message: string;
  private statusMsg: string;
  private borrowerRefId: string;
  private executivePFIndex: any;
  private unitAddress: any = {};
  private registredAddressUnit: any = {};
  private timeStamp: any;
  private establishmentDate: any;
  private btnDisabled: boolean;
  private pfId: string;
  private isEmailValid: boolean = true;
  private guarantorDisable: boolean;
  private collateralDisable: boolean;
  private isLoanEditable: boolean;
  private isPinCodeValid: boolean = true;
  private isPanValid: boolean = true;
  private isMobileNumberValid: boolean = true;
  private isCollateralDisabled: boolean = false;
  private loanDetailsWithOutInspection: any;
  private cityVal: boolean = false;
  private activityVal: boolean = false;
  private panVal: boolean = false;
  private districtVal: boolean = false;
  private blockVal: boolean = false;
  private villageVal: boolean = false;
  private pincodeVal: boolean = false;
  private mobileVal: boolean = false;
  private landlineVal: boolean = false;
  private cityValBlur: boolean = false;
  private activityValBlur: boolean = false;
  private panValBlur: boolean = false;
  private districtValBlur: boolean = false;
  private blockValBlur: boolean = false;
  private villageValBlur: boolean = false;
  private pincodeValBlur: boolean = false;
  private mobileValBlur: boolean = false;
  private landlineValBlur: boolean = false;
  private onlyNumber: string;
  private onlyAlphabet: string;
  private avoidSpecialChar: string;

  constructor(private _addUnitDetailsService: AddUnitDetailsService, private _addUpdateInspectionService: AddUpdateInspectionService, private service: SmePresanctionGetUnitDetailsService, private route: ActivatedRoute, private utilsService: UtilsService, private constants: Constants) {
    this.states = DIAConstants.states;
    this.constitution = DIAConstants.constitution;
    this.upToFive = DIAConstants.upToFive;
    this.establishmentDate = new Date();
  }

  @Input() unitDetails: UnitDetails;

  guarantorPresent(status) {
    if (status == 1) {
      this.guarantorDisable = true;
      this.unitDetails.noOfGuarantors = "1";
    } else {
      this.unitDetails.noOfGuarantors = "";
      this.guarantorDisable = false;
    }
  }

  collateralIdentified(status) {
    if (status == 1) {
      this.collateralDisable = true;
      this.unitDetails.noOfKeyPerson = "1";
    } else {
      this.unitDetails.noOfKeyPerson = "";
      this.collateralDisable = false;
    }
  }

  addUnitDetail() {
    //  if (this.unitDetails.nameOfUnit && this.unitDetails.activityOfUnit && this.unitDetails.panTan && this.unitDetails.plotBuildingFlatNum && this.unitDetails.addressLine1 && this.unitDetails.addressLine2 && this.unitDetails.city && this.unitDetails.pinCode && this.unitDetails.mobileNum && this.unitDetails.constitution && this.unitDetails.state && this.unitDetails.guarantorsPresent && this.unitDetails.collateralIdentified && this.unitDetails.chkCollateral) {
    if (this.unitDetails.emailId) {
      this.isEmailValid = this.utilsService.emailValidator(this.unitDetails.emailId);
    } else {
      this.isEmailValid = true;
    }
    if (this.unitDetails.pinCode) {
      if (this.unitDetails.pinCode.length < 6) {
        this.isPinCodeValid = false;
      } else {
        this.isPinCodeValid = true;
      }
    } else {
      this.isPinCodeValid = true;
    }
    if (this.unitDetails.mobileNum) {
      if (this.unitDetails.mobileNum.length < 10) {
        this.isMobileNumberValid = false;
      } else {
        this.isMobileNumberValid = true;
      }
    } else {
      this.isMobileNumberValid = true;
    }
    if (this.unitDetails.panTan) {
      if (this.unitDetails.panTan.length < 10) {
        this.isPanValid = false;
      } else {
        this.isPanValid = true;
      }
    } else {
      this.isPanValid = true;
    }
    if (this.isMobileNumberValid && this.isPinCodeValid && this.isEmailValid && this.isPanValid) {
      let actualTime = this.establishmentDate;
      let timeStamp = new Date(actualTime).getTime().toString();
      this.unitDetails["dateOfEstablishTS"] = timeStamp;
      if (!this.unitDetails["borrowerRefId"] && localStorage.getItem("borrowerRefID") != null && localStorage.getItem("borrowerRefID") != "null") {
        this.unitDetails["borrowerRefId"] = localStorage.getItem("borrowerRefID");
      }
      this.unitDetails = this.utilsService.dataSourceSave(this.unitDetails);
      var objectToPost: { borrowerUnit: object; appId: string; executivePFIndex: string; } = { borrowerUnit: this.unitDetails, appId: "WEB-DIA", executivePFIndex: this.pfId };

      console.log(JSON.stringify(objectToPost));

      this.addUnitAddress();

      this._addUnitDetailsService.postComment(objectToPost).subscribe(
        data => {
          console.log(this.responseStatus = data);
          if (data.responseMessage == "Successful") {
            this.registredAddressUnit = localStorage.setItem("registredAddressUnit", JSON.stringify(this.unitAddress));
            if (localStorage.getItem("borrowerRefID") == "null" || localStorage.getItem("borrowerRefID") == null) {
              this.addUpdateInspection = new AddUpdateInspection();
              this.borrowerRefId = data.refId;
              this.addInspectionDetail();
              this.addUpdateinspection();
            } else {
              this.message = this.constants.getMessage('successMsg');
              this.statusMsg = "success";
            }
          } else {
            this.message = data.responseMessage;
            this.statusMsg = "error";
          }
        },
        err => {
          console.log(err);
          this.message = this.constants.getMessage('errorMsg');
          this.statusMsg = "error";
        },
        () => console.log('Request Completed')
      );
      this.status = true;

      this.setTimeOut();
    } else {
      if (!this.isEmailValid) {
        this.message = this.constants.getMessage('emailVal');
        this.statusMsg = "error";
      } else if (!this.isPinCodeValid) {
        this.message = this.constants.getMessage('pinCodeVal');
        this.statusMsg = "error";
      } else if (!this.isMobileNumberValid) {
        this.message = this.constants.getMessage('mobileNoVal');
        this.statusMsg = "error";
      } else if (!this.isPanValid) {
        this.message = this.constants.getMessage('panVal');
        this.statusMsg = "error";
      }
      this.setTimeOut();
    }
    // } else {
    //   this.message = this.constants.getMessage('requiredFieldVal');
    //   this.statusMsg = "error";
    //   this.setTimeOut();
    // }
  }

  setTimeOut() {
    setTimeout(() => {
      this.message = "";
      this.onlyNumber = "";
      this.onlyAlphabet = "";
      this.avoidSpecialChar = "";
    }, 3000);
  }

  addInspectionDetail() {
    let randomNum = Math.random().toString();
    let lastFive = randomNum.substr(randomNum.length - 5);
    let losNumber = this.executivePFIndex.user.brId.toString() + lastFive;
    console.log(losNumber);
    this.addUpdateInspection["borrowerUnitId"] = this.borrowerRefId;
    this.addUpdateInspection["losNum"] = JSON.parse(localStorage.getItem("loanNo"));
    this.addUpdateInspection["executiveReviewerPFIndex"] = "12345";
    this.addUpdateInspection["accountNum"] = "123456";
    this.addUpdateInspection["branchCode"] = this.executivePFIndex.user.brId.toString();
    this.addUpdateInspection["conductedDateTS"] = new Date().getTime().toString();
  }

  @Input() addUpdateInspection: AddUpdateInspection;
  addUpdateinspection() {
    var objectToPost: { loanInspectionStatusDetailsVO: object; appId: string; executivePFIndex: string; } = { loanInspectionStatusDetailsVO: this.addUpdateInspection, appId: "WEB-DIA", executivePFIndex: this.pfId };
    console.log(JSON.stringify(objectToPost));
    this._addUpdateInspectionService.postInspection(objectToPost).subscribe(
      data => {
        console.log(this.responseStatus = data);
        if (data.responseMessage == "Successful") {
          localStorage.setItem("borrowerRefID", this.borrowerRefId);
          localStorage.setItem("inspectionID", JSON.stringify(data.refId));
          this.message = this.constants.getMessage('successMsg');
          this.statusMsg = "success";
        } else {
          this.message = data.responseMessage;
          this.statusMsg = "error";
        }
      },
      err => {
        console.log(err);
        this.message = this.constants.getMessage('errorMsg');
        this.statusMsg = "error";
      },
      () => console.log('Request Completed')
    );
    this.status = true;
    this.setTimeOut();
  }

  ngOnInit() {
    this.unitDetails = new UnitDetails();
    if (localStorage.getItem("isLoanEditable") != null && localStorage.getItem("isLoanEditable") != "null") {
      if (localStorage.getItem("isLoanEditable") == "true") {
        this.isLoanEditable = true;
      } else {
        this.isLoanEditable = false;
      }
    }
    if (localStorage.getItem("isCollateralEditable") != null && localStorage.getItem("isCollateralEditable") != "null") {
      if (localStorage.getItem("isCollateralEditable") == "true") {
        this.isCollateralDisabled = true;
      } else {
        this.isCollateralDisabled = false;
      }
    }
    if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
      this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
      this.pfId = this.executivePFIndex.user.pfId.toString();
    }
    if (localStorage.getItem("borrowerRefID") != null && localStorage.getItem("borrowerRefID") != "null") {
      this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
      this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };
      this.service.getUnitDetails(this.apiRequest).subscribe(response => {
        if (response.borrowerUnit) {
          this.unitDetails = response.borrowerUnit;
          this.unitDetails = this.utilsService.dataSourceCheck(this.unitDetails);
          if (!this.unitDetails.nameOfUnit) {
            if (JSON.parse(localStorage.getItem('UnitName'))) {
              this.unitDetails.nameOfUnit = JSON.parse(localStorage.getItem('UnitName'));
            }
          }
          if (this.unitDetails.collateralIdentified == "1") {
            this.collateralDisable = true;
          } else {
            this.collateralDisable = false;
          }
          if (this.unitDetails.guarantorsPresent == "1") {
            this.guarantorDisable = true;
          } else {
            this.guarantorDisable = false;
          }
          this.addUnitAddress();
          if (this.unitDetails.dateOfEstablishTS != null) {
            if (+this.unitDetails.dateOfEstablishTS == 0) {
              this.establishmentDate = new Date();
            } else {
              this.establishmentDate = new Date(this.unitDetails.dateOfEstablishTS);
            }
          }
        }
      })
    }
    else {
      this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
      if (this.loanDetailsWithOutInspection) {
        if (this.loanDetailsWithOutInspection.borrowerDetails) {
          this.unitDetails = this.loanDetailsWithOutInspection.borrowerDetails;
          this.unitDetails = this.utilsService.dataSourceCheckFromLOS(this.unitDetails);
          if (this.loanDetailsWithOutInspection.borrowerPersonalList && this.loanDetailsWithOutInspection.borrowerPersonalList.length > 0) {
            this.unitDetails["noOfKeyPerson"] = this.loanDetailsWithOutInspection.borrowerPersonalList.length;
            this.unitDetails["collateralIdentified"] = "1";
            this.unitDetails["noOfKeyPersonStatus"] = "LOS"
            this.unitDetails["collateralIdentifiedStatus"] = "LOS";
          }
          if (this.loanDetailsWithOutInspection.collateralList && this.loanDetailsWithOutInspection.collateralList.length > 0) {
            this.unitDetails["chkCollateral"] = "1";
            this.unitDetails["chkCollateralStatus"] = "LOS";
          }
          if (this.loanDetailsWithOutInspection.gurantorList && this.loanDetailsWithOutInspection.gurantorList.length > 0) {
            this.unitDetails["noOfGuarantors"] = this.loanDetailsWithOutInspection.gurantorList.length;
            this.unitDetails["guarantorsPresent"] = "1";
            this.unitDetails["noOfGuarantorsStatus"] = "LOS"
            this.unitDetails["guarantorsPresentStatus"] = "LOS";
          }
          if (!this.unitDetails.nameOfUnit) {
            if (JSON.parse(localStorage.getItem('UnitName'))) {
              this.unitDetails.nameOfUnit = JSON.parse(localStorage.getItem('UnitName'));
            }
          }
          delete this.unitDetails["borrowerRefId"];
          if (this.unitDetails.collateralIdentified == "1") {
            this.collateralDisable = true;
          } else {
            this.collateralDisable = false;
          }
          if (this.unitDetails.guarantorsPresent == "1") {
            this.guarantorDisable = true;
          } else {
            this.guarantorDisable = false;
          }
          this.addUnitAddress();
          this.establishmentDate = new Date();
          // if (this.unitDetails.dateOfEstablishTS != null) {
          //   if (+this.unitDetails.dateOfEstablishTS == 0) {
          //     this.establishmentDate = new Date();
          //   } else {
          //     this.establishmentDate = new Date(this.unitDetails.dateOfEstablishTS);
          //   }
          // }
        } else {
          if (JSON.parse(localStorage.getItem('UnitName'))) {
            this.unitDetails.nameOfUnit = JSON.parse(localStorage.getItem('UnitName'));
          }
          delete this.unitDetails.field_status;
          delete this.unitDetails.field_statusMap;
          this.unitDetails = this.utilsService.dataSourceCheckFromLOS(this.unitDetails);
        }
      } else {
        delete this.unitDetails.field_status;
        delete this.unitDetails.field_statusMap;
        this.unitDetails = this.utilsService.dataSourceCheckFromLOS(this.unitDetails);
      }
      this.statusMsg = this.constants.getMessage('borrowerVal');
      console.log(this.statusMsg);
    }

  }
  addUnitAddress() {
    this.unitAddress["plotBuildingFlatNum"] = this.unitDetails.plotBuildingFlatNum;
    this.unitAddress["addressLine1"] = this.unitDetails.addressLine1;
    this.unitAddress["addressLine2"] = this.unitDetails.addressLine2;
    this.unitAddress["state"] = this.unitDetails.state;
    this.unitAddress["city"] = this.unitDetails.city;
    this.unitAddress["district"] = this.unitDetails.district;
    this.unitAddress["blockTehsilSubdistrict"] = this.unitDetails.blockTehsilSubdistrict;
    this.unitAddress["village"] = this.unitDetails.village;
    this.unitAddress["pinCode"] = this.unitDetails.pinCode;
    this.unitAddress["mobileNum"] = this.unitDetails.mobileNum;
    this.unitAddress["landlineNum"] = this.unitDetails.landlineNum;
    this.unitAddress["emailId"] = this.unitDetails.emailId;
    this.registredAddressUnit = localStorage.setItem("registredAddressUnit", JSON.stringify(this.unitAddress));
  }
  keypressCheck(data, field) {
    this.cityVal = false;
    this.blockVal = false;
    this.activityVal = false;
    this.panVal = false;
    this.districtVal = false;
    this.villageVal = false;
    this.pincodeVal = false;
    this.mobileVal = false;
    this.landlineVal = false;
    switch (field) {
      case "Activity":
        if (this.utilsService.validString) {
          this.activityVal = true;
        } else {
          this.activityVal = false;
        }
        break;

      case "PAN":
        if (this.utilsService.validString) {
          this.panVal = true;
        } else {
          this.panVal = false;
        }
        break;

      case "City":
        if (this.utilsService.validString) {
          this.cityVal = true;
        } else {
          this.cityVal = false;
        }
        break;

      case "Block":
        if (this.utilsService.validString) {
          this.blockVal = true;
        } else {
          this.blockVal = false;
        }
        break;

      case "District":
        if (this.utilsService.validString) {
          this.districtVal = true;
        } else {
          this.districtVal = false;
        }
        break;

      case "Village":
        if (this.utilsService.validString) {
          this.villageVal = true;
        } else {
          this.villageVal = false;
        }
        break;

      case "Pincode":
        if (this.utilsService.validString) {
          this.pincodeVal = true;
        } else {
          this.pincodeVal = false;
        }
        break;

      case "Mobile":
        if (this.utilsService.validString) {
          this.mobileVal = true;
        } else {
          this.mobileVal = false;
        }
        break;

      case "Landline":
        if (this.utilsService.validString) {
          this.landlineVal = true;
        } else {
          this.landlineVal = false;
        }
        break;

    }
    return data;
  }

  onBlurValidation(value, field) {
    if (value) {
      this.cityValBlur = false;
      this.blockValBlur = false;
      this.activityValBlur = false;
      this.panValBlur = false;
      this.districtValBlur = false;
      this.villageValBlur = false;
      this.pincodeValBlur = false;
      this.mobileValBlur = false;
      this.landlineValBlur = false;
      this.onlyNumber = "Only numbers are allowed";
      this.onlyAlphabet = "Please enter only alphabets";
      this.avoidSpecialChar = "Please enter alphabets & numbers";
      switch (field) {
        case "Mobile":
          if (!(value.match(/^\d+$/))) {
            this.unitDetails.mobileNum = '';
            this.mobileValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Activity":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.unitDetails.activityOfUnit = '';
            this.activityValBlur = true;
            this.setTimeOut();
          }
          break;

        case "PAN":
          if (value.match(/[\W]/)) {
            this.unitDetails.panTan = '';
            this.panValBlur = true;
            this.setTimeOut();
          }
          break;

        case "City":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.unitDetails.city = '';
            this.cityValBlur = true;
            this.setTimeOut();
          }
          break;

        case "District":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.unitDetails.district = '';
            this.districtValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Block":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.unitDetails.blockTehsilSubdistrict = '';
            this.blockValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Village":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.unitDetails.village = '';
            this.villageValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Pincode":
          if (!(value.match(/^\d+$/))) {
            this.unitDetails.pinCode = '';
            this.pincodeValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Landline":
          if (!(value.match(/^\d+$/))) {
            this.unitDetails.landlineNum = '';
            this.landlineValBlur = true;
            this.setTimeOut();
          }
          break;
      }
    }
  }
}