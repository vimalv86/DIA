import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'presanction',
    templateUrl: './presanction.component.html',
    styleUrls: ['./presanction.component.scss', '../../module.component.scss']
})

export class PresanctionComponent implements OnInit {
    private appUrl:string;
    private items: any[];
    
    ngOnInit() {
        window.scrollTo(0, 0);
        if(localStorage.getItem("appUrl") != null) {
            this.appUrl = localStorage.getItem("appUrl").toString();
        }
        
        this.items = [
            {label:'Home', url: this.appUrl + 'sme'},
            {label:'Presanction Inspection'}
        ];
    }
}