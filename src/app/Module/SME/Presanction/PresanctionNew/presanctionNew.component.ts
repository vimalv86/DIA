import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Constants } from '../../../../Shared/Constants/constants.service';
import { UtilsService } from '../../../../Shared/UtilsService/utils.service';

@Component({
    selector: 'presanction-new',
    templateUrl: './presanctionNew.component.html',
    styleUrls: ['./presanctionNew.component.scss', '../../../module.component.scss'],
    providers: [
        Constants,
        UtilsService
    ]
})

export class PresanctionNewComponent implements OnInit {
    constructor(private router: Router, private constants: Constants, private utilsService: UtilsService) {
        this.currentDate = new Date();
    }

    private searchLoanWithout: boolean = false;
    private losNumber: any;
    private branchCode: string;
    private fromDate: any;
    private toDate: any;
    private fromTimeStamp: any;
    private toTimeStamp: any;
    private searchWithLos: string = "LOS Number";
    private searchWithBranch: string;
    private image: string;
    private statusMsg: string;
    private timer: any;
    private LOSVal: boolean = false;
    private BranchVal: boolean = false;
    private currentDate: any;
    private LOSValBlur: boolean = false;
    private BranchValBlur: boolean = false;
    private onlyNumber: string;

    searchCategory(status) { //radio button selection
        this.router.navigate(['sme/detail/presanctionNew']);
        this.searchWithLos = "";
        this.searchWithBranch = "";
        clearTimeout(this.timer);
        if (status === 1) {
            // this.reviewerDetail = false;
            this.searchLoanWithout = false;
            this.branchCode = "";
            this.fromDate = "";
            this.toDate = "";
            this.searchWithLos = "LOS Number";
        } else if (status === 2) {
            // this.reviewerDetail = false;
            this.searchLoanWithout = false;
            this.losNumber = null;
            this.searchWithBranch = "Branch Code";
        } else if (status === 3) {
            // this.reviewerDetail = true;
            this.searchLoanWithout = true;
            this.losNumber = null;
            this.branchCode = "";
            this.fromDate = "";
            this.toDate = "";
            this.searchWithLos = "";
            this.searchWithBranch = "";
        }
    }

    searchLoanList = function () {
        clearTimeout(this.timer);
        localStorage.setItem("rejectedCount", "0");
        if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            if (this.executivePFIndex.user) {
                this.pfId = this.executivePFIndex.user.pfId.toString();
                localStorage.setItem("isLoanEditable", "true");
                if (this.branchCode) {
                    localStorage.setItem("isCollateralEditable", "false");
                    let convertfromDateFormat = new Date(this.fromDate);
                    this.fromTimeStamp = convertfromDateFormat.getTime();
                    let convertToDateFormat = new Date(this.toDate);
                    this.toTimeStamp = convertToDateFormat.getTime();
                    if (this.fromTimeStamp > this.toTimeStamp) {
                        this.statusMsg = this.constants.getMessage('DateVal');
                        console.log(this.statusMsg);
                        this.setTimeOut();
                    } else {
                        this.router.navigate(['sme/detail/presanctionNew/presanctionList', { branchCode: this.branchCode, fromTimeStamp: this.fromTimeStamp, toTimeStamp: this.toTimeStamp }]);
                    }
                } else if (this.losNumber) {
                    if (this.losNumber.charAt(0) == '0') {
                        this.statusMsg = this.constants.getMessage('LOSVal');
                        console.log(this.statusMsg);
                        this.setTimeOut();
                    } else {
                        localStorage.setItem("loanNo", JSON.stringify(this.losNumber));
                        localStorage.setItem("isCollateralEditable", "false");
                        this.router.navigate(['sme/detail/presanctionNew/presanctionList', { losNumber: this.losNumber }]);
                    }
                } else {
                    localStorage.setItem("UnitName", null);
                    localStorage.setItem("loanDetailsWithOutInspection", null);
                    localStorage.setItem("isCollateralEditable", "true");
                    localStorage.setItem("borrowerRefID", null);
                    localStorage.setItem("loanNo", null);
                    localStorage.setItem("inspectionID", null);
                    localStorage.setItem("registredAddressUnit", null);
                    localStorage.setItem("salutationAddress", null);
                    this.router.navigate(['sme/presanctionDetail/borrowerDetails/unitDetails']);
                }
            } else {
                this.statusMsg = this.constants.getMessage('PFIDVal');
                console.log(this.statusMsg);
                this.setTimeOut();
            }
        } else {
            this.statusMsg = this.constants.getMessage('PFIDVal');
            console.log(this.statusMsg);
        }
    };

    setTimeOut() {
        this.timer = setTimeout(() => {
            this.statusMsg = "";
            this.onlyNumber = "";
        }, 3000);
    }


    ngOnInit() {
    }

    keypressCheck(data, field) {
        this.LOSVal = false;
        switch (field) {
            case "LOS":
                if (this.utilsService.validString) {
                    this.LOSVal = true;
                    this.BranchVal = false;
                } else {
                    this.LOSVal = false;
                }
                break;

            case "Branch":
                if (this.utilsService.validString) {
                    this.BranchVal = true;
                } else {
                    this.BranchVal = false;
                }
                break;
        }
        return data;
    }

    onBlurValidation(value, field) {
        if (value) {
            this.LOSValBlur = false;
            this.BranchValBlur = false;
            this.onlyNumber = "Only numbers are allowed";
            switch (field) {
                case "LOS":
                    if (!(value.match(/^\d+$/))) {
                        this.losNumber = '';
                        this.LOSValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Branch":
                    if (!(value.match(/^\d+$/))) {
                        this.branchCode = '';
                        this.BranchValBlur = true;
                        this.setTimeOut();
                    }
                    break;
            }
        }
    }
}