import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Constants } from '../../../../Shared/Constants/constants.service';

import { SmePresanctionSearchLoanService } from '../../../../Service/SME/Presanction/SearchLoan/searchLoan.service';
@Component({
    selector: 'presanctionList',
    templateUrl: './presanctionList.component.html',
    styleUrls: ['./presanctionList.component.scss', '../../../module.component.scss'],
    providers: [
        Constants
    ]
})

export class PresanctionListComponent {
    private searchLosList: any;
    private responseMessage: any;
    private losNumber: number;
    private branchCode: string;
    private apiRequest: any;
    private apiLOSRequest: any;
    private fromTimeStamp: any;
    private toTimeStamp: any;
    private loanDetails: any = {};
    private listIstoHide: boolean;
    private executivePFIndex: any;
    private pfId: string;
    private timer: any;

    constructor(private service: SmePresanctionSearchLoanService, private router: Router, private route: ActivatedRoute, private constants: Constants) { }
    ngOnInit() {
        window.scrollTo(0, 0);
        this.route
            .params
            .subscribe(params => {
                this.branchCode = params['branchCode'];
                this.losNumber = params['losNumber'];
                this.fromTimeStamp = params['fromTimeStamp'];
                this.toTimeStamp = params['toTimeStamp'];
            });
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
            this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId };
            this.apiLOSRequest = { "executivePFIndex": this.pfId };
            if (this.branchCode) {
                this.apiRequest['branchCode'] = this.branchCode;
                this.apiRequest['toDateTS'] = this.toTimeStamp;
                this.apiRequest['fromDateTS'] = this.fromTimeStamp;
                this.service.getSmeSearchLoanWithBranch(this.apiRequest).subscribe(response => {
                    if (response.responseCode == 200) {
                        this.searchLosList = response.loanInspectionList;
                        if (this.searchLosList.length == 0) {
                            this.responseMessage = this.constants.getMessage('LOSDataError');
                            this.setTimeOut();
                        }
                    } else {
                        this.setTimeOut();
                        console.log(response);
                        this.responseMessage = response.responseMessage;
                    }
                })
            } else {
                this.apiLOSRequest['key'] = this.losNumber;
                this.listIstoHide = true;
                localStorage.setItem("UnitName", null);
                this.getSmeSearchLoanWithLOS();
            }
        } else {
            this.responseMessage = this.constants.getMessage('PFIDVal');
        }
    }

    getSmeSearchLoanWithLOS() {
        this.service.getSmeSearchLoanWithLOS(this.apiLOSRequest).subscribe(response => {
            if (response.responseCode == 200) {
                if (response.inspectionId) {
                    localStorage.setItem("borrowerRefID", JSON.stringify(response.borrowerDetails.borrowerRefId));
                    localStorage.setItem("loanNo", JSON.stringify(response.loanNum));
                    localStorage.setItem("inspectionID", JSON.stringify(response.inspectionId));
                    localStorage.setItem("UnitName", null);
                    if (this.apiRequest['key']) {
                        this.router.navigate(['/sme/presanctionDetail']);
                        return;
                    }
                } else {
                    localStorage.setItem("loanDetailsWithOutInspection", JSON.stringify(response));
                    localStorage.setItem("isCollateralEditable", "true");
                    localStorage.setItem("borrowerRefID", null);
                    localStorage.setItem("inspectionID", null);
                    localStorage.setItem("registredAddressUnit", null);
                    localStorage.setItem("salutationAddress", null);
                    this.router.navigate(['sme/presanctionDetail/borrowerDetails/unitDetails']);
                }

            } else {
                this.setTimeOut();
                console.log(response);
                this.responseMessage = response.responseMessage;
            }
        })
    }

    ngOnDestroy() {
        clearTimeout(this.timer);
    }

    setTimeOut() {
        this.timer = setTimeout(() => {
            this.router.navigate(['sme/detail/presanctionNew']);
        }, 3000);
    }

    selectLOS(losDetails) {
        localStorage.setItem("loanDetailsWithOutInspection", null);
        if (losDetails.borrowerRefId) {
            localStorage.setItem("borrowerRefID", JSON.stringify(losDetails.borrowerRefId));
        } else {
            localStorage.setItem("borrowerRefID", null);
        }
        localStorage.setItem("loanNo", JSON.stringify(losDetails.accountNum));
        localStorage.setItem("inspectionID", JSON.stringify(losDetails.inspectionId));
        localStorage.setItem("UnitName", JSON.stringify(losDetails.borrowerUnitId));
        this.apiLOSRequest = { "executivePFIndex": this.pfId, "key": losDetails.accountNum };
        this.getSmeSearchLoanWithLOS();
       // this.router.navigate(['/sme/presanctionDetail']);
    }
}
