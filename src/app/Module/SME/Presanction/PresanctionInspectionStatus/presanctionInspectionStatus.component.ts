import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { DIAConstants } from '../../../../Shared/constants';

import { SmeInspectionStatus } from '../../../../Service/SME/Presanction/InspectionStatus/inspectionStatus';
import { SmePresanctionInspectionStatusService } from '../../../../Service/SME/Presanction/InspectionStatus/inspectionStatus.service';

@Component({
    selector: 'presanctionInspection',
    templateUrl: './presanctionInspectionStatus.component.html',
    styleUrls: ['./presanctionInspectionStatus.component.scss', '../../../module.component.scss'],
    providers: [
        SmeInspectionStatus,
        SmePresanctionInspectionStatusService
    ]
})


export class PresanctionInspectionStatusComponent implements OnInit {
    private loading: boolean;
    private apiRequest: any;
    private rejectedList: any[];
    private reviewStatus: any[];
    private executivePFIndex: any;
    private pfId: string;
    private isDisabled: boolean;

    constructor(private service: SmePresanctionInspectionStatusService, private router: Router, private route: ActivatedRoute) {
        this.reviewStatus = DIAConstants.reviewStatus;
    }

    @Input() smeInspectionStatus: SmeInspectionStatus;

    ngOnInit() {
        this.loading = true;
        this.smeInspectionStatus = new SmeInspectionStatus();
        window.scrollTo(0, 0);
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            if (this.executivePFIndex.user) {
                this.pfId = this.executivePFIndex.user.pfId.toString();
                this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId };
                this.service.getSmeInspectionStatus(this.apiRequest).subscribe(response => {
                    if (response.inspectionList) {
                        this.isDisabled = false;
                        this.rejectedList = response.inspectionList;
                        this.reviewStatus = [];
                        this.reviewStatus.push({ label: "All Inspections", value: '' });
                        for (let k = 0; k < this.rejectedList.length; k++) {
                          if(this.rejectedList[k].reviewStatus == 'PendingForReview') {
                            this.rejectedList[k].reviewStatus = 'Pending For Review';
                          }
                          if(this.rejectedList[k].reviewStatus == 'WebIncomplete') {
                            this.rejectedList[k].reviewStatus = 'Web Incomplete';
                          }
                          if(this.rejectedList[k].reviewStatus == 'MobIncomplete') {
                            this.rejectedList[k].reviewStatus = 'Mob Incomplete';
                          }
                            let reviewStatusObj = {
                                label: this.rejectedList[k].reviewStatus,
                                value: this.rejectedList[k].reviewStatus
                            }
                            this.reviewStatus.push(reviewStatusObj);
                        }
                        this.reviewStatus = this.reviewStatus.filter((object, index, duplicate) =>
                            index === duplicate.findIndex((t) => (
                                t.label === object.label
                            ))
                        )                        
                        console.log(this.rejectedList);
                    } else {
                        this.isDisabled = true;
                    }
                })
            }
        }
    }

    viewLOS(data) {
        console.log(data);
        localStorage.setItem("borrowerRefID", null);
        localStorage.setItem("loanNo", null);
        localStorage.setItem("inspectionID", null);
        localStorage.setItem("loanDetailsWithOutInspection", null);
        localStorage.setItem("UnitName", null);
        localStorage.setItem("borrowerRefID", JSON.stringify(data.borrowerRefId));
        localStorage.setItem("loanNo", JSON.stringify(data.losNum));
        localStorage.setItem("inspectionID", JSON.stringify(data.inspectionId));
        console.log(data.reviewStatus);
        if (data.reviewStatus == "Pending For Review" || data.reviewStatus == "PendingForReview" || data.reviewStatus == "Accepted") {
            localStorage.setItem("isLoanEditable", "false");
            localStorage.setItem("isCollateralEditable", "false");
        } else {
            localStorage.setItem("isLoanEditable", "true");
            localStorage.setItem("isCollateralEditable", "true");
        }
        this.router.navigate(['/sme/presanctionDetail']);
    }
}