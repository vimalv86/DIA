import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UnitDetails } from '../../../../Service/SME/Presanction/BorrowerDetails/UnitDetails/unitDetails';
import { SmePresanctionInspectionDetailService } from '../../../../Service/SME/Presanction/PresanctionDetail/presanctionDetail.service';


@Component({
    selector: 'presanctionDetail',
    templateUrl: './presanctionDetail.component.html',
    styleUrls: ['./presanctionDetail.component.scss', '../../../module.component.scss'],
    providers: [
        SmePresanctionInspectionDetailService,
    ]
})

export class PresanctionDetailComponent implements OnInit {
    private apiRequest: object;
    private items: any[];
    private nameOfUnit: string;
    private executivePFIndex: any;
    private inspectionID: string;
    private losNum: string;
    private appUrl: string;
    private pfId: string;
    private noLosNo: boolean;
    private initiateDate: any;
    private isValidationComplete: boolean = true;
    private refId: any;
    private loanDetailsWithOutInspection: any;

    constructor(private service: SmePresanctionInspectionDetailService, private route: ActivatedRoute) { }

    @Input() unitDetails: UnitDetails;
    ngOnInit() {
        this.unitDetails = new UnitDetails();
        window.scrollTo(0, 0);
        this.initiateDate = new Date();

        if (localStorage.getItem("appUrl") != null) {
            this.appUrl = localStorage.getItem("appUrl").toString();
        }

        this.nameOfUnit = localStorage.getItem("nameOfUnit")
        if (JSON.parse(localStorage.getItem("inspectionID"))) {
            this.refId = JSON.parse(localStorage.getItem("inspectionID"));
            this.isValidationComplete = false;
        }
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null && localStorage.getItem("inspectionID") != "null" && localStorage.getItem("inspectionID") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
            if (localStorage.getItem("loanNo") != "null" && localStorage.getItem("loanNo") != null) {
                this.losNum = JSON.parse(localStorage.getItem('loanNo'));
            } else {
                this.losNum = '';
            }
            this.inspectionID = JSON.parse(localStorage.getItem('inspectionID'));
            this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.inspectionID };
            this.service.getSmeInspectionDetails(this.apiRequest).subscribe(response => {
                if (response.borrowerDetails) {
                    this.unitDetails = response.borrowerDetails;
                    console.log(JSON.stringify(this.unitDetails));

                    console.log(this.initiateDate);
                    if (!this.unitDetails.nameOfUnit) {
                        if (JSON.parse(localStorage.getItem("UnitName"))) {
                            this.unitDetails.nameOfUnit = JSON.parse(localStorage.getItem("UnitName"));
                        }
                    }
                    if (!this.losNum) {
                        if (JSON.parse(localStorage.getItem("loanNo"))) {
                            this.losNum = JSON.parse(localStorage.getItem("loanNo"));
                        }
                    }
                }
                localStorage.setItem("rejectedCount", response.reject_count);
                localStorage.setItem("salutationAddress", JSON.stringify(response.contactPersonDetails));
                if (response.dateOfInspectionTS) {
                    if (+response.dateOfInspectionTS == 0) {
                        this.initiateDate = new Date();
                    } else {
                        this.initiateDate = new Date(response.dateOfInspectionTS);
                    }
                } else {
                    this.initiateDate = new Date();
                }
            })
        }
        else {
            this.initiateDate = new Date();
            if (JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"))) {
                this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
                if (this.loanDetailsWithOutInspection.borrowerDetails) {
                    this.unitDetails.nameOfUnit = this.loanDetailsWithOutInspection.borrowerDetails.nameOfUnit;
                }
            }
            if (JSON.parse(localStorage.getItem("UnitName"))) {
                this.unitDetails.nameOfUnit = JSON.parse(localStorage.getItem("UnitName"));
                this.isValidationComplete = false;
            }
            if (JSON.parse(localStorage.getItem("loanNo"))) {
                this.losNum = JSON.parse(localStorage.getItem("loanNo"));
                this.isValidationComplete = false;
            }
            if (this.isValidationComplete) {
                this.noLosNo = true;
            }
            console.log("Error");
        }

        this.items = [
            { label: 'Home', url: this.appUrl + 'sme' },
            { label: 'Presanction', url: this.appUrl + 'sme/detail/presanctionNew' },
            { label: 'New Inspection' }
        ];
    }
}