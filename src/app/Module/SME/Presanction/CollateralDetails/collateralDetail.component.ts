import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DIAConstants } from '../../../../Shared/constants';
import { Constants } from '../../../../Shared/Constants/constants.service';

import { CollateralDetails } from '../../../../Service/SME/Presanction/CollateralDetails/collateralDetails';
import { AddCollateralDetailsService } from '../../../../Service/SME/Presanction/CollateralDetails/AddCollateralDetails/addCollateralDetails.service';
import { SmePresanctionGetCollateralListService } from '../../../../Service/SME/Presanction/CollateralDetails/GetCollateralDetails/getCollateralDetails.service';
import { UtilsService } from '../../../../Shared/UtilsService/utils.service';

import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
    selector: 'collateralDetail',
    templateUrl: './collateralDetail.component.html',
    styleUrls: ['./collateralDetail.component.scss', '../../../module.component.scss'],
    providers: [
        AddCollateralDetailsService,
        SmePresanctionGetCollateralListService,
        UtilsService,
        Constants
    ]
})

export class CollateralDetailComponent implements OnInit {
    private establishmentWebVO: object;
    private apiRequest: any;
    private collateralList: any;
    private message: string;
    private statusMsg: string;
    private states: any[];
    private upToFive: any[];
    private responseStatus: Object = [];
    private status: boolean;
    private noData: boolean;
    private executivePFIndex: any;
    private borrowerRefId: string;
    private indexVal: any;
    private checked: boolean;
    private registredAddressUnit: any = {};
    private unRegistredAddressUnit: any = {};
    private pfId: string;
    private images: any = [];
    private inspectionId: string;
    private galleryOptionsSelfy: NgxGalleryOptions[];
    private galleryImagesSelfy: NgxGalleryImage[];
    private galleryOptions: NgxGalleryOptions[];
    private galleryImages: NgxGalleryImage[];
    private isLoanEditable: boolean;
    private isPinCodeValid: boolean = true;
    private isMobileNumberValid: boolean = true;
    private loanDetailsWithOutInspection: any;
    private cityVal: boolean = false;
    private districtVal: boolean = false;
    private blockVal: boolean = false;
    private villageVal: boolean = false;
    private pincodeVal: boolean = false;
    private mobileVal: boolean = false;
    private collateralListDupl = null;
    private cityValBlur: boolean = false;
    private districtValBlur: boolean = false;
    private blockValBlur: boolean = false;
    private villageValBlur: boolean = false;
    private pincodeValBlur: boolean = false;
    private mobileValBlur: boolean = false;
    private onlyNumber: string;
    private onlyAlphabet: string;
    private avoidSpecialChar: string;
    // private dateOfInspectionTSTemp: boolean = false;
    // private otherSource: boolean = false;
    // private commentsOnActivity: boolean = false;
    // private commentOnGeneralWorkFirm: boolean = false;
    // private commentsOnDataCollection: boolean = false;
    // private remarkablePointFromBorrower: boolean = false;
    // private positionOfStatutoryDues: boolean = false;
    // private nameOfAccompanyingOfficial: boolean = false;
    // private pfIdOfReviewerbol: boolean = false;

    constructor(private _addCollateralDetailsService: AddCollateralDetailsService, private service: SmePresanctionGetCollateralListService, private route: ActivatedRoute, private utilsService: UtilsService, private constants: Constants) {
        this.states = DIAConstants.states;
        this.upToFive = DIAConstants.upToFive;
    }

    @Input() collateralDetails: CollateralDetails;


    addCollateralDetail() {
        if (this.inspectionId) {
            //   if (this.collateralList[this.indexVal].dateOfInspectionTSTemp && this.collateralList[this.indexVal].building_no && this.collateralList[this.indexVal].addressLine1 && this.collateralList[this.indexVal].state && this.collateralList[this.indexVal].city && this.collateralList[this.indexVal].mobile_number && this.collateralList[this.indexVal].pinCode) {
            if (this.collateralList[this.indexVal].pinCode) {
                if (this.collateralList[this.indexVal].pinCode.length < 6) {
                    this.isPinCodeValid = false;
                } else {
                    this.isPinCodeValid = true;
                }
            } else {
                this.isPinCodeValid = true;
            }
            if (this.collateralList[this.indexVal].mobile_number) {
                if (this.collateralList[this.indexVal].mobile_number.length < 10) {
                    this.isMobileNumberValid = false;
                } else {
                    this.isMobileNumberValid = true;
                }
            } else {
                this.isMobileNumberValid = true;
            }

            if (this.isMobileNumberValid && this.isPinCodeValid) {
                let actualTime = this.collateralList[this.indexVal].dateOfInspectionTSTemp;
                let timeStamp = new Date(actualTime).getTime().toString();
                this.collateralList[this.indexVal].dateOfInspectionTS = timeStamp;
                delete this.collateralList[this.indexVal].dateOfInspectionTSTemp;
                this.checked = this.collateralList[this.indexVal].checked;
                delete this.collateralList[this.indexVal].checked;
                delete this.collateralList[this.indexVal].message;
                delete this.collateralList[this.indexVal].imageListStatus;
                if (this.collateralList[this.indexVal].losNum) {
                    delete this.collateralList[this.indexVal].losNum;
                }
                if (this.inspectionId && !this.collateralList[this.indexVal].inspectionId) {
                    this.collateralList[this.indexVal].inspectionId = this.inspectionId;
                }
                this.collateralList[this.indexVal] = this.utilsService.dataSourceSave(this.collateralList[this.indexVal]);
                var objectToPost: { collateralWebVO: object; executivePFIndex: string; appId: string } = { collateralWebVO: this.collateralList[this.indexVal], executivePFIndex: this.pfId, appId: "WEB-DIA" };
                console.log(JSON.stringify(objectToPost));
                this._addCollateralDetailsService.postComment(objectToPost).subscribe(
                    data => {
                        console.log(this.responseStatus = data);
                        if (data.responseMessage == "Successful") {
                            this.collateralList[this.indexVal].dateOfInspectionTSTemp = new Date(parseInt(this.collateralList[this.indexVal].dateOfInspectionTS));
                            this.collateralList[this.indexVal].message = this.constants.getMessage('successMsg');
                            this.collateralList[this.indexVal].collateralRefId = data.refId;
                            this.statusMsg = "success";
                        } else {
                            this.collateralList[this.indexVal].message = data.responseMessage;
                            this.statusMsg = "error";
                        }
                        this.setTimeOut();
                    },
                    err => {
                        console.log(err);
                        this.collateralList[this.indexVal].message = this.constants.getMessage('errorMsg');
                        this.statusMsg = "error";
                        this.setTimeOut();
                    },
                    () => console.log('Request Completed')
                );
                this.collateralList[this.indexVal].checked = this.checked;
                this.status = true;
            } else {
                if (!this.isPinCodeValid) {
                    this.collateralList[this.indexVal].message = this.constants.getMessage('pinCodeVal');
                    this.statusMsg = "error";
                } else if (!this.isMobileNumberValid) {
                    this.collateralList[this.indexVal].message = this.constants.getMessage('mobileNoVal');
                    this.statusMsg = "error";
                }
                this.setTimeOut();
            }
            // } else {
            //     this.collateralList[this.indexVal].message = this.constants.getMessage('requiredFieldVal');
            //     this.statusMsg = "error";
            //     this.setTimeOut();
            // }
        } else {
            this.collateralList[this.indexVal].message = this.constants.getMessage('unitDetailVal');
            this.statusMsg = "error";
            this.setTimeOut();
        }

    }

    setTimeOut() {
        setTimeout(() => {
            this.collateralList[this.indexVal].message = "";
            this.onlyNumber = "";
            this.onlyAlphabet = "";
            this.avoidSpecialChar = "";
        }, 3000);
    }

    ngOnInit() {
        window.scrollTo(0, 0);
        this.collateralDetails = new CollateralDetails();
        if (localStorage.getItem("isLoanEditable") != null && localStorage.getItem("isLoanEditable") != "null") {
            if (localStorage.getItem("isLoanEditable") == "true") {
                this.isLoanEditable = true;
            } else {
                this.isLoanEditable = false;
            }
        }
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null && localStorage.getItem("inspectionID") != "null" && localStorage.getItem("inspectionID") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
            // this.loanNumber = JSON.parse(localStorage.getItem('loanNo'));
            this.inspectionId = JSON.parse(localStorage.getItem('inspectionID'));
            this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
            this.getCollateralDetails();
        } else {
            this.getDataFromLocalStorage();
        }
    }

    getCollateralDetails() {
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.inspectionId };
        this.service.getCollateralList(this.apiRequest).subscribe(response => {
            if (response.collateralWebList) {
                this.registredAddressUnit = JSON.parse(localStorage.getItem("registredAddressUnit"));
                this.collateralList = response.collateralWebList;
                if (this.collateralList.length > 0) {
                    this.unRegistredAddressUnit = JSON.stringify(this.collateralList[0]);
                    for (let k = 0; k < this.collateralList.length; k++) {
                        if (this.collateralList[k].collateralRefId == null) {
                            this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
                            if (this.loanDetailsWithOutInspection) {
                                if (this.loanDetailsWithOutInspection.collateralList && this.loanDetailsWithOutInspection.collateralList.length > 0) {
                                    if (this.loanDetailsWithOutInspection.collateralList.length > k) {
                                        this.collateralList[k] = this.loanDetailsWithOutInspection.borrowerPersonalList[k];
                                        this.collateralList[k].collateralRefId = null;
                                        this.collateralList[k] = this.setCollateralObject(this.collateralList[k]);
                                        this.collateralList[k] = this.utilsService.dataSourceCheckFromLOS(this.collateralList[k]);
                                    } else {
                                        this.collateralList[k] = this.utilsService.dataSourceCheck(this.collateralList[k]);
                                    }
                                }
                            } else {
                                this.collateralList[k] = this.utilsService.dataSourceCheck(this.collateralList[k]);
                            }
                        } else {
                            this.collateralList[k] = this.utilsService.dataSourceCheck(this.collateralList[k]);
                        }
                    }

                    // if (this.unitDetails.dateOfInspectionTS != null) {
                    //     this.establishmentDate = new Date(this.unitDetails.dateOfInspectionTS);
                    // }
                    this.imageGalleryOptions();
                    for (let i = 0; i < this.collateralList.length; i++) {
                        this.collateralList[i].checked = false;
                        this.collateralList[i].message = '';
                        //delete this.collateralList[i].imageList;
                        //   delete this.collateralList[i].addressLine2;
                        if (this.collateralList[i].dateOfInspectionTS != null) {
                            if (+this.collateralList[i].dateOfInspectionTS == 0) {
                                this.collateralList[i].dateOfInspectionTSTemp = new Date();
                            } else {
                                this.collateralList[i].dateOfInspectionTSTemp = new Date(this.collateralList[i].dateOfInspectionTS);
                            }
                        }
                    }
                    this.collateralListDupl = this.collateralList;
                } else {
                    this.getDataFromLocalStorage();
                }
                console.log(this.collateralList);
            } else {
                this.getDataFromLocalStorage();
            }
        })
    }

    getDataFromLocalStorage() {
        this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
        if (this.loanDetailsWithOutInspection) {
            if (this.loanDetailsWithOutInspection.collateralList && this.loanDetailsWithOutInspection.collateralList.length > 0) {
                this.collateralList = this.loanDetailsWithOutInspection.collateralList;
                for (let k = 0; k < this.collateralList.length; k++) {
                    this.collateralList[k] = this.setCollateralObject(this.collateralList[k]);
                }
                this.collateralList = this.utilsService.dataSourceCheckFromLOSArray(this.collateralList);
                this.collateralListDupl = this.collateralList;
                this.unRegistredAddressUnit = JSON.stringify(this.collateralList[0]);
                this.imageGalleryOptions();
                for (let i = 0; i < this.collateralList.length; i++) {
                    this.collateralList[i].checked = false;
                    this.collateralList[i].message = '';
                    this.collateralList[i].collateralRefId = null;
                    // delete this.collateralList[i].addressLine2;
                    if (this.collateralList[i].dateOfInspectionTS != null) {
                        if (+this.collateralList[i].dateOfInspectionTS == 0) {
                            this.collateralList[i].dateOfInspectionTSTemp = new Date();
                        } else {
                            this.collateralList[i].dateOfInspectionTSTemp = new Date(this.collateralList[i].dateOfInspectionTS);
                        }
                    }
                }
            } else {
                this.noData = true;
            }
        } else {
            this.noData = true;
        }
    }

    setCollateralObject(object) {
        object["addressLine2"] = null;
        object["building_no"] = null;
        object["district"] = null;
        object["sub_district"] = null;
        object["village"] = null;
        object["mobile_number"] = null;
        return object;
    }

    imageGalleryOptions() {
        this.galleryOptionsSelfy = [
            {
                "image": false,
                "height": "80px",
                "width": "100px",
                "thumbnailsColumns": 1
            }
        ];

        this.galleryOptions = [
            {
                "image": false,
                "height": "80px",
                "width": "100%",
                "thumbnailsColumns": 6,
                "previewDescription": true
            }
        ];
    }

    tabHeaderClick(i, collateralDetail) {
        this.indexVal = i;
        this.galleryImagesSelfy = [];
        this.galleryImages = [];
        if (collateralDetail.imageList != null) {
            for (let j = 0; j < collateralDetail.imageList.length; j++) {
              let latitudeAndLongitude = "Latitude: " + collateralDetail.imageList[j].imageLatitude + ", " + "Longitude: " + collateralDetail.imageList[j].imageLongitude;
                let imageObj = {
                    small: collateralDetail.imageList[j].imagePath,
                    medium: collateralDetail.imageList[j].imagePath,
                    big: collateralDetail.imageList[j].imagePath,
                    description: latitudeAndLongitude
                }
                if (collateralDetail.imageList[j].imageType == "SELFIE_TYPE") {
                    if (this.galleryImagesSelfy.length < 1) {
                        this.galleryImagesSelfy.push(imageObj);
                    }
                } else {
                    this.galleryImages.push(imageObj);
                }
            }
        }
        for (var k = 0; k < this.collateralList.length; k++) {
            this.collateralList[k].message = '';
        }
    }

    sameAddress(i) {
        if (this.collateralList[this.indexVal].checked) {
            console.log(this.registredAddressUnit);
            this.unRegistredAddressUnit = JSON.stringify(this.collateralList[this.indexVal]);
            if (!(this.collateralListDupl[this.indexVal].building_noStatus == 'LOS' || this.collateralListDupl[this.indexVal].building_noStatus == 'Mob')) {
                this.collateralList[this.indexVal].building_no = this.registredAddressUnit.plotBuildingFlatNum;
            }
            if (!(this.collateralListDupl[this.indexVal].addressLine1Status == 'LOS' || this.collateralListDupl[this.indexVal].addressLine1Status == 'Mob')) {
                this.collateralList[this.indexVal].addressLine1 = this.registredAddressUnit.addressLine1;
            }
            if (!(this.collateralListDupl[this.indexVal].addressLine2Status == 'LOS' || this.collateralListDupl[this.indexVal].addressLine2Status == 'Mob')) {
                this.collateralList[this.indexVal].addressLine2 = this.registredAddressUnit.addressLine2;
            }
            if (!(this.collateralListDupl[this.indexVal].stateStatus == 'LOS' || this.collateralListDupl[this.indexVal].stateStatus == 'Mob')) {
                this.collateralList[this.indexVal].state = this.registredAddressUnit.state;
            }
            if (!(this.collateralListDupl[this.indexVal].cityStatus == 'LOS' || this.collateralListDupl[this.indexVal].cityStatus == 'Mob')) {
                this.collateralList[this.indexVal].city = this.registredAddressUnit.city;
            }
            if (!(this.collateralListDupl[this.indexVal].districtStatus == 'LOS' || this.collateralListDupl[this.indexVal].districtStatus == 'Mob')) {
                this.collateralList[this.indexVal].district = this.registredAddressUnit.district;
            }
            if (!(this.collateralListDupl[this.indexVal].sub_districtStatus == 'LOS' || this.collateralListDupl[this.indexVal].sub_districtStatus == 'Mob')) {
                this.collateralList[this.indexVal].sub_district = this.registredAddressUnit.blockTehsilSubdistrict;
            }
            if (!(this.collateralListDupl[this.indexVal].villageStatus == 'LOS' || this.collateralListDupl[this.indexVal].villageStatus == 'Mob')) {
                this.collateralList[this.indexVal].village = this.registredAddressUnit.village;
            }
            if (!(this.collateralListDupl[this.indexVal].pinCodeStatus == 'LOS' || this.collateralListDupl[this.indexVal].pinCodeStatus == 'Mob')) {
                this.collateralList[this.indexVal].pinCode = this.registredAddressUnit.pinCode;
            }
            if (!(this.collateralListDupl[this.indexVal].mobile_numberStatus == 'LOS' || this.collateralListDupl[this.indexVal].mobile_numberStatus == 'Mob')) {
                this.collateralList[this.indexVal].mobile_number = this.registredAddressUnit.mobileNum;
            }
        } else {
            console.log("Unchecked");
            this.unRegistredAddressUnit = JSON.parse(this.unRegistredAddressUnit);
            this.collateralList[this.indexVal].building_no = this.unRegistredAddressUnit.building_no;
            this.collateralList[this.indexVal].addressLine1 = this.unRegistredAddressUnit.addressLine1;
            this.collateralList[this.indexVal].addressLine2 = this.unRegistredAddressUnit.addressLine2;
            this.collateralList[this.indexVal].state = this.unRegistredAddressUnit.state;
            this.collateralList[this.indexVal].city = this.unRegistredAddressUnit.city;
            this.collateralList[this.indexVal].district = this.unRegistredAddressUnit.district;
            this.collateralList[this.indexVal].sub_district = this.unRegistredAddressUnit.sub_district;
            this.collateralList[this.indexVal].village = this.unRegistredAddressUnit.village;
            this.collateralList[this.indexVal].pinCode = this.unRegistredAddressUnit.pinCode;
            this.collateralList[this.indexVal].mobile_number = this.registredAddressUnit.mobile_number;
        }
    }

    keypressCheck(data, field) {
        this.cityVal = false;
        this.districtVal = false;
        this.blockVal = false;
        this.villageVal = false;
        this.pincodeVal = false;
        this.mobileVal = false;
        switch (field) {
            case "City":
                if (this.utilsService.validString) {
                    this.cityVal = true;
                } else {
                    this.cityVal = false;
                }
                break;

            case "District":
                if (this.utilsService.validString) {
                    this.districtVal = true;
                } else {
                    this.districtVal = false;
                }
                break;

            case "Block":
                if (this.utilsService.validString) {
                    this.blockVal = true;
                } else {
                    this.blockVal = false;
                }
                break;

            case "Village":
                if (this.utilsService.validString) {
                    this.villageVal = true;
                } else {
                    this.villageVal = false;
                }
                break;

            case "Pincode":
                if (this.utilsService.validString) {
                    this.pincodeVal = true;
                } else {
                    this.pincodeVal = false;
                }
                break;

            case "Mobile":
                if (this.utilsService.validString) {
                    this.mobileVal = true;
                } else {
                    this.mobileVal = false;
                }
                break;

        }
        return data;
    }

    onBlurValidation(value, field) {
        if (value) {
            this.cityValBlur = false;
            this.districtValBlur = false;
            this.blockValBlur = false;
            this.villageValBlur = false;
            this.pincodeValBlur = false;
            this.mobileValBlur = false;
            this.onlyNumber = "Only numbers are allowed";
            this.onlyAlphabet = "Please enter only alphabets";
            this.avoidSpecialChar = "Please enter alphabets & numbers";
            switch (field) {
                case "City":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.collateralList[this.indexVal].city = '';
                        this.cityValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "District":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.collateralList[this.indexVal].district = '';
                        this.districtValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Block":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.collateralList[this.indexVal].sub_district = '';
                        this.blockValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Village":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.collateralList[this.indexVal].village = '';
                        this.villageValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Pincode":
                    if (!(value.match(/^\d+$/))) {
                        this.collateralList[this.indexVal].pinCode = '';
                        this.pincodeValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Mobile":
                    if (!(value.match(/^\d+$/))) {
                        this.collateralList[this.indexVal].mobile_number = '';
                        this.mobileValBlur = true;
                        this.setTimeOut();
                    }
                    break;
            }
        }
    }
}