import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DIAConstants } from '../../../../Shared/constants';
import { Constants } from '../../../../Shared/Constants/constants.service';

import { GuarantorDetails } from '../../../../Service/SME/Presanction/GuarantorDetails/guarantorDetails';
import { AddGuarantorDetailsService } from '../../../../Service/SME/Presanction/GuarantorDetails/AddGuarantorDetails/addGuarantorDetails.service';
import { SmePresanctionGetGuarantorDetailsService } from '../../../../Service/SME/Presanction/GuarantorDetails/GetGuarantorDetails/getGuarantorDetails.service';
import { UtilsService } from '../../../../Shared/UtilsService/utils.service';

@Component({
  selector: 'guarantorDetail',
  templateUrl: './guarantorDetail.component.html',
  styleUrls: ['./guarantorDetail.component.scss', '../../../module.component.scss'],
  providers: [
    GuarantorDetails,
    AddGuarantorDetailsService,
    SmePresanctionGetGuarantorDetailsService,
    UtilsService,
    Constants
  ]
})

export class GuarantorDetailComponent implements OnInit {
  private apiRequest: any;
  private states: any = [];
  private salutation: any = [];
  private responseStatus: Object = [];
  private status: boolean;
  private message: string;
  private statusMsg: string;
  private guarantorList: any = [];
  private executivePFIndex: any;
  private borrowerRefId: string;
  private checked: boolean;
  private noData: boolean;
  private indexVal: any;
  private registredAddressUnit: any = {};
  private unRegistredAddressUnit: any = {};
  private pfId: string;
  private isEmailValid: boolean = true;
  private isLoanEditable: boolean;
  private isPinCodeValid: boolean = true;
  private isMobileNumberValid: boolean = true;
  private loanDetailsWithOutInspection: any;
  private firstNameVal: boolean = false;
  private middleNameVal: boolean = false;
  private lastNameVal: boolean = false;
  private firstName2Val: boolean = false;
  private middleName2Val: boolean = false;
  private lastName2Val: boolean = false;
  private relationshipVal: boolean = false;
  private cityVal: boolean = false;
  private districtVal: boolean = false;
  private blockVal: boolean = false;
  private villageVal: boolean = false;
  private pincodeVal: boolean = false;
  private mobileVal: boolean = false;
  private landlineVal: boolean = false;
  private guarantorListDupl = null;
  private firstNameValBlur: boolean = false;
  private middleNameValBlur: boolean = false;
  private lastNameValBlur: boolean = false;
  private firstName2ValBlur: boolean = false;
  private middleName2ValBlur: boolean = false;
  private lastName2ValBlur: boolean = false;
  private relationshipValBlur: boolean = false;
  private cityValBlur: boolean = false;
  private districtValBlur: boolean = false;
  private blockValBlur: boolean = false;
  private villageValBlur: boolean = false;
  private pincodeValBlur: boolean = false;
  private mobileValBlur: boolean = false;
  private landlineValBlur: boolean = false;
  private onlyNumber: string;
  private onlyAlphabet: string;
  private avoidSpecialChar: string;

  constructor(private _addGuarantorDetailsService: AddGuarantorDetailsService, private service: SmePresanctionGetGuarantorDetailsService, private route: ActivatedRoute, private utilsService: UtilsService, private constants: Constants) {
    this.states = DIAConstants.states;
    this.salutation = DIAConstants.salutation;
  }

  @Input() guarantorDetails: GuarantorDetails;

  addGuarantorDetail() {
    if (this.borrowerRefId && !this.guarantorList[this.indexVal].borrowerRefId) {
      this.guarantorList[this.indexVal].borrowerRefId = this.borrowerRefId;
    }
    if (this.guarantorList[this.indexVal].borrowerRefId) {
      //  if (this.guarantorList[this.indexVal].salution && this.guarantorList[this.indexVal].firstName && this.guarantorList[this.indexVal].lastName && this.guarantorList[this.indexVal].realtiveSalution && this.guarantorList[this.indexVal].relativeFirstName && this.guarantorList[this.indexVal].relativeLastName && this.guarantorList[this.indexVal].plotBuildingFlatNum && this.guarantorList[this.indexVal].address1 && this.guarantorList[this.indexVal].address2 && this.guarantorList[this.indexVal].state && this.guarantorList[this.indexVal].city && this.guarantorList[this.indexVal].mobileNum && this.guarantorList[this.indexVal].pinCode) {
      if (this.guarantorList[this.indexVal].email) {
        this.isEmailValid = this.utilsService.emailValidator(this.guarantorList[this.indexVal].email);
      } else {
        this.isEmailValid = true;
      }
      if (this.guarantorList[this.indexVal].pinCode) {
        if (this.guarantorList[this.indexVal].pinCode.length < 6) {
          this.isPinCodeValid = false;
        } else {
          this.isPinCodeValid = true;
        }
      } else {
        this.isPinCodeValid = true;
      }
      if (this.guarantorList[this.indexVal].mobileNum) {
        if (this.guarantorList[this.indexVal].mobileNum.length < 10) {
          this.isMobileNumberValid = false;
        } else {
          this.isMobileNumberValid = true;
        }
      } else {
        this.isMobileNumberValid = true;
      }

      if (this.isMobileNumberValid && this.isPinCodeValid && this.isEmailValid) {
        this.checked = this.guarantorList[this.indexVal].checked;
        delete this.guarantorList[this.indexVal].checked;
        delete this.guarantorList[this.indexVal].message;
        //  this.deleteBoolValues();
        this.guarantorList[this.indexVal] = this.utilsService.dataSourceSave(this.guarantorList[this.indexVal]);

        var objectToPost: { gurantorVO: object; appId: string; executivePFIndex: string; } = { gurantorVO: this.guarantorList[this.indexVal], appId: "WEB-DIA", executivePFIndex: this.pfId };
        console.log(JSON.stringify(objectToPost));
        this._addGuarantorDetailsService.postComment(objectToPost).subscribe(
          data => {
            console.log(this.responseStatus = data);
            if (data.responseMessage == "Successful") {
              this.guarantorList[this.indexVal].guarantorRefId = data.refId;
              console.log(data);
              this.guarantorList[this.indexVal].message = this.constants.getMessage('successMsg');
              this.statusMsg = "success";
              //   this.disableCurrentIndex();
            } else {
              this.guarantorList[this.indexVal].message = data.responseMessage;
              this.statusMsg = "error";
            }
            this.setTimeOut();
          },
          err => {
            console.log(err);
            this.guarantorList[this.indexVal].message = this.constants.getMessage('errorMsg');
            this.statusMsg = "error";
            this.setTimeOut();
          },
          () => console.log('Request Completed')
        );
        this.guarantorList[this.indexVal].checked = this.checked;
        this.status = true;
      } else {
        if (!this.isEmailValid) {
          this.guarantorList[this.indexVal].message = this.constants.getMessage('emailVal');
          this.statusMsg = "error";
        } else if (!this.isPinCodeValid) {
          this.guarantorList[this.indexVal].message = this.constants.getMessage('pinCodeVal');
          this.statusMsg = "error";
        } else if (!this.isMobileNumberValid) {
          this.guarantorList[this.indexVal].message = this.constants.getMessage('mobileNoVal');
          this.statusMsg = "error";
        }
        this.setTimeOut();
      }
      // } else {
      //     this.guarantorList[this.indexVal].message =  this.constants.getMessage('requiredFieldVal');
      //     this.statusMsg = "error";
      //     this.setTimeOut();
      // }
    } else {
      this.guarantorList[this.indexVal].message = this.constants.getMessage('unitDetailVal');
      this.statusMsg = "error";
      this.setTimeOut();
    }
  }

  setTimeOut() {
    setTimeout(() => {
      this.guarantorList[this.indexVal].message = "";
      this.onlyNumber = "";
      this.onlyAlphabet = "";
      this.avoidSpecialChar = "";
    }, 3000);
  }

  sameAddress(i) {
    if (this.guarantorList[this.indexVal].checked) {
      console.log(this.registredAddressUnit);
      this.unRegistredAddressUnit = JSON.stringify(this.guarantorList[this.indexVal]);
      if (!(this.guarantorListDupl[this.indexVal].plotBuildingFlatNumStatus == 'LOS' || this.guarantorListDupl[this.indexVal].plotBuildingFlatNumStatus == 'Mob')) {
        this.guarantorList[this.indexVal].plotBuildingFlatNum = this.registredAddressUnit.plotBuildingFlatNum;
      }
      if (!(this.guarantorListDupl[this.indexVal].address1Status == 'LOS' || this.guarantorListDupl[this.indexVal].address1Status == 'Mob')) {
        this.guarantorList[this.indexVal].address1 = this.registredAddressUnit.addressLine1;
      }
      if (!(this.guarantorListDupl[this.indexVal].address2Status == 'LOS' || this.guarantorListDupl[this.indexVal].address2Status == 'Mob')) {
        this.guarantorList[this.indexVal].address2 = this.registredAddressUnit.addressLine2;
      }
      if (!(this.guarantorListDupl[this.indexVal].stateStatus == 'LOS' || this.guarantorListDupl[this.indexVal].stateStatus == 'Mob')) {
        this.guarantorList[this.indexVal].state = this.registredAddressUnit.state;
      }
      if (!(this.guarantorListDupl[this.indexVal].cityStatus == 'LOS' || this.guarantorListDupl[this.indexVal].cityStatus == 'Mob')) {
        this.guarantorList[this.indexVal].city = this.registredAddressUnit.city;
      }
      if (!(this.guarantorListDupl[this.indexVal].districtStatus == 'LOS' || this.guarantorListDupl[this.indexVal].districtStatus == 'Mob')) {
        this.guarantorList[this.indexVal].district = this.registredAddressUnit.district;
      }
      if (!(this.guarantorListDupl[this.indexVal].blockTesilSubdistrictStatus == 'LOS' || this.guarantorListDupl[this.indexVal].blockTesilSubdistrictStatus == 'Mob')) {
        this.guarantorList[this.indexVal].blockTesilSubdistrict = this.registredAddressUnit.blockTehsilSubdistrict;
      }
      if (!(this.guarantorListDupl[this.indexVal].villageStatus == 'LOS' || this.guarantorListDupl[this.indexVal].villageStatus == 'Mob')) {
        this.guarantorList[this.indexVal].village = this.registredAddressUnit.village;
      }
      if (!(this.guarantorListDupl[this.indexVal].pinCodeStatus == 'LOS' || this.guarantorListDupl[this.indexVal].pinCodeStatus == 'Mob')) {
        this.guarantorList[this.indexVal].pinCode = this.registredAddressUnit.pinCode;
      }
      if (!(this.guarantorListDupl[this.indexVal].mobileNumStatus == 'LOS' || this.guarantorListDupl[this.indexVal].mobileNumStatus == 'Mob')) {
        this.guarantorList[this.indexVal].mobileNum = this.registredAddressUnit.mobileNum;
      }
      if (!(this.guarantorListDupl[this.indexVal].landNumStatus == 'LOS' || this.guarantorListDupl[this.indexVal].landNumStatus == 'Mob')) {
        this.guarantorList[this.indexVal].landNum = this.registredAddressUnit.landlineNum;
      }
      if (!(this.guarantorListDupl[this.indexVal].emailStatus == 'LOS' || this.guarantorListDupl[this.indexVal].emailStatus == 'Mob')) {
        this.guarantorList[this.indexVal].email = this.registredAddressUnit.emailId;
      }
    } else {
      console.log("Unchecked");
      this.unRegistredAddressUnit = JSON.parse(this.unRegistredAddressUnit);
      this.guarantorList[this.indexVal].plotBuildingFlatNum = this.unRegistredAddressUnit.plotBuildingFlatNum;
      this.guarantorList[this.indexVal].address1 = this.unRegistredAddressUnit.address1;
      this.guarantorList[this.indexVal].address2 = this.unRegistredAddressUnit.address2;
      this.guarantorList[this.indexVal].state = this.unRegistredAddressUnit.state;
      this.guarantorList[this.indexVal].city = this.unRegistredAddressUnit.city;
      this.guarantorList[this.indexVal].district = this.unRegistredAddressUnit.district;
      this.guarantorList[this.indexVal].blockTesilSubdistrict = this.unRegistredAddressUnit.blockTesilSubdistrict;
      this.guarantorList[this.indexVal].village = this.unRegistredAddressUnit.village;
      this.guarantorList[this.indexVal].pinCode = this.unRegistredAddressUnit.pinCode;
      this.guarantorList[this.indexVal].mobileNum = this.unRegistredAddressUnit.mobileNum;
      this.guarantorList[this.indexVal].landNum = this.unRegistredAddressUnit.landNum;
      this.guarantorList[this.indexVal].email = this.unRegistredAddressUnit.email;
    }
  }
  ngOnInit() {
    this.guarantorDetails = new GuarantorDetails();
    window.scrollTo(0, 0);
    if (localStorage.getItem("isLoanEditable") != null && localStorage.getItem("isLoanEditable") != "null") {
      if (localStorage.getItem("isLoanEditable") == "true") {
        this.isLoanEditable = true;
      } else {
        this.isLoanEditable = false;
      }
    }
    // if (localStorage.getItem("borrowerRefID") == "null" || localStorage.getItem("borrowerRefID") == null) {
    //     this.noData = true;
    //     this.message = this.constants.getMessage('unitDetailVal');
    // }
    if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null && localStorage.getItem("borrowerRefID") != "null" && localStorage.getItem("borrowerRefID") != null) {
      this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
      this.pfId = this.executivePFIndex.user.pfId.toString();
      this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
      this.registredAddressUnit = JSON.parse(localStorage.getItem("registredAddressUnit"));
      this.getGuarantorDetails();
    } else {
      console.log("No data");
      this.getDataFromLocalStorage();
    }
  }

  getGuarantorDetails() {
    this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };
    this.service.getGuarantorDetails(this.apiRequest).subscribe(response => {
      if (response.guarantorList) {
        this.guarantorList = response.guarantorList;
        if (this.guarantorList.length > 0) {
          for (let k = 0; k < this.guarantorList.length; k++) {
            if (this.guarantorList[k].guarantorRefId == null) {
              this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
              if (this.loanDetailsWithOutInspection) {
                if (this.loanDetailsWithOutInspection.gurantorList && this.loanDetailsWithOutInspection.gurantorList.length > 0) {
                  if (this.loanDetailsWithOutInspection.gurantorList.length > k) {
                    this.guarantorList[k] = this.loanDetailsWithOutInspection.gurantorList[k];
                    this.guarantorList[k].guarantorRefId = null;
                    this.guarantorList[k] = this.utilsService.dataSourceCheckFromLOS(this.guarantorList[k]);
                  } else {
                    this.guarantorList[k] = this.utilsService.dataSourceCheck(this.guarantorList[k]);
                  }
                }
              } else {
                this.guarantorList[k] = this.utilsService.dataSourceCheck(this.guarantorList[k]);
              }
            } else {
              this.guarantorList[k] = this.utilsService.dataSourceCheck(this.guarantorList[k]);
            }
          }


          this.unRegistredAddressUnit = JSON.stringify(this.guarantorList[0]);
          for (let k = 0; k < this.guarantorList.length; k++) {
            this.guarantorList[k].checked = false;
            this.guarantorList[k].message = '';
          }
          this.guarantorListDupl = this.guarantorList;
          //  this.disabledFeature();
        } else {
          console.log("No data");
          this.getDataFromLocalStorage();
        }
      } else {
        this.getDataFromLocalStorage();
      }
    })
  }

  getDataFromLocalStorage() {
    this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
    if (this.loanDetailsWithOutInspection) {
      if (this.loanDetailsWithOutInspection.gurantorList && this.loanDetailsWithOutInspection.gurantorList.length > 0) {
        this.guarantorList = this.loanDetailsWithOutInspection.gurantorList;
        this.guarantorList = this.utilsService.dataSourceCheckFromLOSArray(this.guarantorList);
        this.guarantorListDupl = this.guarantorList;
        this.unRegistredAddressUnit = JSON.stringify(this.guarantorList[0]);
        for (let k = 0; k < this.guarantorList.length; k++) {
          this.guarantorList[k].checked = false;
          this.guarantorList[k].message = '';
          this.guarantorList[k].guarantorRefId = null;
        }
        // this.disabledFeature();
      } else {
        this.noData = true;
        this.message = this.constants.getMessage('unitDetailVal');
      }
    } else {
      this.noData = true;
    }
  }

  tabHeaderClick(i) {
    this.indexVal = i;
    for (var k = 0; k < this.guarantorList.length; k++) {
      this.guarantorList[k].message = '';
    }
  }

  keypressCheck(data, field) {
    this.firstNameVal = false;
    this.middleNameVal = false;
    this.lastNameVal = false;
    this.relationshipVal = false;
    this.firstName2Val = false;
    this.middleName2Val = false;
    this.lastName2Val = false;
    this.cityVal = false;
    this.districtVal = false;
    this.blockVal = false;
    this.villageVal = false;
    this.pincodeVal = false;
    this.mobileVal = false;
    this.landlineVal = false;
    switch (field) {
      case "First Name":
        if (this.utilsService.validString) {
          this.firstNameVal = true;
        } else {
          this.firstNameVal = false;
        }
        break;

      case "Middle Name":
        if (this.utilsService.validString) {
          this.middleNameVal = true;
        } else {
          this.middleNameVal = false;
        }
        break;

      case "Last Name":
        if (this.utilsService.validString) {
          this.lastNameVal = true;
        } else {
          this.lastNameVal = false;
        }
        break;

      case "Relationship":
        if (this.utilsService.validString) {
          this.relationshipVal = true;
        } else {
          this.relationshipVal = false;
        }
        break;

      case "First Name 2":
        if (this.utilsService.validString) {
          this.firstName2Val = true;
        } else {
          this.firstName2Val = false;
        }
        break;

      case "Middle Name 2":
        if (this.utilsService.validString) {
          this.middleName2Val = true;
        } else {
          this.middleName2Val = false;
        }
        break;

      case "Last Name 2":
        if (this.utilsService.validString) {
          this.lastName2Val = true;
        } else {
          this.lastName2Val = false;
        }
        break;

      case "City":
        if (this.utilsService.validString) {
          this.cityVal = true;
        } else {
          this.cityVal = false;
        }
        break;

      case "District":
        if (this.utilsService.validString) {
          this.districtVal = true;
        } else {
          this.districtVal = false;
        }
        break;

      case "Block":
        if (this.utilsService.validString) {
          this.blockVal = true;
        } else {
          this.blockVal = false;
        }
        break;

      case "Village":
        if (this.utilsService.validString) {
          this.villageVal = true;
        } else {
          this.villageVal = false;
        }
        break;

      case "Pincode":
        if (this.utilsService.validString) {
          this.pincodeVal = true;
        } else {
          this.pincodeVal = false;
        }
        break;

      case "Mobile":
        if (this.utilsService.validString) {
          this.mobileVal = true;
        } else {
          this.mobileVal = false;
        }
        break;

      case "Landline":
        if (this.utilsService.validString) {
          this.landlineVal = true;
        } else {
          this.landlineVal = false;
        }
        break;

    }
    return data;
  }

  onBlurValidation(value, field) {
    if (value) {
      this.firstNameValBlur = false;
      this.middleNameValBlur = false;
      this.lastNameValBlur = false;
      this.relationshipValBlur = false;
      this.firstName2ValBlur = false;
      this.middleName2ValBlur = false;
      this.lastName2ValBlur = false;
      this.cityValBlur = false;
      this.districtValBlur = false;
      this.blockValBlur = false;
      this.villageValBlur = false;
      this.pincodeValBlur = false;
      this.mobileValBlur = false;
      this.landlineValBlur = false;
      this.onlyNumber = "Only numbers are allowed";
      this.onlyAlphabet = "Please enter only alphabets";
      this.avoidSpecialChar = "Please enter alphabets & numbers";
      switch (field) {
        case "First Name":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].firstName = '';
            this.firstNameValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Middle Name":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].middleName = '';
            this.middleNameValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Last Name":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].lastName = '';
            this.lastNameValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Relationship":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].realtiveSalutation = '';
            this.relationshipValBlur = true;
            this.setTimeOut();
          }
          break;

        case "First Name 2":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].relativeFirstName = '';
            this.firstName2ValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Middle Name 2":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].relativeMiddleName = '';
            this.middleName2ValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Last Name 2":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].relativeLastName = '';
            this.lastName2ValBlur = true;
            this.setTimeOut();
          }
          break;

        case "City":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].city = '';
            this.cityValBlur = true;
            this.setTimeOut();
          }
          break;

        case "District":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].district = '';
            this.districtValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Block":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].blockTesilSubdistrict = '';
            this.blockValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Village":
          if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
            this.guarantorList[this.indexVal].village = '';
            this.villageValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Pincode":
          if (!(value.match(/^\d+$/))) {
            this.guarantorList[this.indexVal].pinCode = '';
            this.pincodeValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Mobile":
          if (!(value.match(/^\d+$/))) {
            this.guarantorList[this.indexVal].mobileNum = '';
            this.mobileValBlur = true;
            this.setTimeOut();
          }
          break;

        case "Landline":
          if (!(value.match(/^\d+$/))) {
            this.guarantorList[this.indexVal].landNum = '';
            this.landlineValBlur = true;
            this.setTimeOut();
          }
          break;
      }
    }
  }
}