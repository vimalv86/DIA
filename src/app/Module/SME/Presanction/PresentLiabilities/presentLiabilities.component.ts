import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DIAConstants } from '../../../../Shared/constants';
import { Constants } from '../../../../Shared/Constants/constants.service';
import { UtilsService } from '../../../../Shared/UtilsService/utils.service';

import { LiabilityDetails } from '../../../../Service/SME/Presanction/PresentLiabilities/liabilityDetails';
import { AddLiabilityDetailsService } from '../../../../Service/SME/Presanction/PresentLiabilities/AddLiabilityDetails/addLiabilityDetails.service';
import { RemoveLiabilityDetailsService } from '../../../../Service/SME/Presanction/PresentLiabilities/RemoveLiabilityDetails/removeLiabilityDetails.service';
import { SmePresanctionGetLiabilityDetailsService } from '../../../../Service/SME/Presanction/PresentLiabilities/GetLiabilityDetails/getLiabilityDetails.service';

import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'presentLiabilities',
    templateUrl: './presentLiabilities.component.html',
    styleUrls: ['./presentLiabilities.component.scss', '../../../module.component.scss'],
    providers: [
        LiabilityDetails,
        AddLiabilityDetailsService,
        SmePresanctionGetLiabilityDetailsService,
        ConfirmationService,
        RemoveLiabilityDetailsService,
        UtilsService,
        Constants
    ]
})

export class PresentLiabilitiesComponent implements OnInit {
    private apiRequest: any;
    private responseStatus: Object = [];
    private liabilityList: any;
    private executivePFIndex: any;
    private borrowerRefId: string;
    private status: boolean;
    private message: string;
    private statusMsg: string;
    private noData: boolean;
    private facilityType: any = [];
    private sanctionDate: any;
    private pfId: string;
    private isDisabled: boolean;
    private isOtherLoansDisabled: boolean;
    private isValidationComplete: boolean;
    private isLoanEditable: boolean;
    private loanDetailsWithOutInspection: any;
    private otherVal: boolean = false;
    private accountVal: boolean = false;
    private limitedVal: boolean = false;
    private marketVal: boolean = false;
    private advanceVal: boolean = false;
    private drawingVal: boolean = false;
    private outstandingVal: boolean = false;
    private otherValBlur: boolean = false;
    private accountValBlur: boolean = false;
    private limitedValBlur: boolean = false;
    private marketValBlur: boolean = false;
    private advanceValBlur: boolean = false;
    private drawingValBlur: boolean = false;
    private outstandingValBlur: boolean = false;
    private onlyNumber: string;
    private onlyAlphabet: string;
    private avoidSpecialChar: string;

    constructor(private _addLiabilityDetailsService: AddLiabilityDetailsService, private service: SmePresanctionGetLiabilityDetailsService, private route: ActivatedRoute, private confirmationService: ConfirmationService, private _removeLiabilityDetailsService: RemoveLiabilityDetailsService, private utilsService: UtilsService, private constants: Constants) {
        this.facilityType = DIAConstants.facilityType;
        this.sanctionDate = '';
    }

    @Input() liabilityDetails: LiabilityDetails;

    addLiabilityDetail() {
        if (this.borrowerRefId) {
            this.isValidationComplete = true;
            if (this.liabilityDetails.facilityType && this.liabilityDetails.accountNumber && this.liabilityDetails.limitSanction && this.liabilityDetails.marketValue && this.sanctionDate && this.liabilityDetails.liabilityExist && this.liabilityDetails.drawingPower && this.liabilityDetails.advancedValue) {
                if (this.liabilityDetails.facilityType == "Others") {
                    if (!this.liabilityDetails.others) {
                        this.isValidationComplete = false;
                    }
                }
                if (this.isValidationComplete) {
                    let actualTime = this.sanctionDate;
                    let timeStamp = new Date(actualTime).getTime().toString();
                    this.liabilityDetails["dateOfSanctionTS"] = timeStamp;

                    //console.log(this.borrowerRefId);
                    this.liabilityDetails["borrowerRefId"] = this.borrowerRefId;

                    var objectToPost: { borrowerLiabilityVO: object; appId: string; executivePFIndex: string; } = { borrowerLiabilityVO: this.liabilityDetails, appId: "WEB-DIA", executivePFIndex: this.pfId };

                    console.log(JSON.stringify(objectToPost));
                    this._addLiabilityDetailsService.postComment(objectToPost).subscribe(
                        data => {
                            console.log(this.responseStatus = data);
                            if (data.responseMessage == "Successful") {
                                this.message = this.constants.getMessage('successMsg');
                                this.statusMsg = "success";
                                this.getLiability();
                            } else {
                                this.message = data.responseMessage;
                                this.statusMsg = "error";
                            }
                        },
                        err => {
                            console.log(err);
                            this.message = this.constants.getMessage('errorMsg');
                            this.statusMsg = "error";
                        },
                        () => console.log('Request Completed')
                    );
                    this.status = true;
                    this.setTimeOut();
                } else {
                    this.message = this.constants.getMessage('requiredFieldVal');
                    this.statusMsg = "error";
                    this.setTimeOut();
                }
            } else {
                this.message = this.constants.getMessage('requiredFieldVal');
                this.statusMsg = "error";
                this.setTimeOut();
            }
        } else {
            this.message = this.constants.getMessage('unitDetailVal');
            this.statusMsg = "error";
            this.setTimeOut();
        }
    }

    setTimeOut() {
        setTimeout(() => {
            this.message = "";
            this.onlyNumber = "";
            this.onlyAlphabet = "";
            this.avoidSpecialChar = "";
        }, 3000);
    }

    ngOnInit() {
        this.liabilityDetails = new LiabilityDetails();
        this.liabilityDetails.liabilityExist = "0";
        this.isDisabled = true;
        if (localStorage.getItem("isLoanEditable") != null && localStorage.getItem("isLoanEditable") != "null") {
            if (localStorage.getItem("isLoanEditable") == "true") {
                this.isLoanEditable = true;
            } else {
                this.isLoanEditable = false;
            }
        }
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null && localStorage.getItem("borrowerRefID") != "null" && localStorage.getItem("borrowerRefID") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
            this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
            this.getLiability();
        } else {
            this.getDataFromLocalStorage();
        }
    }

    getLiability() {
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.borrowerRefId };
        this.service.getLiabilityDetails(this.apiRequest).subscribe(response => {
            if (response.borrowerLiabilityList) {
                //this.registredAddressUnit = JSON.parse(localStorage.getItem("registredAddressUnit"));
                this.liabilityList = response.borrowerLiabilityList;
                console.log(this.liabilityList);
                if (this.liabilityList.length == 0) {
                    this.liabilityDetails.liabilityExist = "0";
                } else {
                    this.liabilityDetails.liabilityExist = "1";
                    this.isDisabled = false;
                }
                this.liabilityDetails.facilityType = "";
                this.liabilityDetails.others = "";
                this.liabilityDetails.accountNumber = "";
                this.liabilityDetails.limitSanction = "";
                this.liabilityDetails.dateOfSanctionTS = "";
                this.liabilityDetails.drawingPower = "";
                this.liabilityDetails.advancedValue = "";
                this.liabilityDetails.marketValue = "";
                this.liabilityDetails.outstandingAmount = "";
            } else {
                this.getDataFromLocalStorage();
            }
        })
    }

    getDataFromLocalStorage() {
        this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
        if (this.loanDetailsWithOutInspection) {
            if (this.loanDetailsWithOutInspection.libilityList && this.loanDetailsWithOutInspection.libilityList.length > 0) {
                this.liabilityList = this.loanDetailsWithOutInspection.libilityList;
                console.log(JSON.stringify(this.liabilityList));
                if (this.liabilityList.length == 0) {
                    this.liabilityDetails.liabilityExist = "0";
                } else {
                    this.liabilityDetails.liabilityExist = "1";
                    this.isDisabled = false;
                }
                this.liabilityDetails.facilityType = "";
                this.liabilityDetails.others = "";
                this.liabilityDetails.accountNumber = "";
                this.liabilityDetails.limitSanction = "";
                this.liabilityDetails.dateOfSanctionTS = "";
                this.liabilityDetails.drawingPower = "";
                this.liabilityDetails.advancedValue = "";
                this.liabilityDetails.marketValue = "";
                this.liabilityDetails.outstandingAmount = "";
            } else {
                this.noData = true;
            }
        } else {
            this.noData = true;
        }
    }

    liabilitySelect(data) {
        if (data == "1") {
            this.isDisabled = false;
        } else {
            this.isDisabled = true;
            if (this.liabilityList && this.liabilityList.length > 0) {
                this.confirmationService.confirm({
                    message: this.constants.getMessage('delPopUpMsg'),
                    accept: () => {
                        this.clearLiabilities();
                    }
                });
            }
        }
    }

    clearLiabilities() {
        let liabilityRefIdList = [];
        for (let k = 0; k < this.liabilityList.length; k++) {
            liabilityRefIdList.push(this.liabilityList[k].liabilityRefId);
        }
        var objectToPost: { borrowerLiabilityRefIdList: object; appId: string; executivePFIndex: string; } = { borrowerLiabilityRefIdList: liabilityRefIdList, appId: "WEB-DIA", executivePFIndex: this.pfId };

        console.log(JSON.stringify(objectToPost));
        this._removeLiabilityDetailsService.postComment(objectToPost).subscribe(
            data => {
                console.log(this.responseStatus = data);
                if (data.responseMessage == "Successful") {
                    this.message = this.constants.getMessage('liabilityDelSuccMsg');
                    this.statusMsg = "success";
                    this.getLiability();
                } else {
                    this.message = data.responseMessage;
                    this.statusMsg = "error";
                }
            },
            err => {
                console.log(err);
                this.message = this.constants.getMessage('liabilityDelFailMsg');
                this.statusMsg = "error";
            },
            () => console.log('Request Completed')
        );
        this.status = true;
        this.setTimeOut();
    }

    loanPurposeSelection(data) {
        if (data == "Others") {
            this.isOtherLoansDisabled = true;
        } else {
            this.isOtherLoansDisabled = false;
            this.liabilityDetails.others = null;
        }
    }

    keypressCheck(data, field) {
        this.otherVal = false;
        this.accountVal = false;
        this.limitedVal = false;
        this.marketVal = false;
        this.advanceVal = false;
        this.drawingVal = false;
        this.outstandingVal = false;
        switch (field) {
            case "Other":
                if (this.utilsService.validString) {
                    this.otherVal = true;
                } else {
                    this.otherVal = false;
                }
                break;

            case "Account":
                if (this.utilsService.validString) {
                    this.accountVal = true;
                } else {
                    this.accountVal = false;
                }
                break;

            case "Limited":
                if (this.utilsService.validString) {
                    this.limitedVal = true;
                } else {
                    this.limitedVal = false;
                }
                break;

            case "Market":
                if (this.utilsService.validString) {
                    this.marketVal = true;
                } else {
                    this.marketVal = false;
                }
                break;

            case "Advance":
                if (this.utilsService.validString) {
                    this.advanceVal = true;
                } else {
                    this.advanceVal = false;
                }
                break;

            case "Drawing":
                if (this.utilsService.validString) {
                    this.drawingVal = true;
                } else {
                    this.drawingVal = false;
                }
                break;

            case "Outstanding":
                if (this.utilsService.validString) {
                    this.outstandingVal = true;
                } else {
                    this.outstandingVal = false;
                }
                break;
        }
        return data;
    }

    onBlurValidation(value, field) {
        if (value) {
            this.otherValBlur = false;
            this.accountValBlur = false;
            this.limitedValBlur = false;
            this.marketValBlur = false;
            this.advanceValBlur = false;
            this.drawingValBlur = false;
            this.outstandingValBlur = false;
            this.onlyNumber = "Only numbers are allowed";
            this.onlyAlphabet = "Please enter only alphabets";
            this.avoidSpecialChar = "Please enter alphabets & numbers";
            switch (field) {
                case "Other":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.liabilityDetails.others = '';
                        this.otherValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Limited":
                    if (!(value.match(/^\d+$/))) {
                        this.liabilityDetails.limitSanction = '';
                        this.limitedValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Account":
                    if (!(value.match(/^\d+$/))) {
                        this.liabilityDetails.accountNumber = '';
                        this.accountValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Market":
                    if (!(value.match(/^\d+$/))) {
                        this.liabilityDetails.marketValue = '';
                        this.marketValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Advance":
                    if (!(value.match(/^\d+$/))) {
                        this.liabilityDetails.advancedValue = '';
                        this.advanceValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Drawing":
                    if (!(value.match(/^\d+$/))) {
                        this.liabilityDetails.drawingPower = '';
                        this.drawingValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Outstanding":
                    if (!(value.match(/^\d+$/))) {
                        this.liabilityDetails.outstandingAmount = '';
                        this.outstandingValBlur = true;
                        this.setTimeOut();
                    }
                    break;
            }
        }
    }
}