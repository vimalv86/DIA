import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { DIAConstants } from '../../../../Shared/constants';
import { Constants } from '../../../../Shared/Constants/constants.service';

import { Comments } from '../../../../Service/SME/Presanction/Comments/comments';
import { AddCommentsService } from '../../../../Service/SME/Presanction/Comments/AddComments/addComments.service';
import { SmePresanctionGetCommentsService } from '../../../../Service/SME/Presanction/Comments/GetComments/getComments.service';
import { CheckSubmissionService } from '../../../../Service/SME/Presanction/Comments/checkSubmission/checkSubmission.service';
import { SubmitSMEPresanctionService } from '../../../../Service/SME/Presanction/Comments/submitSMEPresanction/submitSMEPresanction.service';
import { SmePresanctionGetReviewersService } from '../../../../Service/SME/Presanction/Comments/GetReviewers/getReviewers.service';
import { UtilsService } from '../../../../Shared/UtilsService/utils.service';

import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'comments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.scss', '../../../module.component.scss'],
    providers: [
        Comments,
        AddCommentsService,
        SmePresanctionGetCommentsService,
        CheckSubmissionService,
        SubmitSMEPresanctionService,
        SmePresanctionGetReviewersService,
        ConfirmationService,
        UtilsService,
        Constants
    ]
})

export class CommentsComponent implements OnInit {
    private apiRequest: any;
    private responseStatus: Object = [];
    private comments: any;
    private borrowerRefId: string;
    private inspectionID: string;
    private status: boolean;
    private message: string;
    private statusMsg: string;
    private executivePFIndex: any;
    private applicationSource: any[];
    private pfIdOfReviewer: any = [];
    private noData: boolean;
    private pfId: string;
    private isSubmitValidationComplete: boolean;
    private isDisabled: boolean;
    private isValidationComplete: boolean;
    private isLoanEditable: boolean;
    private submissionStatusMessage: string;
    private reviewerList: any = [];
    private loanDetailsWithOutInspection: any;
    private otherVal: boolean = false;
    private positionVal: boolean = false;
    private nameVal: boolean = false;
    private otherValBlur: boolean = false;
    private positionValBlur: boolean = false;
    private nameValBlur: boolean = false;
    private onlyNumber: string;
    private onlyAlphabet: string;
    private avoidSpecialChar: string;
    private rejectCount: any;

    constructor(private _addCommentsService: AddCommentsService, private service: SmePresanctionGetCommentsService, private checkSubmissionService: CheckSubmissionService, private submitSMEPresanctionService: SubmitSMEPresanctionService, private router: Router, private constants: Constants, private getReviewerService: SmePresanctionGetReviewersService, private utilsService: UtilsService, private confirmationService: ConfirmationService) {
        this.applicationSource = DIAConstants.applicationSource;
    }

    addComments() {
        //document.getElementById('test').scrollIntoView();
        var element = document.getElementById("bg-image");

        element.scrollLeft += 150;

        if (this.inspectionID) {
            this.isValidationComplete = true;
            //this.comments["borrowerRefId"] = this.borrowerRefId;
            //  if (this.comments.applicationSource && this.comments.commentsOnActivity && this.comments.commentOnGeneralWorkFirm && this.comments.commentsOnDataCollection && this.comments.pfIdOfReviewer) {
            if (this.comments.applicationSource == "Others") {
                if (!this.comments.otherSource) {
                    this.isValidationComplete = false;
                }
            }

            if (this.isValidationComplete) {
                this.comments["inspectionId"] = this.inspectionID;
                this.comments = this.utilsService.dataSourceSave(this.comments);
                var objectToPost: { officialComments: object; appId: string; executivePFIndex: string; } = { officialComments: this.comments, appId: "WEB-DIA", executivePFIndex: this.pfId };

                console.log(JSON.stringify(objectToPost));
                this._addCommentsService.postComment(objectToPost).subscribe(
                    data => {
                        console.log(this.responseStatus = data);
                        if (data.responseMessage == "Successful") {
                            this.checkSubmissionStatus();
                            this.getComments();
                            this.message = this.constants.getMessage('successMsg');
                            this.statusMsg = "success";
                        } else {
                            this.message = data.responseMessage;
                            this.statusMsg = "error";
                        }
                    },
                    err => {
                        console.log(err);
                        this.message = this.constants.getMessage('errorMsg');
                        this.statusMsg = "error";
                    },
                    () => console.log('Request Completed')
                );
                this.status = true;
                this.setTimeOut();
            } else {
                this.message = this.constants.getMessage('requiredFieldVal');
                this.statusMsg = "error";
                this.setTimeOut();
            }
            // } else {
            //     this.message = this.constants.getMessage('requiredFieldVal');
            //     this.statusMsg = "error";
            //     this.setTimeOut();
            // }
        } else {
            this.message = this.constants.getMessage('unitDetailVal');
            this.statusMsg = "error";
            this.setTimeOut();
        }
    }

    setTimeOut() {
        setTimeout(() => {
            this.message = "";
            this.onlyNumber = "";
            this.onlyAlphabet = "";
            this.avoidSpecialChar = "";
        }, 3000);
    }

    getReviewerList() {
        var objectToPost: { inspectorPfId: any; } = { inspectorPfId: this.pfId };
        console.log(JSON.stringify(objectToPost));
        this.getReviewerService.getReviewerList(objectToPost).subscribe(
            data => {
                if (data.responseCode == 200) {
                    this.reviewerList = data.reviwerList;
                    for (let k = 0; k < this.reviewerList.length; k++) {
                        let pfObj = {
                            label: this.reviewerList[k].reviewerPfId, value: this.reviewerList[k].reviewerPfId
                        }
                        this.pfIdOfReviewer.push(pfObj);
                    }
                } else {
                    this.message = data.responseMessage;
                    this.statusMsg = "error";
                }
                this.getComments();
                this.setTimeOut();
            },
            err => {
                console.log(err);
                this.message = this.constants.getMessage('errorMsg');
                this.setTimeOut();
            },
            () => console.log('Request Completed')
        );
    }

    checkSubmissionStatus() {
        var objectToPost: { appId: string; executivePFIndex: string; key: string; } = { appId: "WEB-DIA", executivePFIndex: this.pfId, key: this.inspectionID };

        console.log(JSON.stringify(objectToPost));
        this.checkSubmissionService.postComment(objectToPost).subscribe(
            data => {
                if (data.responseCode == 200) {
                    this.isSubmitValidationComplete = true;
                } else {
                    this.submissionStatusMessage = data.responseMessage;
                    this.isSubmitValidationComplete = false;
                }
            },
            err => {
                this.isSubmitValidationComplete = false;
            },
            () => console.log('Request Completed')
        );
        this.status = true;
        this.setTimeOut();
    }

    submitSMEPresanction() {
        this.rejectCount = +(localStorage.getItem("rejectedCount"));
        if (this.inspectionID) {
            if (this.rejectCount == 5) {
                this.message = this.constants.getMessage('rejectVal');
                this.statusMsg = "error";
                this.setTimeOut();
            } else {
                if (this.isSubmitValidationComplete) {
                    if (this.inspectionID) {
                        var objectToPost: { appId: string; executivePFIndex: string; key: string; } = { appId: "WEB-DIA", executivePFIndex: this.pfId, key: this.inspectionID };

                        console.log(JSON.stringify(objectToPost));
                        this.submitSMEPresanctionService.postComment(objectToPost).subscribe(
                            data => {
                                if (data.responseCode == 200) {
                                    this.message = this.constants.getMessage('submitSuccess');
                                    this.statusMsg = "success";
                                    this.router.navigate(['sme']);
                                } else {
                                    this.message = data.responseMessage;
                                    this.statusMsg = "error";
                                }
                            },
                            err => {
                                this.message = this.constants.getMessage('errorMsg');
                                this.statusMsg = "error";
                            },
                            () => console.log('Request Completed')
                        );
                        this.status = true;
                        this.setTimeOut();
                    } else {
                        this.message = this.constants.getMessage('unitDetailVal');
                        this.statusMsg = "error";
                        this.setTimeOut();
                    }
                } else {

                    this.confirmationService.confirm({
                        //message: this.submissionStatusMessage,
                        message: this.submissionStatusMessage,
                        accept: () => {
                            //CLOSE DIALOG
                        }
                    });
                }
            }
        } else {
            this.message = this.constants.getMessage('unitDetailVal');
            this.statusMsg = "error";
            this.setTimeOut();
        }
    }

    getComments() {
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.inspectionID };
        this.service.getComments(this.apiRequest).subscribe(response => {
            if (response.officialCommentsVO) {
                //this.registredAddressUnit = JSON.parse(localStorage.getItem("registredAddressUnit"));
                this.comments = response.officialCommentsVO;
                if (this.comments) {
                    if (this.comments.field_statusMap == null) {
                        this.comments = this.setFieldStatusObj();
                    }
                    this.comments = this.utilsService.dataSourceCheck(this.comments);
                    if (this.comments.applicationSource == "Others") {
                        this.isDisabled = true;
                    } else {
                        this.isDisabled = false;
                        this.comments.otherSource = null;
                    }
                }
                console.log(this.comments);
            } else {
                this.getDataFromLocalStorage();
            }
            this.checkSubmissionStatus();
        })
    }

    getDataFromLocalStorage() {
        this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
        if (this.loanDetailsWithOutInspection) {
            if (this.loanDetailsWithOutInspection.officialComments) {
                this.comments = this.loanDetailsWithOutInspection.officialComments;
                this.comments = this.utilsService.dataSourceCheckFromLOS(this.comments);
            } else {
                this.comments = this.utilsService.dataSourceCheckFromLOS(this.comments);
                this.noData = true;
            }
        } else {
            this.comments = this.utilsService.dataSourceCheckFromLOS(this.comments);
            this.noData = true;
        }
    }

    setFieldStatusObj() {
        var obj = this.comments;
        Object.keys(obj).forEach(function (key) {
            if (key.substr(-6) == "Status") {
                obj[key] = "null"
            }
        });
        return obj;
    }

    ngOnInit() {
        this.comments = new Comments();
        if (localStorage.getItem("isLoanEditable") != null && localStorage.getItem("isLoanEditable") != "null") {
            if (localStorage.getItem("isLoanEditable") == "true") {
                this.isLoanEditable = true;
            } else {
                this.isLoanEditable = false;
            }
        }
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null && localStorage.getItem("borrowerRefID") != "null" && localStorage.getItem("borrowerRefID") != null && localStorage.getItem("inspectionID") != "null" && localStorage.getItem("inspectionID") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
            this.borrowerRefId = JSON.parse(localStorage.getItem('borrowerRefID'));
            if (localStorage.getItem("inspectionID") != "null" && localStorage.getItem("inspectionID") != null) {
                this.inspectionID = JSON.parse(localStorage.getItem("inspectionID"));
            }
            this.getReviewerList();

        } else {
            this.getDataFromLocalStorage();
        }
    }

    applicationSourceSelection(data) {
        if (data == "Others") {
            this.isDisabled = true;
        } else {
            this.isDisabled = false;
            this.comments.otherSource = null;
        }
    }

    keypressCheck(data, field) {
        this.otherVal = false;
        this.positionVal = false;
        this.nameVal = false;
        switch (field) {
            case "Others":
                if (this.utilsService.validString) {
                    this.otherVal = true;
                } else {
                    this.otherVal = false;
                }
                break;

            case "Position":
                if (this.utilsService.validString) {
                    this.positionVal = true;
                } else {
                    this.positionVal = false;
                }
                break;

            case "Name":
                if (this.utilsService.validString) {
                    this.nameVal = true;
                } else {
                    this.nameVal = false;
                }
                break;

        }
        return data;
    }

    onBlurValidation(value, field) {
        if (value) {
            this.otherValBlur = false;
            this.positionValBlur = false;
            this.nameValBlur = false;
            this.onlyNumber = "Only numbers are allowed";
            this.onlyAlphabet = "Please enter only alphabets";
            this.avoidSpecialChar = "Please enter alphabets & numbers";
            switch (field) {
                case "Others":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.comments.otherSource = '';
                        this.otherValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Position":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.comments.positionOfStatutoryDues = '';
                        this.positionValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Name":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.comments.nameOfAccompanyingOfficial = '';
                        this.nameValBlur = true;
                        this.setTimeOut();
                    }
                    break;
            }
        }
    }
}