import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { SmeIncomplete } from '../../../../Service/SME/Presanction/Incomplete/incomplete';
import { SmePresanctionIncompleteService } from '../../../../Service/SME/Presanction/Incomplete/incomplete.service';
import { SmePresanctionSearchLoanService } from '../../../../Service/SME/Presanction/SearchLoan/searchLoan.service';

@Component({
    selector: 'presanctionIncomplete',
    templateUrl: './presanctionIncomplete.component.html',
    styleUrls: ['./presanctionIncomplete.component.scss', '../../../module.component.scss'],
    providers: [
        SmeIncomplete,
        SmePresanctionIncompleteService
    ]
})

export class PresanctionIncompleteComponent implements OnInit {
    private loading: boolean;
    private apiRequest: any;
    private incompleteList: any[];
    private executivePFIndex: any;
    private pfId: string;
    private responseMessage: any;
    private timer: any;

    constructor(private searchService: SmePresanctionSearchLoanService, private service: SmePresanctionIncompleteService,private router: Router, private route: ActivatedRoute) { }

    @Input() smeIncomplete: SmeIncomplete;

    ngOnInit() {
        this.loading = true;
        this.smeIncomplete = new SmeIncomplete();
        window.scrollTo(0, 0);
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null) {
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            if(this.executivePFIndex.user){
                this.pfId = this.executivePFIndex.user.pfId.toString();
                this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId };
                this.service.getSmeIncomplete(this.apiRequest).subscribe(response => {
                    if (response.incompleteList) {
                      this.incompleteList = response.incompleteList;
                      for (let k = 0; k < this.incompleteList.length; k++) {
                        if(this.incompleteList[k].loanStatus == 'WebIncomplete') {
                          this.incompleteList[k].loanStatus = 'Web Incomplete';
                        }
                        if(this.incompleteList[k].loanStatus == 'MobIncomplete') {
                          this.incompleteList[k].loanStatus = 'Mob Incomplete';
                        }
                      }
                        
                        console.log(this.incompleteList);
                    }
                })
            }
        }
    }

    completeLOS(data) {
        console.log(data);
        localStorage.setItem("borrowerRefID", null);
        localStorage.setItem("loanNo", null);
        localStorage.setItem("inspectionID", null);
        localStorage.setItem("loanDetailsWithOutInspection", null);
        localStorage.setItem("UnitName", null);
        if(data.borrowerRefId){
            localStorage.setItem("borrowerRefID", JSON.stringify(data.borrowerRefId));
        }
        localStorage.setItem("loanNo", JSON.stringify(data.losNum));
        localStorage.setItem("inspectionID", JSON.stringify(data.inspectionId));
        localStorage.setItem("isLoanEditable", "true");
        localStorage.setItem("isCollateralEditable", "false");
        this.getSmeSearchLoanWithLOS();
    }

        getSmeSearchLoanWithLOS() {
        let apiLOSRequest = { "executivePFIndex": this.pfId, "key": JSON.parse(localStorage.getItem("loanNo"))};
        this.searchService.getSmeSearchLoanWithLOS(apiLOSRequest).subscribe(response => {
            if (response.responseCode == 200) {
                if (!response.inspectionId) {
                    localStorage.setItem("loanDetailsWithOutInspection", JSON.stringify(response));
                }
            } else {
                this.setTimeOut();
                console.log(response);
                this.responseMessage = response.responseMessage;
            }
            this.router.navigate(['/sme/presanctionDetail']);
        })
    }

     setTimeOut() {
        this.timer = setTimeout(() => {
            this.responseMessage = '';
        }, 3000);
    }
}