import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DIAConstants } from '../../../../Shared/constants';
import { Constants } from '../../../../Shared/Constants/constants.service';

import { EstablishmentDetails } from '../../../../Service/SME/Presanction/EstablishmentDetails/establishmentDetails';
import { AddEstablishmentDetailsService } from '../../../../Service/SME/Presanction/EstablishmentDetails/AddEstablishmentDetails/addEstablishmentDetails.service';
import { SmePresanctionGetEstablishmentListService } from '../../../../Service/SME/Presanction/EstablishmentDetails/GetEstablishmentDetails/getEstablishmentDetails.service';

import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { UtilsService } from '../../../../Shared/UtilsService/utils.service';

@Component({
    selector: 'establishmentDetail',
    templateUrl: './establishmentDetail.component.html',
    styleUrls: ['./establishmentDetail.component.scss', '../../../module.component.scss'],
    providers: [
        AddEstablishmentDetailsService,
        SmePresanctionGetEstablishmentListService,
        UtilsService,
        Constants
    ]
})

export class EstablishmentDetailComponent implements OnInit {
    private establishmentWebVO: any;
    private apiRequest: any;
    private establishmentList: any;
    private message: string;
    private statusMsg: string;
    private states: any[];
    private upToFive: any[];
    private ownership: any = [];
    private noData: boolean = false;
    private proposedActivity: any = [];
    private executivePFIndex: any;
    private indexVal: any;
    private checked: boolean;
    private registredAddressUnit: any = {};
    private unRegistredAddressUnit: any = {};
    private images: any = [];
    private pfId: string;
    // private loanNumber: string;
    private inspectionID: string;
    private galleryOptionsSelfy: NgxGalleryOptions[];
    private galleryImagesSelfy: NgxGalleryImage[];
    private galleryOptions: NgxGalleryOptions[];
    private galleryImages: NgxGalleryImage[];
    private isLoanEditable: boolean;
    private isPinCodeValid: boolean = true;
    private loanDetailsWithOutInspection: any;
    private cityVal: boolean = false;
    private districtVal: boolean = false;
    private blockVal: boolean = false;
    private villageVal: boolean = false;
    private pincodeVal: boolean = false;
    private activityVal: boolean = false;
    private detailsVal: boolean = false;
    private establishmentListDupl = null;
    private cityValBlur: boolean = false;
    private districtValBlur: boolean = false;
    private blockValBlur: boolean = false;
    private villageValBlur: boolean = false;
    private pincodeValBlur: boolean = false;
    private activityValBlur: boolean = false;
    private detailsValBlur: boolean = false;
    private onlyNumber: string;
    private onlyAlphabet: string;
    private avoidSpecialChar: string;

    constructor(private _addEstablishmentDetailsService: AddEstablishmentDetailsService, private service: SmePresanctionGetEstablishmentListService, private route: ActivatedRoute, private utilsService: UtilsService, private constants: Constants) {
        this.states = DIAConstants.states;
        this.upToFive = DIAConstants.upToFive;
        this.ownership = DIAConstants.ownership
        this.proposedActivity = DIAConstants.proposedActivity;
    }

    @Input() establishmentDetails: EstablishmentDetails;
    responseStatus: Object = [];
    status: boolean;

    addEstablishmentDetail() {
        if (this.inspectionID) {
            //    if (this.establishmentList[this.indexVal].dateOfInspectionTSTemp && this.establishmentList[this.indexVal].building_no && this.establishmentList[this.indexVal].addressLine1 && this.establishmentList[this.indexVal].addressLine2 && this.establishmentList[this.indexVal].state && this.establishmentList[this.indexVal].city && this.establishmentList[this.indexVal].pinCode) {
            if (this.establishmentList[this.indexVal].pinCode) {
                if (this.establishmentList[this.indexVal].pinCode.length < 6) {
                    this.isPinCodeValid = false;
                } else {
                    this.isPinCodeValid = true;
                }
            } else {
                this.isPinCodeValid = true;
            }

            if (this.isPinCodeValid) {
                let actualTime = this.establishmentList[this.indexVal].dateOfInspectionTSTemp;
                let timeStamp = new Date(actualTime).getTime().toString();
                this.establishmentList[this.indexVal].dateOfInspectionTS = timeStamp;
                delete this.establishmentList[this.indexVal].dateOfInspectionTSTemp;
                this.checked = this.establishmentList[this.indexVal].checked;
                delete this.establishmentList[this.indexVal].checked;
                delete this.establishmentList[this.indexVal].message;
                delete this.establishmentList[this.indexVal].imageListStatus;
                // delete this.establishmentList[this.indexVal].pinCodeStatus;
                if (this.establishmentList[this.indexVal].losNum) {
                    delete this.establishmentList[this.indexVal].losNum;
                }
                if (this.inspectionID && !this.establishmentList[this.indexVal].inspectionId) {
                    this.establishmentList[this.indexVal].inspectionId = this.inspectionID;
                }
                this.establishmentList[this.indexVal] = this.utilsService.dataSourceSave(this.establishmentList[this.indexVal]);

                var objectToPost: { establishmentWebVO: any; appId: string; executivePFIndex: string; } = { establishmentWebVO: this.establishmentList[this.indexVal], appId: "WEB-DIA", executivePFIndex: this.pfId };
                this._addEstablishmentDetailsService.postComment(objectToPost).subscribe(
                    data => {
                        console.log(this.responseStatus = data);
                        if (data.responseMessage == "Successful") {
                            this.establishmentList[this.indexVal].dateOfInspectionTSTemp = new Date(parseInt(this.establishmentList[this.indexVal].dateOfInspectionTS));
                            this.establishmentList[this.indexVal].message = this.constants.getMessage('successMsg');
                            this.establishmentList[this.indexVal].establishRefId = data.refId;
                            this.statusMsg = "success";
                        } else {
                            this.establishmentList[this.indexVal].message = data.responseMessage;
                            this.statusMsg = "error";
                        }
                        this.setTimeOut();
                    },
                    err => {
                        console.log(err);
                        this.establishmentList[this.indexVal].message = this.constants.getMessage('errorMsg');
                        this.statusMsg = "error";
                        this.setTimeOut();
                    },
                    () => console.log('Request Completed')
                );
                this.establishmentList[this.indexVal].checked = this.checked;
                this.status = true;
            } else {
                if (!this.isPinCodeValid) {
                    this.establishmentList[this.indexVal].message = this.constants.getMessage('pinCodeVal');
                    this.statusMsg = "error";
                }
                this.setTimeOut();
            }
            // } else {
            //     this.establishmentList[this.indexVal].message = this.constants.getMessage('requiredFieldVal');
            //     this.statusMsg = "error";
            //     this.setTimeOut();
            // }
        } else {
            this.establishmentList[this.indexVal].message = this.constants.getMessage('unitDetailVal');
            this.statusMsg = "error";
            this.setTimeOut();
        }
    }

    setTimeOut() {
        setTimeout(() => {
            this.establishmentList[this.indexVal].message = "";
            this.onlyNumber = "";
            this.onlyAlphabet = "";
            this.avoidSpecialChar = "";
        }, 3000);
    }

    ngOnInit() {
        this.establishmentDetails = new EstablishmentDetails();
        window.scrollTo(0, 0);
        if (localStorage.getItem("isLoanEditable") != null && localStorage.getItem("isLoanEditable") != "null") {
            if (localStorage.getItem("isLoanEditable") == "true") {
                this.isLoanEditable = true;
            } else {
                this.isLoanEditable = false;
            }
        }
        if (localStorage.getItem("userDetails") != "null" && localStorage.getItem("userDetails") != null && localStorage.getItem("inspectionID") != "null" && localStorage.getItem("inspectionID") != null) {
            // this.loanNumber = JSON.parse(localStorage.getItem('loanNo'));
            this.inspectionID = JSON.parse(localStorage.getItem("inspectionID"));
            this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
            this.pfId = this.executivePFIndex.user.pfId.toString();
            this.getEstablishmentDetails();
        } else {
            this.getDataFromLocalStorage();
        }
    }

    getEstablishmentDetails() {
        this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.inspectionID };
        this.service.getEstablishmentList(this.apiRequest).subscribe(response => {
            if (response.establishmentWebVOList) {
                this.registredAddressUnit = JSON.parse(localStorage.getItem("registredAddressUnit"));
                this.establishmentList = response.establishmentWebVOList;
                console.log(this.establishmentList);
                if (this.establishmentList.length > 0) {
                    for (let k = 0; k < this.establishmentList.length; k++) {
                        if (this.establishmentList[k].establishRefId == null) {
                            this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
                            if (this.loanDetailsWithOutInspection) {
                                if (this.loanDetailsWithOutInspection.establishmentList && this.loanDetailsWithOutInspection.establishmentList.length > 0) {
                                    if (this.loanDetailsWithOutInspection.establishmentList.length > k) {
                                        this.establishmentList[k] = this.loanDetailsWithOutInspection.gurantorList[k];
                                        this.establishmentList[k].establishRefId = null;
                                        this.establishmentList[k] = this.setEstablishmentObject(this.establishmentList[k]);
                                        this.establishmentList[k] = this.utilsService.dataSourceCheckFromLOS(this.establishmentList[k]);
                                    } else {
                                        this.establishmentList[k] = this.utilsService.dataSourceCheck(this.establishmentList[k]);
                                    }
                                }
                            } else {
                                this.establishmentList[k] = this.utilsService.dataSourceCheck(this.establishmentList[k]);
                            }
                        } else {
                            this.establishmentList[k] = this.utilsService.dataSourceCheck(this.establishmentList[k]);
                        }
                    }


                    this.unRegistredAddressUnit = JSON.stringify(this.establishmentList[0]);
                    this.imageGalleryOptions();
                    // if (this.unitDetails.dateOfInspectionTS != null) {
                    //     this.establishmentDate = new Date(this.unitDetails.dateOfInspectionTS);
                    // }
                    for (let i = 0; i < this.establishmentList.length; i++) {
                        this.establishmentList[i].checked = false;
                        this.establishmentList[i].message = '';
                        if (this.establishmentList[i].dateOfInspectionTS != null) {
                            if (+this.establishmentList[i].dateOfInspectionTS == 0) {
                                this.establishmentList[i].dateOfInspectionTSTemp = '';
                            } else {
                                this.establishmentList[i].dateOfInspectionTSTemp = new Date(this.establishmentList[i].dateOfInspectionTS);
                            }
                        }
                    }
                    this.establishmentListDupl = this.establishmentList;
                } else {
                    this.getDataFromLocalStorage();
                }
            } else {
                this.getDataFromLocalStorage();
            }
        })
    }

    getDataFromLocalStorage() {
        this.loanDetailsWithOutInspection = JSON.parse(localStorage.getItem("loanDetailsWithOutInspection"));
        if (this.loanDetailsWithOutInspection) {
            if (this.loanDetailsWithOutInspection.establishmentList && this.loanDetailsWithOutInspection.establishmentList.length > 0) {
                this.establishmentList = this.loanDetailsWithOutInspection.establishmentList;
                for (let k = 0; k < this.establishmentList.length; k++) {
                    this.establishmentList[k] = this.setEstablishmentObject(this.establishmentList[k]);
                }
                this.establishmentList = this.utilsService.dataSourceCheckFromLOSArray(this.establishmentList);
                this.establishmentListDupl = this.establishmentList;
                this.unRegistredAddressUnit = JSON.stringify(this.establishmentList[0]);
                this.imageGalleryOptions();
                for (let i = 0; i < this.establishmentList.length; i++) {
                    this.establishmentList[i].checked = false;
                    this.establishmentList[i].message = '';
                    this.establishmentList[i].establishRefId = null;
                    if (this.establishmentList[i].dateOfInspectionTS != null) {
                        if (+this.establishmentList[i].dateOfInspectionTS == 0) {
                            this.establishmentList[i].dateOfInspectionTSTemp = '';
                        } else {
                            this.establishmentList[i].dateOfInspectionTSTemp = new Date(this.establishmentList[i].dateOfInspectionTS);
                        }
                    }
                }
            } else {
                this.noData = true;
            }
        } else {
            this.noData = true;
        }
    }

    setEstablishmentObject(object) {
        object["addressLine2"] = null;
        object["building_no"] = null;
        object["district"] = null;
        object["sub_district"] = null;
        object["village"] = null;
        object["ownership"] = null;
        object["proposedActivity"] = null;
        object["activity"] = null;
        object["comment"] = null;
        return object;
    }

    tabHeaderClick(i, establishmentDetail) {
        this.indexVal = i;
        this.galleryImagesSelfy = [];
        this.galleryImages = [];
        if (establishmentDetail.imageList != null) {
            for (let j = 0; j < establishmentDetail.imageList.length; j++) {
              let latitudeAndLongitude = "Latitude: " + establishmentDetail.imageList[j].imageLatitude + ", " + "Longitude: " + establishmentDetail.imageList[j].imageLongitude;
                let imageObj = {
                    small: establishmentDetail.imageList[j].imagePath,
                    medium: establishmentDetail.imageList[j].imagePath,
                    big: establishmentDetail.imageList[j].imagePath,
                    description: latitudeAndLongitude
                }
                if (establishmentDetail.imageList[j].imageType == "SELFIE_TYPE") {
                    if (this.galleryImagesSelfy.length < 1) {
                        this.galleryImagesSelfy.push(imageObj);
                    }
                } else {
                    this.galleryImages.push(imageObj);
                }
            }
        }
        for (var k = 0; k < this.establishmentList.length; k++) {
            this.establishmentList[k].message = '';
        }
    }

    imageGalleryOptions() { 
     this.galleryOptionsSelfy = [
            {
                "image": false,
                "height": "80px",
                "width": "100px",
                "thumbnailsColumns": 1
            }
        ];

        this.galleryOptions = [
            {
                "image": false,
                "height": "80px",
                "width": "100%",
                "thumbnailsColumns": 6,
                "previewDescription": true
            }
        ];
    }

    sameAddress(i) {
        if (this.establishmentList[this.indexVal].checked) {
            console.log(this.registredAddressUnit);
            this.unRegistredAddressUnit = JSON.stringify(this.establishmentList[this.indexVal]);
            if (!(this.establishmentListDupl[this.indexVal].building_noStatus == 'LOS' || this.establishmentListDupl[this.indexVal].building_noStatus == 'Mob')) {
                this.establishmentList[this.indexVal].building_no = this.registredAddressUnit.plotBuildingFlatNum;
            }
            if (!(this.establishmentListDupl[this.indexVal].addressLine1Status == 'LOS' || this.establishmentListDupl[this.indexVal].addressLine1Status == 'Mob')) {
                this.establishmentList[this.indexVal].addressLine1 = this.registredAddressUnit.addressLine1;
            }
            if (!(this.establishmentListDupl[this.indexVal].addressLine2Status == 'LOS' || this.establishmentListDupl[this.indexVal].addressLine2Status == 'Mob')) {
                this.establishmentList[this.indexVal].addressLine2 = this.registredAddressUnit.addressLine2;
            }
            if (!(this.establishmentListDupl[this.indexVal].stateStatus == 'LOS' || this.establishmentListDupl[this.indexVal].stateStatus == 'Mob')) {
                this.establishmentList[this.indexVal].state = this.registredAddressUnit.state;
            }
            if (!(this.establishmentListDupl[this.indexVal].cityStatus == 'LOS' || this.establishmentListDupl[this.indexVal].cityStatus == 'Mob')) {
                this.establishmentList[this.indexVal].city = this.registredAddressUnit.city;
            }
            if (!(this.establishmentListDupl[this.indexVal].districtStatus == 'LOS' || this.establishmentListDupl[this.indexVal].districtStatus == 'Mob')) {
                this.establishmentList[this.indexVal].district = this.registredAddressUnit.district;
            }
            if (!(this.establishmentListDupl[this.indexVal].sub_districtStatus == 'LOS' || this.establishmentListDupl[this.indexVal].sub_districtStatus == 'Mob')) {
                this.establishmentList[this.indexVal].sub_district = this.registredAddressUnit.blockTehsilSubdistrict;
            }
            if (!(this.establishmentListDupl[this.indexVal].villageStatus == 'LOS' || this.establishmentListDupl[this.indexVal].villageStatus == 'Mob')) {
                this.establishmentList[this.indexVal].village = this.registredAddressUnit.village;
            }
            if (!(this.establishmentListDupl[this.indexVal].pinCodeStatus == 'LOS' || this.establishmentListDupl[this.indexVal].pinCodeStatus == 'Mob')) {
                this.establishmentList[this.indexVal].pinCode = this.registredAddressUnit.pinCode;
            }
        } else {
            console.log("Unchecked");
            this.unRegistredAddressUnit = JSON.parse(this.unRegistredAddressUnit);
            this.establishmentList[this.indexVal].building_no = this.unRegistredAddressUnit.building_no;
            this.establishmentList[this.indexVal].addressLine1 = this.unRegistredAddressUnit.addressLine1;
            this.establishmentList[this.indexVal].addressLine2 = this.unRegistredAddressUnit.addressLine2;
            this.establishmentList[this.indexVal].state = this.unRegistredAddressUnit.state;
            this.establishmentList[this.indexVal].city = this.unRegistredAddressUnit.city;
            this.establishmentList[this.indexVal].district = this.unRegistredAddressUnit.district;
            this.establishmentList[this.indexVal].sub_district = this.unRegistredAddressUnit.sub_district;
            this.establishmentList[this.indexVal].village = this.unRegistredAddressUnit.village;
            this.establishmentList[this.indexVal].pinCode = this.unRegistredAddressUnit.pinCode;
        }
    }

    keypressCheck(data, field) {
        this.cityVal = false;
        this.districtVal = false;
        this.blockVal = false;
        this.villageVal = false;
        this.pincodeVal = false;
        this.activityVal = false;
        this.detailsVal = false;
        switch (field) {
            case "City":
                if (this.utilsService.validString) {
                    this.cityVal = true;
                } else {
                    this.cityVal = false;
                }
                break;

            case "District":
                if (this.utilsService.validString) {
                    this.districtVal = true;
                } else {
                    this.districtVal = false;
                }
                break;

            case "Block":
                if (this.utilsService.validString) {
                    this.blockVal = true;
                } else {
                    this.blockVal = false;
                }
                break;

            case "Village":
                if (this.utilsService.validString) {
                    this.villageVal = true;
                } else {
                    this.villageVal = false;
                }
                break;

            case "Pincode":
                if (this.utilsService.validString) {
                    this.pincodeVal = true;
                } else {
                    this.pincodeVal = false;
                }
                break;

            case "Activity":
                if (this.utilsService.validString) {
                    this.activityVal = true;
                } else {
                    this.activityVal = false;
                }
                break;

            case "Details":
                if (this.utilsService.validString) {
                    this.detailsVal = true;
                } else {
                    this.detailsVal = false;
                }
                break;
        }
        return data;
    }

    onBlurValidation(value, field) {
        if (value) {
            this.cityValBlur = false;
            this.districtValBlur = false;
            this.blockValBlur = false;
            this.villageValBlur = false;
            this.pincodeValBlur = false;
            this.activityValBlur = false;
            this.detailsValBlur = false;
            this.onlyNumber = "Only numbers are allowed";
            this.onlyAlphabet = "Please enter only alphabets";
            this.avoidSpecialChar = "Please enter alphabets & numbers";
            switch (field) {
                case "City":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.establishmentList[this.indexVal].city = '';
                        this.cityValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "District":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.establishmentList[this.indexVal].district = '';
                        this.districtValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Block":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.establishmentList[this.indexVal].sub_district = '';
                        this.blockValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Village":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.establishmentList[this.indexVal].village = '';
                        this.villageValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Pincode":
                    if (!(value.match(/^\d+$/))) {
                        this.establishmentList[this.indexVal].pinCode = '';
                        this.pincodeValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Activity":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.establishmentList[this.indexVal].activity = '';
                        this.activityValBlur = true;
                        this.setTimeOut();
                    }
                    break;

                case "Details":
                    if (!(value.match(/^[a-zA-Z&-/.#, ]*$/))) {
                        this.establishmentList[this.indexVal].comment = '';
                        this.detailsValBlur = true;
                        this.setTimeOut();
                    }
                    break;
            }
        }
    }
}