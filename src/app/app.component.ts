import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  //title = 'app';
  private appUrl: string;
  private image: string;

  constructor( @Inject(DOCUMENT) private document: Document, router: Router) {
    let baseAppUrl = (this.document.location.href).split('/');
    this.appUrl = baseAppUrl[baseAppUrl.length - 1];
    router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
         const mainDiv = document.getElementById('bg-image');
        if (mainDiv) {
          setTimeout(function() {
            mainDiv.scrollTop = 0;
          }, 1)
          
        }
        let urlContent = event.url.split('/');
        let letlastChar = urlContent[urlContent.length - 1];
        if (letlastChar == "" || letlastChar == "sme") {
          this.image = "./assets/images/sme-banner.png"
        } else {
          this.image = ""
        }
      }
    });
  }
  ngOnInit() {
   
  }

}
