import { Component, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { SmeGetInspectionDetailsService } from '../../Service/SME/ReviewReport/GetInspectionDetails/getInspectionDetails';

@Component({
  selector: 'popupAsideMenu',
  templateUrl: './popupAsideMenu.component.html',
  styleUrls: ['./popupAsideMenu.component.scss', '../../Module/module.component.scss'],
  providers: [
    SmeGetInspectionDetailsService
  ]
})
export class PopupAsideMenuComponent {

  private apiRequest: object;
  private pfId: string;
  private executivePFIndex: any;
  private inspectionId: any;
  private inspectionDetails: any;
  private keyPersonCount: any = [];
  private collateralCount: any = [];
  private guarantorCount: any = [];
  private establishmentCount: any = [];
  private borrowerSelectedIndex: number;
  private guarantorSelectedIndex: number;
  private establishmentSelectedIndex: number;
  private collateralSelectedIndex: number;
  private index: number = null;
  private lastIndex = -1;
  private initialSelect: boolean;
  private collateralCountForSearchReport: boolean = false;
  private guarantorCountForSearchReport: boolean = false;
  private establishmentCountForSearchReport: boolean = false;
  private inspectionReviewStatus: boolean = false;

  constructor(private service: SmeGetInspectionDetailsService, private route: ActivatedRoute, private router: Router, @Inject(DOCUMENT) private document: Document) { 
    let urlCheck = this.document.location.href;
    if(urlCheck.includes("searchReport")) {
      this.inspectionReviewStatus = true;
    }
  }

  ngOnInit() {
    this.initialSelect = true;
    if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
      this.executivePFIndex = JSON.parse(localStorage.getItem("userDetails"));
      this.pfId = this.executivePFIndex.user.pfId.toString();
    }
    if (localStorage.getItem("inspectionID") != null && localStorage.getItem("inspectionID") != "null") {
      this.inspectionId = JSON.parse(localStorage.getItem('inspectionID'));

      this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId, "key": this.inspectionId };
      this.service.getInspectionDetails(this.apiRequest).subscribe(response => {
        if (response.responseCode == 200) {
          this.inspectionDetails = response;
          this.keyPersonCount = this.inspectionDetails.keyPersonDetails;
          this.keyPersonCount.forEach(element => {
            element.active = 'active';
          });
          this.collateralCount = this.inspectionDetails.collateralList;
          this.guarantorCount = this.inspectionDetails.guarantorList;
          this.establishmentCount = this.inspectionDetails.establishmentList;
          if(this.collateralCount.length == 0) {
            this.collateralCountForSearchReport = true;
          }
          if(this.guarantorCount.length == 0) {
            this.guarantorCountForSearchReport = true;
          }
          if(this.establishmentCount.length == 0) {
            this.establishmentCountForSearchReport = true;
          }
        } else {
          console.log("Error");
        }
      })
    }
  }


  borrowerSelect(index: number) {
    if (index < 10) {
      this.borrowerSelectedIndex = index;
    } else {
      this.borrowerSelectedIndex = null;
    }
  }

  guarantorSelect(index: number) {
    this.borrowerSelectedIndex = null;
    this.collateralSelectedIndex = null;
    this.establishmentSelectedIndex = null;
    this.guarantorSelectedIndex = index;
  }

  establishmentSelect(index: number) {
    this.borrowerSelectedIndex = null;
    this.guarantorSelectedIndex = null;
    this.collateralSelectedIndex = null;
    this.establishmentSelectedIndex = index;
  }

  collateralSelect(index: number) {
    this.borrowerSelectedIndex = null;
    this.guarantorSelectedIndex = null;
    this.establishmentSelectedIndex = null;
    this.collateralSelectedIndex = index;
  }

  withoutSelect() {
    this.index = this.lastIndex--;
    this.borrowerSelectedIndex = null;
    this.guarantorSelectedIndex = null;
    this.establishmentSelectedIndex = null;
    this.collateralSelectedIndex = null;
  }
}
