import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { Constants } from '../../Shared/Constants/constants.service';

import { SmeSectionNotification } from '../../Service/SME/Presanction/SectionNotification/sectionNotification';
import { SmePresanctionSectionNotificationService } from '../../Service/SME/Presanction/SectionNotification/sectionNotification.service';
import { DIAUserInfoService } from '../../Service/User/userInfo.service';

@Component({
    selector: 'dia-section',
    templateUrl: './section.component.html',
    styleUrls: ['./section.component.scss', '../../Module/module.component.scss'],
    providers: [
        SmeSectionNotification,
        SmePresanctionSectionNotificationService,
        DIAUserInfoService,
        Constants
    ]
})
export class SectionComponent implements OnInit {
    private apiRequest: any;
    private notificationCount: any;
    private executivePFIndex: any;
    private pfId: any;
    private password: string;
    private statusMsg: string;
    private image: string;
    private loanName: string;
    private userDetails: boolean = true;
    private message: string;
    private userRole: Number;

    constructor(private service: SmePresanctionSectionNotificationService, private route: ActivatedRoute, router: Router, private constants: Constants,private userService: DIAUserInfoService) {
        router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                let urlContent = event.url.split('/');
                let letlastChar = urlContent[urlContent.length - 1];
                if (letlastChar == "" || letlastChar == "sme") {
                    this.loanName = "SME"
                }
            }
        });

    }

    ngOnInit() {
        if (localStorage.getItem("userDetails") != null && localStorage.getItem("userDetails") != "null") {
            this.pfId = localStorage.getItem("pfID");
            this.getNotification();
            if(this.pfId == JSON.parse(localStorage.getItem("userDetails")).user.pfId){
              this.userRole = JSON.parse(localStorage.getItem("userDetails")).userDetails.role;
              if(JSON.parse(localStorage.getItem("userDetails")).userDetails.role != 4) {
                this.userDetails = false;
              }
            }else{
              this.getUserDetails();
            }
            
        } else if(localStorage.getItem("pfID") && localStorage.getItem("pfID") != null && localStorage.getItem("pfID") != "null"){
            this.pfId = localStorage.getItem("pfID");
            this.getUserDetails();
            }else{
              localStorage.setItem("userDetails", null);
              this.message = "You don't have access to DIA Application, Please raise a request in DIA User Management Portal";
            }
        }
        getNotification() {
            this.apiRequest = { "appId": "WEB-DIA", "executivePFIndex": this.pfId };
    
            this.service.getNotificationCount(this.apiRequest).subscribe(response => {
                if (response.status) {
                    this.notificationCount = response.status;
                    console.log(this.notificationCount);
                }
            })
        }

        getUserDetails(){
         // this.password = "hrmS@123";
         // this.apiRequest = { "pfId": this.pfId, "password": this.password, "invokedFrom": "other" };
          this.apiRequest = { "pfId": this.pfId, "invokedFrom": "other"};
          this.userService.getUserInfo(this.apiRequest).subscribe(response => {
              if (response && response.userDetails) {
                if(response.userDetails.role == 4 ){
                  localStorage.setItem("userDetails", null);
                  localStorage.setItem("userDetails", JSON.stringify(response));
                  this.getNotification();
                }else{
                  localStorage.setItem("userDetails", null);
                  this.message = "You don't have access to DIA Application, Please raise a request in DIA User Management Portal";
                  this.userDetails = false;
                }
              } else {
                  this.statusMsg = this.constants.getMessage('borrowerVal');
                  console.log(this.statusMsg);
                  this.userDetails = false;
              }
          })
        }
    }
