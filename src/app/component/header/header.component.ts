import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DIAUserInfoService } from '../../Service/User/userInfo.service';
import { UserInfo } from '../../Service/User/userInfo';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'dia-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [
    UserInfo,
    DIAUserInfoService
  ]
})
export class HeaderComponent implements OnInit {
  private apiRequest: any;
  private pfId: any;
  private password: string;
  private message: string;

  constructor(private service: DIAUserInfoService, private route: ActivatedRoute) {

  }

  @Input() userInfo: UserInfo;
  ngOnInit() {
    this.userInfo = new UserInfo();
    // localStorage.setItem("pfID", null);
    // if (Cookie.get('PFID') && Cookie.get('PFID') != null && Cookie.get('PFID') != "null") {
    // this.pfId = Cookie.get('PFID');
     
      this.pfId = 1100024;
    //  this.password = "hrmS@123";
    this.apiRequest = { "pfId": this.pfId, "invokedFrom": "login"};
   // this.apiRequest = { "pfId": this.pfId, "password": this.password, "invokedFrom": "login" };
    localStorage.setItem("pfID", this.pfId);
    this.service.getUserInfo(this.apiRequest).subscribe(response => {
      if (response && response.userDetails) {
        if (response.userDetails.role == 4) {
          this.userInfo = response;
          localStorage.setItem("userDetails", null);
          localStorage.setItem("userDetails", JSON.stringify(this.userInfo));
          console.log(this.userInfo);
        } else {
          this.message = "You don't have access to DIA Application, Please raise a request in DIA User Management Portal";
        }
      } else {
        localStorage.setItem("userDetails", null);
        this.message = "You don't have access to DIA Application, Please raise a request in DIA User Management Portal";
        console.log("Error");
      }
    })
    // } else {
    //   localStorage.setItem("userDetails", null);
    //   this.message = "You don't have access to DIA Application, Please raise a request in DIA User Management Portal";
    // }
  }

  logout() {
    console.log("logut");
    localStorage.setItem("pfID", null);
    localStorage.setItem("userDetails", null);
    localStorage.setItem("borrowerRefID", null);
    localStorage.setItem("loanNo", null);
    localStorage.setItem("inspectionID", null);
    localStorage.setItem("isLoanEditable", null);
    localStorage.setItem("isCollateralEditable", null);
    localStorage.setItem("registredAddressUnit", null);
    localStorage.setItem("salutationAddress", null);
    Cookie.set('PFID',  null);
    window.location.href  =  'https://10.209.74.71:7002/diauiuat/logout';
  }
}