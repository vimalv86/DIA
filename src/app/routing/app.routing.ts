import { Routes } from '@angular/router';

import { SmeComponent } from '../Module/SME/sme.component';
import { McgComponent } from '../Module/MCG/mcg.component';
import { AgricultureComponent } from '../Module/Agriculture/agriculture.component';
import { AutomobileComponent } from '../Module/Automobile/automobile.component';
import { CagComponent } from '../Module/CAG/cag.component';
import { EducationComponent } from '../Module/Education/education.component';
import { HomeComponent } from '../Module/Home/home.component';
import { PersonalComponent } from '../Module/Personal/personal.component';
import { SectionComponent } from '../Component/Section/section.component';
import { DetailComponent } from '../Module/SME/Presanction/Detail/detail.component';
import { PresanctionComponent } from '../Module/SME/Presanction/presanction.component';
import { PresanctionNewComponent } from '../Module/SME/Presanction/PresanctionNew/presanctionNew.component';
import { PresanctionListComponent } from '../Module/SME/Presanction/PresanctionList/presanctionList.component';
import { PresanctionIncompleteComponent } from '../Module/SME/Presanction/PresanctionIncomplete/presanctionIncomplete.component';
import { PresanctionInspectionStatusComponent } from '../Module/SME/Presanction/PresanctionInspectionStatus/presanctionInspectionStatus.component'; 
import { PresanctionDetailComponent } from '../Module/SME/Presanction/PresanctionDetail/presanctionDetail.component';
import { BorrowerDetailComponent } from '../Module/SME/Presanction/BorrowerDetails/borrowerDetail.component';
import { UnitDetailComponent } from '../Module/SME/Presanction/BorrowerDetails/UnitDetails/unitDetail.component';
import { ContactPersonDetailComponent } from '../Module/SME/Presanction/BorrowerDetails/ContactPersonDetails/contactPersonDetail.component';
import { KeyPersonDetailComponent } from '../Module/SME/Presanction/BorrowerDetails/KeyPersonDetails/keyPersonDetail.component';
import { GuarantorDetailComponent } from '../Module/SME/Presanction/GuarantorDetails/guarantorDetail.component';
import { EstablishmentDetailComponent } from '../Module/SME/Presanction/EstablishmentDetails/establishmentDetail.component';
import { CollateralDetailComponent } from '../Module/SME/Presanction/CollateralDetails/collateralDetail.component';
import { LoanRequestedComponent } from '../Module/SME/Presanction/LoanRequested/loanRequested.component';
import { PresentLiabilitiesComponent } from '../Module/SME/Presanction/PresentLiabilities/presentLiabilities.component';
import { CommentsComponent } from '../Module/SME/Presanction/Comments/comments.component';

import { ReviewReportComponent } from '../Module/SME/ReviewReport/reviewReport.component';
import { ReviewReportPopupComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/reviewReportPopup.component';
import { ManageInspectionComponent } from '../Module/SME/ManageInspection/manageInspection.component';
import { ManageReviewerComponent } from '../Module/SME/ManageInspection/ManageReviewer/manageReviewer.component';
import { ChangeReviewerComponent } from '../Module/SME/ManageInspection/ChangeReviewer/changeReviewer.component';
import { ManageAccountTransfer } from '../Module/SME/ManageInspection/ManageAccountTransfer/manageAccountTransfer.component';
import { PopupUnitDetailsComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/UnitDetails/unitDetails.component';
import { ContactPersonDetailsComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/ContactPerson/contactPerson.component';
import { KeyPersonDetailsComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/KeyPerson/keyPerson.component';
import { PopupGuarantorDetailsComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/GuarantorDetails/guarantorDetails.component';
import { PopupEstablishmentDetailsComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/EstablishmentDetails/establishmentDetails.component';
import { PopupCollateralDetailsComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/CollateralDetails/collateralDetails.component';
import { PopupLoanRequestedComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/LoanRequested/loanRequested.component';
import { PopupPresentLiabilitiesComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/PresentLiabilities/presentLiabilities.component';
import { PopupCommentsComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/Comments/comments.component';
import { PopupInspectionComponent } from '../Module/SME/ReviewReport/ReviewReportPopup/Inspection/inspection.component';

import { SearchReportComponent } from '../Module/SME/SearchReport/searchReport.component';
import { SearchReportPopupComponent } from '../Module/SME/SearchReport/SearchReportPopup/searchReportPopup.component';
import { SearchPopupUnitDetailsComponent } from '../Module/SME/SearchReport/SearchReportPopup/UnitDetails/unitDetails.component';
import { SearchContactPersonDetailsComponent } from '../Module/SME/SearchReport/SearchReportPopup/ContactPerson/contactPerson.component';
import { SearchKeyPersonDetailsComponent } from '../Module/SME/SearchReport/SearchReportPopup/KeyPerson/keyPerson.component';
import { SearchPopupGuarantorDetailsComponent } from '../Module/SME/SearchReport/SearchReportPopup/GuarantorDetails/guarantorDetails.component';
import { SearchPopupEstablishmentDetailsComponent } from '../Module/SME/SearchReport/SearchReportPopup/EstablishmentDetails/establishmentDetails.component';
import { SearchPopupCollateralDetailsComponent } from '../Module/SME/SearchReport/SearchReportPopup/CollateralDetails/collateralDetails.component';
import { SearchPopupLoanRequestedComponent } from '../Module/SME/SearchReport/SearchReportPopup/LoanRequested/loanRequested.component';
import { SearchPopupPresentLiabilitiesComponent } from '../Module/SME/SearchReport/SearchReportPopup/PresentLiabilities/presentLiabilities.component';
import { SearchPopupCommentsComponent } from '../Module/SME/SearchReport/SearchReportPopup/Comments/comments.component';
import { SearchPopupInspectionComponent } from '../Module/SME/SearchReport/SearchReportPopup/Inspection/inspection.component';

export const routes: Routes = [
    {
        path: 'sme',
        component: SmeComponent,
        children: [
            {
                path: '',
                component: SectionComponent
            }, 
            {
                path: 'detail',
                component: DetailComponent,
                children: [
                    {
                        path: 'presanctionNew',
                        component: PresanctionNewComponent,
                        children: [
                            {
                                path: 'presanctionList',
                                component: PresanctionListComponent
                            }
                        ]
                    },
                    {
                        path: 'presanctionIncomplete',
                        component: PresanctionIncompleteComponent
                    },
                    {
                        path: 'presanctionInspection',
                        component: PresanctionInspectionStatusComponent
                    }
                ]
            },
            {
                path: 'presanctionDetail',
                component: PresanctionDetailComponent,
                children: [
                    {
                        path: 'borrowerDetails',
                        component: BorrowerDetailComponent,
                        children: [
                            {
                                path: 'unitDetails',
                                component: UnitDetailComponent
                            },
                            {
                                path: 'contactPersonDetails',
                                component: ContactPersonDetailComponent
                            },
                            {
                                path: 'keyPersonDetails',
                                component: KeyPersonDetailComponent
                            },
                            {
                                path: '',
                                redirectTo: 'unitDetails',
                                pathMatch: 'full'
                            }
                        ]
                    },
                    {
                        path:'guarantorDetails',
                        component: GuarantorDetailComponent
                    },
                    {
                        path: 'establishmentDetails',
                        component: EstablishmentDetailComponent
                    },
                    {
                        path: 'collateralDetails',
                        component: CollateralDetailComponent
                    },
                    {
                        path: 'loanRequested',
                        component: LoanRequestedComponent
                    },
                    {
                        path: 'presentLiabilities',
                        component: PresentLiabilitiesComponent
                    },
                    {
                        path: 'comments',
                        component: CommentsComponent
                    },
                    {
                        path: '',
                        redirectTo: 'borrowerDetails',
                        pathMatch: 'full'
                    }
                ]
            },
            {
                path: 'reviewReport',
                component: ReviewReportComponent,
                children: [
                    {
                        path: 'reviewReportPopup',
                        component: ReviewReportPopupComponent,
                        children: [
                          {
                            path: 'unitDetails',
                            component: PopupUnitDetailsComponent
                          },
                          {
                            path: 'contactPersonDetails',
                            component: ContactPersonDetailsComponent
                          },
                          {
                            path: 'keyPersonDetails',
                            component: KeyPersonDetailsComponent
                          },
                          {
                            path: 'guarantorDetail',
                            component: PopupGuarantorDetailsComponent
                          },
                          {
                            path: 'establishmentDetail',
                            component: PopupEstablishmentDetailsComponent
                          },
                          {
                            path: 'collateralDetail',
                            component: PopupCollateralDetailsComponent
                          },
                          {
                            path: 'loanRequested',
                            component: PopupLoanRequestedComponent
                          },
                          {
                            path: 'presentLiabilities',
                            component: PopupPresentLiabilitiesComponent
                          },
                          {
                            path: 'comments',
                            component: PopupCommentsComponent
                          },
                          {
                            path: 'inspection',
                            component: PopupInspectionComponent
                          },
                          {
                              path: '',
                              redirectTo: 'unitDetails',
                              pathMatch: 'full'
                          }
                        ]
                    }
                ]
            },
            {
                path: 'manageInspection',
                component: ManageInspectionComponent,
                children: [
                    {
                        path: 'manageReviewer',
                        component: ManageReviewerComponent
                    },
                    {
                        path: 'changeReviewer',
                        component: ChangeReviewerComponent
                    },
                    {
                        path: 'manageAccountTransfer',
                        component: ManageAccountTransfer
                    },
                    {
                        path: '',
                        redirectTo: 'manageReviewer',
                        pathMatch: 'full'
                    }
                ]
            },
            {
              path: 'searchReport',
              component: SearchReportComponent,
              children: [
                  {
                      path: 'searchReportPopup',
                      component: SearchReportPopupComponent,
                      children: [
                        {
                          path: 'unitDetails',
                          component: SearchPopupUnitDetailsComponent
                        },
                        {
                          path: 'contactPersonDetails',
                          component: SearchContactPersonDetailsComponent
                        },
                        {
                          path: 'keyPersonDetails',
                          component: SearchKeyPersonDetailsComponent
                        },
                        {
                          path: 'guarantorDetail',
                          component: SearchPopupGuarantorDetailsComponent
                        },
                        {
                          path: 'establishmentDetail',
                          component: SearchPopupEstablishmentDetailsComponent
                        },
                        {
                          path: 'collateralDetail',
                          component: SearchPopupCollateralDetailsComponent
                        },
                        {
                          path: 'loanRequested',
                          component: SearchPopupLoanRequestedComponent
                        },
                        {
                          path: 'presentLiabilities',
                          component: SearchPopupPresentLiabilitiesComponent
                        },
                        {
                          path: 'comments',
                          component: SearchPopupCommentsComponent
                        },
                        {
                          path: 'inspection',
                          component: SearchPopupInspectionComponent
                        },
                        {
                            path: '',
                            redirectTo: 'unitDetails',
                            pathMatch: 'full'
                        }
                      ]
                  }
              ]
          }
        ]
    },
    {
        path: 'mcg',
        component: McgComponent
    },
    {
        path: 'agriculture',
        component: AgricultureComponent
    },
    {
        path: 'automobile',
        component: AutomobileComponent
    },
    {
        path: 'cag',
        component: CagComponent
    },
    {
        path: 'education',
        component: EducationComponent
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'personal',
        component: PersonalComponent
    },
    {
        path: '',
        redirectTo: 'sme',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: 'sme',
        pathMatch: 'full'
    }
];