import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { DIAConfig } from '../../../../Shared/config';

@Injectable()
export class SmeSearchGetSearchReportsService {
    url = DIAConfig.apiUrl + "/sme/getSearchReports";
    constructor(private http: Http) { }

    public getSearchReportList(data:any): Observable<any> {
       console.log(data);
       console.log(this.url);
        return this.http.post(this.url, data, null)
            .map(response => {
                console.log("Success");
                let result: any = response.json();
                console.log(result);
                return result;
            })
            .catch(response => {
                console.log("Error");
                return "Error";
            });

    }
}