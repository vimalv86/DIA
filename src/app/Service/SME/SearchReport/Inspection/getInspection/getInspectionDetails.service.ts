import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'

import { DIAConfig } from '../../../../../Shared/config';
@Injectable()
export class getInspectionDetailsService {
    http: Http;
    posts_Url: string = DIAConfig.apiUrl + '/sme/getInspectionStatus';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(data: any) {
        return this.http.post(this.posts_Url, data, {
        })
            .map(res => res.json());
    }
}		