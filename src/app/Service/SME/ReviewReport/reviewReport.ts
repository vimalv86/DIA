export class ReviewReport {
    losNumber: string = null;
    referenceId: string = null;
    unitName: string = null;
    inspectionType: string = null;
    pfId: string = null;
    dateofSubmission: string = null;
}