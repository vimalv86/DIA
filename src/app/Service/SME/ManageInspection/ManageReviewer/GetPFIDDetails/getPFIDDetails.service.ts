import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class GetPFIDService {
   // url = "http://sbi-706173083.us-west-2.elb.amazonaws.com/UmService/getPfId";
    url = "https://10.209.74.71:7002/UmService/getPfId"; 

    constructor(private http: Http) { }

    public getPFIDInfo(data:any): Observable<any> {
       console.log(data);
        return this.http.post(this.url, data, null)
            .map(response => {
                console.log("Success");
                let result: any = response.json();
                console.log(result);
                return result;
            })
            .catch(response => {
                console.log("Error");
                return "Error";
            });

    }
}