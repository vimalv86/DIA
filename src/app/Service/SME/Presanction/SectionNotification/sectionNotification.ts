export class SmeSectionNotification {
    postPending: number;
    postReview: number;
    preReview: number;
    postIncompleted: number;
    prePending: number;
    preIncompleted: number;
}