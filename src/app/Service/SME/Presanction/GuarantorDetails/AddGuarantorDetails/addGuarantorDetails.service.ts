import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { GuarantorDetails } from '../GuarantorDetails';

import { DIAConfig } from '../../../../../Shared/config';
@Injectable()
export class AddGuarantorDetailsService {
    http: Http;
    returnCommentStatus: Object = [];
    posts_Url: string = DIAConfig.apiUrl + '/sme/updateguarantordetails';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(guarantorDetails: any) {
        return this.http.post(this.posts_Url, guarantorDetails, {
        })
            .map(res => res.json());
    }
}		