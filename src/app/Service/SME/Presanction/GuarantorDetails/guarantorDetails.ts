export class GuarantorDetails {
    borrowerRefId: string = null;
    salutation: string = null;
    firstName: string = null;
    middleName: string = null;
    lastName: string = null;
    realtiveSalutation: string = null;
    relativeFirstName: string = null;
    relativeMiddleName: string = null;
    relativeLastName: string = null;
    plotBuildingFlatNum: string = null;
    address1: string = null;
    address2: string = null;
    city: string = null;
    district: string = null;
    blockTesilSubdistrict: string = null;
    village: string = null;
    state: string = null;
    pinCode: string = null;
    mobileNum: string = null;
    landNum: string = null;
    email: string = null;
}


