import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { EstablishmentDetails } from '../establishmentDetails';

import { DIAConfig } from '../../../../../Shared/config';
@Injectable()
export class AddEstablishmentDetailsService {
    http: Http;
    returnCommentStatus: Object = [];
    posts_Url: string = DIAConfig.apiUrl + '/smepre/updateestablishmentdetails';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(establishmentDetails: any) {
        return this.http.post(this.posts_Url, establishmentDetails, {
        })
            .map(res => res.json());
    }
}		