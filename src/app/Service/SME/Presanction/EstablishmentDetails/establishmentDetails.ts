export class EstablishmentDetails {
    addressLine1: string = null;
    city: string = null;
    state: string = null;
    pinCode: Number = null;
    building_no: string = null;
    addressLine2: string = null;
    district: string = null;
    sub_district: string = null;
    village: string = null;
    ownership: String = null;
    proposedActivity: String = null;
    activity: String = null;
    comment: String = null;

    establishRefId: string = null;
    dateOfInspectionTS: Number = null;
    imageList: string = null;
}