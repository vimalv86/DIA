import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { Comments } from '../comments';

import { DIAConfig } from '../../../../../Shared/config';
@Injectable()
export class AddCommentsService {
    http: Http;
    returnCommentStatus: Object = [];
    posts_Url: string = DIAConfig.apiUrl + '/sme/addofficialcommets';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(comments: any) {
        return this.http.post(this.posts_Url, comments, {
        })
            .map(res => res.json());
    }
}		