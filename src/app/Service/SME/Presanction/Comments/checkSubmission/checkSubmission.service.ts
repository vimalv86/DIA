import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { Comments } from '../comments';

import { DIAConfig } from '../../../../../Shared/config';
@Injectable()
export class CheckSubmissionService {
    http: Http;
    posts_Url: string = DIAConfig.apiUrl + '/smepre/presubmissioncheck';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(data: any) {
        return this.http.post(this.posts_Url, data, {
        })
            .map(res => res.json());
    }
}		