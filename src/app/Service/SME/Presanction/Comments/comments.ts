export class Comments {
    inspectionId: string = null;
    dateOfInspectionTS: string = null;
    applicationSource: string = null;
    otherSource: string = null;
    commentOnGeneralWorkFirm: string = null;
    commentsOnActivity: string = null;
    commentsOnDataCollection: string = null;
    remarkablePointFromBorrower: string = null;
    positionOfStatutoryDues: string = null;
    nameOfAccompanyingOfficial: string = null;
    pfIdOfReviewer: string = null;
}


