export class LoanRequestedDetails {
    loanReqRefId: string = null;
    borrowerRefId: string = null;
    loanReqType: string = null;
    otherLoan: string = null;
    purposeOfLoan: string = null;
    otherPurposeOfLoan: string = null;
    limitRequested: string = null;
}


