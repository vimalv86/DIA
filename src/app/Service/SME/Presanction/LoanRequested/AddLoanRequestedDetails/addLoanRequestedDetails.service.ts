import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'

import { LoanRequestedDetails } from '../loanRequestedDetails';

import { DIAConfig } from '../../../../../Shared/config';
@Injectable()
export class AddLoanRequestedDetailsService {
    http: Http;
    returnCommentStatus: Object = [];
    posts_Url: string = DIAConfig.apiUrl + '/sme/updateloanrequesteddetails';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(loanRequestedDetails: any) {
        return this.http.post(this.posts_Url, loanRequestedDetails, {
        })
            .map(res => res.json());
    }
}		