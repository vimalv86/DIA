export class CollateralDetails {
    building_no: string = null;
    loanNumber: Number = 999;
    addressLine1: string = null;
    addressLine2: string = null;
    state: string = null;
    city: string = null;
    district: string = null;
    sub_district: string = null;
    village: string = null;
    pinCode: Number = null;
    mobile_number: Number = null;
    //comments: String = null;
    dateOfInspectionTS: Number = null;
    imageList: string = null;
}
