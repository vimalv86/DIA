import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { CollateralDetails } from '../collateralDetails';

import { DIAConfig } from '../../../../../Shared/config';
@Injectable()
export class AddCollateralDetailsService {
    http: Http;
    returnCommentStatus: Object = [];
    posts_Url: string = DIAConfig.apiUrl + '/smepre/updatecollateraldetails';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(collateralDetails: any) {
        return this.http.post(this.posts_Url, collateralDetails, {
        })
            .map(res => res.json());
    }
}		