import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { UnitDetails } from '../unitDetails';

import { DIAConfig } from '../../../../../../Shared/config';
@Injectable()
export class AddUnitDetailsService {
    http: Http;
    returnCommentStatus: Object = [];
    posts_Url: string = DIAConfig.apiUrl + '/smepre/updateborrowerunit';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(unitDetails: any) {
        return this.http.post(this.posts_Url, unitDetails, {
        })
            .map(res => res.json());
    }
}		