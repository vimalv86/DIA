export class AddUpdateInspection {
    borrowerUnitId: string = null;
    executiveReviewerPFIndex: string = null;
    accountNum: string = null;
    branchCode: string = null;
    conductedDateTS: string = null;
}