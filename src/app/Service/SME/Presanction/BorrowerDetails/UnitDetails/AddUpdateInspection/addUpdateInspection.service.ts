import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { AddUpdateInspection } from '../addUpdateInspection';

import { DIAConfig } from '../../../../../../Shared/config';
@Injectable()
export class AddUpdateInspectionService {
    http: Http;
    returnCommentStatus: Object = [];
    posts_Url: string = DIAConfig.apiUrl + '/sme/addpreinspection';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postInspection(AddUpdateInspection: any) {
        return this.http.post(this.posts_Url, AddUpdateInspection, {
        })
            .map(res => res.json());
    }
}		