import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { KeyPersonDetails } from '../keyPersonDetails';

import { DIAConfig } from '../../../../../../Shared/config';
@Injectable()
export class AddKeyPersonDetailsService {
    http: Http;
    returnCommentStatus: Object = [];
    posts_Url: string = DIAConfig.apiUrl+'/sme/updatekeyperson';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(keyPersonDetails: any) {
        return this.http.post(this.posts_Url, keyPersonDetails, {
        })
            .map(res => res.json());
    }
}		