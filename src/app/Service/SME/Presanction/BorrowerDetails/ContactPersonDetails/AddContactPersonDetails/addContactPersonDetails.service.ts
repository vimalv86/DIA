import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { ContactPersonDetails } from '../contactPersonDetails';

import { DIAConfig } from '../../../../../../Shared/config';
@Injectable()
export class AddContactPersonDetailsService {
    http: Http;
    returnCommentStatus: Object = [];
    posts_Url: string = DIAConfig.apiUrl + '/sme/updatecontactperson';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(contactPersonDetails: any) {
        return this.http.post(this.posts_Url, contactPersonDetails, {
        })
            .map(res => res.json());
    }
}		