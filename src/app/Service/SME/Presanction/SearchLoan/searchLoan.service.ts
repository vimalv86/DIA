import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { SmeSearchLoan } from './searchLoan';

import { DIAConfig } from '../../../../Shared/config';

@Injectable()
export class SmePresanctionSearchLoanService {
    //losUrl = DIAConfig.apiUrl + "/smepre/getlosdetails/";
    losUrl = "https://mobile.onlinesbi.com/diauat/smepre/getLOSServiceDetails";
  //  losUrl = DIAConfig.apiUrl + "/smepre/getLOSServiceDetails";
   // branchUrl = DIAConfig.apiUrl + "/smepre/searchlos/";
   // branchUrl = DIAConfig.apiUrl + "/smepre/getLOSServiceDetailsWithDate/";
    branchUrl = "https://mobile.onlinesbi.com/diauat/smepre/getLOSServiceDetailsWithDate/";
    constructor(private http: Http) { }

    public getSmeSearchLoanWithLOS(data:any): Observable<any> {
       console.log(data);
        return this.http.post(this.losUrl, data, null)
            .map(response => {
                console.log("Success");
                let result: any = response.json();
                console.log(result);
                return result;
            })
            .catch(response => {
                console.log("Error");
                return "Error";
            });

    }

    public getSmeSearchLoanWithBranch(data:any): Observable<any> {
        console.log(data);
         return this.http.post(this.branchUrl, data, null)
             .map(response => {
                 console.log("Success");
                 let result: any = response.json();
                 console.log(result);
                 return result;
             })
             .catch(response => {
                 console.log("Error");
                 return "Error";
             });
 
     }
}