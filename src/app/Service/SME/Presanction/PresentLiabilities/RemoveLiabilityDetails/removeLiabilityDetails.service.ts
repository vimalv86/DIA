import { HttpModule, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { LiabilityDetails } from '../LiabilityDetails';

import { DIAConfig } from '../../../../../Shared/config';
@Injectable()
export class RemoveLiabilityDetailsService {
    http: Http;
    returnCommentStatus: Object = [];
    posts_Url: string = DIAConfig.apiUrl + '/sme/removeborrowerliabilitiesdetails';
    constructor(public _http: Http) {
        this.http = _http;
    }
    postComment(liabilityDetails: any) {
        return this.http.post(this.posts_Url, liabilityDetails, {
        })
            .map(res => res.json());
    }
}		