export class LiabilityDetails {
    liabilityRefId: string = null;
    borrowerRefId: string = null;
    liabilityExist: string = null;
    facilityType: string = null;
    others: string = null;
    accountNumber: string = null;
    limitSanction: string = null;
    dateOfSanctionTS: string = null;
    drawingPower: string = null;
    advancedValue: string = null;
    marketValue: string = null;
    outstandingAmount: string = null;
}


