export class SmeInspectionStatus {
    loanNum: string = null;
    cifNum: string = null;
    inspectionDate: string = null;
    reviewDate: string = null;
    reviewStatus: string = null;
    rejectionReason: string = null;
    //delete: string = null;
}